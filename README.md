# Hearting

【2017-05】 

Hearting 心加波 腦波心動指數量測應用於交友系統 近年來，科技發展快速，網路成為人際關係的媒介，越來越多人透過網路交友相識進而交，但往往遇到不適合的對象，因此本研究針對年輕人有吸引力的主題「社群交友」以及「心動的感覺」下去延伸，透過腦波來分析是否對一個人感到心動，進而有後續的發展。

## 系統畫面
------------------------

### 會員註冊

![](https://gitlab.com/w860403/hearting/raw/master/doc/register.PNG)

### 上傳測驗圖片

![](https://gitlab.com/w860403/hearting/raw/master/doc/upload-photo.PNG)

### 選擇測驗及連接腦波耳機

![](https://gitlab.com/w860403/hearting/raw/master/doc/test1.PNG)

### 開始測驗

![](https://gitlab.com/w860403/hearting/raw/master/doc/test2.PNG)

### 測驗結束

![](https://gitlab.com/w860403/hearting/raw/master/doc/test3.PNG)