export const login_success = () => ({
    type: 'LOGIN_SUCCESS'
});

export const login_fail = () => ({
    type: 'LOGIN_FAIL'
});

export const login = (account, password) => {
    return (dispatch, getState, { api }) => {
        api.fetch_login(account, password)
            .then(data => {
                console.log(data)
                if (data && data.status === true) {
                    dispatch({
                        type: 'LOGIN_SUCCESS', user: data.user.Account, sex: data.user.Sex


                    })
                } else {
                    dispatch({
                        type: 'LOGIN_FAIL'
                    })
                }
            })
            .catch(err => console.log(err));
    };
};
