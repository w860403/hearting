export const check_account = (account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_check_account(account)
            .then(data => {
                if (data.result.status === '1') {
                    dispatch({
                        type: 'ACCOUNT_REPEAT', status: true
                    })
                } else {
                    dispatch({
                        type: 'ACCOUNT_REPEAT', status: false
                    })
                }
            })
            .catch(err => console.log(err));
    };
};
export const upload_image = (account, fileStr) => {
    return (dispatch, getState, { api }) => {
        api.fetch_upload_image(account, fileStr)
            .then(data => {
                // if (data.status) {
                //     dispatch({
                //         type: 'UPLOAD_IMAGE', status: false
                //     })
                // } else {
                //     dispatch({
                //         type: 'UPLOAD_IMAGE', status: true
                //     })
                // }
            })
            .catch(err => console.log(err));
    };
};
export const register_member = (mamberData) => {
    var member=mamberData;
    return (dispatch, getState, { api }) => {
        api.fetch_register_member(mamberData)
            .then(data => {
                if (data.status === true) {
                    api.fetch_login(member.master.Account, member.master.Password)
                        .then(data => {
                            console.log(data)
                            if (data && data.status === true) {
                                dispatch({
                                    type: 'LOGIN_SUCCESS', user: data.user.Account


                                })
                            } else {
                                dispatch({
                                    type: 'LOGIN_FAIL'
                                })
                            }
                        })
                        .catch(err => console.log(err));
                } else {
                    dispatch({
                        type: 'LOGIN_FAIL'
                    })
                }
            })
            .catch(err => console.log(err));
    };
};