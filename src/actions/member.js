export const save_upload_image_data = (image) => {
    return (dispatch, getState) => {
        dispatch({
            type: 'SAVE_IMAGE_TYPE', image: image
        })
    };
}
export const get_member_data = (account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_member_data(account)
            .then(data => {
                dispatch({
                    type: 'MEMBER_DATA', member_data: data.user,
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_member_info = (account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_member_info(account)
            .then(data => {
                dispatch({
                    type: 'MEMBER_INFO', member_info: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const edit_member_data = (data) => {
    const account = data.Account;
    return (dispatch, getState, { api }) => {
        api.fetch_edit_member_data(data)
            .then(data => {
                if (data.status) {
                    dispatch({
                        type: 'MEMBER_DATA', member_data: data.user
                    })
                }
            })
            .catch(err => console.log(err));
    };
};
export const edit_member_info = (data) => {
    return (dispatch, getState, { api }) => {
        api.fetch_edit_member_info(data)
            .then(data => {
                if (data.status) {
                }
            })
            .catch(err => console.log(err));
    };
};
export const get_history_rank = (data) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_history_rank(data)
            .then(data => {
                var random = [];
                var back = [];
                var angle = [];
                data.map(function (data, index) {
                    switch (data.Function) {
                        case 'randomTest':
                            random.push(data);
                            break;
                        case 'backTest':
                            back.push(data);
                            break;
                        case 'angleTest':
                            angle.push(data);
                            break;
                        default:
                            break;
                    }
                })
                var history_rank = [
                    { data: random },
                    { data: angle },
                    { data: back }
                ]
                dispatch({
                    type: 'HISTORY_RANK', history_rank: history_rank
                })
            })
            .catch(err => console.log(err));
    };
};
export const upload_test_image = (account, fun, imageType, fileStr) => {
    return (dispatch, getState, { api }) => {
        api.fetch_upload_test_image(account, fun, imageType, fileStr)
            .then(data => {
                if (data.status) {
                }
            })
            .catch(err => console.log(err));
    };
}
export const get_now_image = (account, fun) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_now_image(account, fun)
            .then(data => {
                dispatch({
                    type: 'MEMBER_NOW_IMAGE', member_now_image: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_member_photo = (account) => {
    const randomTest = 'randomTest';
    const backTest = 'backTest';
    const angleTest = 'angleTest';
    return (dispatch, getState, { api }) => {
        api.fetch_get_member_photo(account, randomTest)
            .then(data => {
                dispatch({
                    type: 'MEMBER_PHOTO_RANDOM', member_photo_random: data
                })
            })
            .catch(err => console.log(err));
        api.fetch_get_member_photo(account, backTest)
            .then(data => {
                dispatch({
                    type: 'MEMBER_PHOTO_BACK', member_photo_back: data
                })
            })
            .catch(err => console.log(err));
        api.fetch_get_member_photo(account, angleTest)
            .then(data => {
                dispatch({
                    type: 'MEMBER_PHOTO_ANGLE', member_photo_angle: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_history_quiz = (account) => {
    const randomTest = 'randomTest';
    const backTest = 'backTest';
    const angleTest = 'angleTest';
    return (dispatch, getState, { api }) => {
        api.fetch_get_history_quiz(account, randomTest)
            .then(data => {
                dispatch({
                    type: 'HISTORY_QUIZ_RANDOM', history_quiz_random: data
                })
            })
            .catch(err => console.log(err));
        api.fetch_get_history_quiz(account, backTest)
            .then(data => {
                dispatch({
                    type: 'HISTORY_QUIZ_BACK', history_quiz_back: data
                })
            })
            .catch(err => console.log(err));
        api.fetch_get_history_quiz(account, angleTest)
            .then(data => {
                dispatch({
                    type: 'HISTORY_QUIZ_ANGLE', history_quiz_angle: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_history_quiz_detail = (account, fun, date) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_history_quiz_detail(account, fun, date)
            .then(data => {
                dispatch({
                    type: 'HISTORY_QUIZ_DETAIL', history_quiz_detail: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const logout = () => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOGIN_SUCCESS', user: null, sex: null
        })
    };
}