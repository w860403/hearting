export const get_image = (account, functionName) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_image(account, functionName)
            .then(data => {
                // console.log(data)
                if (data) {
                    dispatch({
                        type: 'GET_IMAGE_SUCCESS', data: data
                    })
                }
            })
            .catch(err => console.log(err));
    };
};

const quizPointArray = []
export const get_quiz_point = (mindwaveData, imageArray, account, imageNumber, sex) => {
    console.log(sex);
    return (dispatch, getState, { api }) => {
        if (sex == '男') {
            api.fetch_get_quiz_point(mindwaveData)
                .then(data => {
                    console.log(data)
                    console.log('算男生分數');
                    if (data) {
                        quizPointArray.push(data.point)
                        if (quizPointArray.length == imageNumber) {
                            console.log('測驗結束，取得以下值，準備Post');
                            console.log(imageArray)
                            console.log(quizPointArray)
                            console.log(account)
                            api.fetch_save_quiz_history(imageArray, quizPointArray, account)
                                .then(data => {
                                    console.log(data)
                                    if (data) {
                                        dispatch({
                                            type: 'SAVE_QUIZ_HISTORY_SUCCESS', resultData: data
                                        })
                                        quizPointArray = []
                                    }
                                })
                                .catch(err => console.log(err));
                        }
                        dispatch({
                            type: 'GET_QUIZARRAY_POINT_SUCCESS', quizPointArray: quizPointArray
                        })
                    }
                })
                .catch(err => console.log(err));
        } else if (sex == '女') {
            api.fetch_get_quiz_pointw(mindwaveData)
                .then(data => {
                    console.log(data)
                    console.log('算女生分數');
                    if (data) {
                        quizPointArray.push(data.point)
                        if (quizPointArray.length == imageNumber) {
                            console.log('測驗結束，取得以下值，準備Post');
                            console.log(imageArray)
                            console.log(quizPointArray)
                            console.log(account)
                            api.fetch_save_quiz_history(imageArray, quizPointArray, account)
                                .then(data => {
                                    console.log(data)
                                    if (data) {
                                        dispatch({
                                            type: 'SAVE_QUIZ_HISTORY_SUCCESS', resultData: data
                                        })
                                        quizPointArray = []
                                    }
                                })
                                .catch(err => console.log(err));
                        }
                        dispatch({
                            type: 'GET_QUIZARRAY_POINT_SUCCESS', quizPointArray: quizPointArray
                        })
                    }
                })
                .catch(err => console.log(err));
        }
    };
};
//save_quiz_history
export const save_quiz_history = (imageArray, quizPointArray, account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_save_quiz_history(imageArray, quizPointArray, account)
            .then(data => {
                console.log(data)
                if (data) {
                    dispatch({
                        type: 'SAVE_QUIZ_HISTORY_SUCCESS', resultData: data
                    })
                }
            })
            .catch(err => console.log(err));
    };
};

//send_invitation
export const send_invitation = (account, partner) => {
    return (dispatch, getState, { api }) => {
        api.fetch_send_invitation(account, partner)
            .then(data => {
                console.log(data)
                // if (data.status) {
                //     dispatch({
                //         type: 'SEND_INVITATION_SUCCESS', status: data.status
                //     })
                // } else {
                //     dispatch({
                //         type: 'SEND_INVITATION_FAIL', status: data.status
                //     })
                // }
            })
            .catch(err => console.log(err));
    };
};
//save_as_charm
export const save_as_charm = (charmList, sex) => {
    return (dispatch, getState, { api }) => {
        api.fetch_save_as_charm(charmList, sex)
            .then(data => {
                console.log(data)
                if (data.status) {
                    dispatch({
                        type: 'SAVE_CHARM_SUCCESS', status: data.status
                    })
                } else {
                    dispatch({
                        type: 'SAVE_CHARM_FAIL', status: data.status
                    })
                }
            })
            .catch(err => console.log(err));
    };
};