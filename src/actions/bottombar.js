export const on_button_click = (newTabIndex) => {
    switch (newTabIndex) {
        case 0:
            return (dispatch, getState) => {
                dispatch({
                    type: 'HOME'
                })
            };
        case 1:
            return (dispatch, getState) => {
                dispatch({
                    type: 'PERSONAL'
                })
            };
        case 2:
            return (dispatch, getState) => {
                dispatch({
                    type: 'QUIZ'
                })
            };
        case 3:
            return (dispatch, getState) => {
                dispatch({
                    type: 'FRIEND'
                })
            };
        case 4:
            return (dispatch, getState) => {
                dispatch({
                    type: 'MEMBER'
                })
            };
    }

};
