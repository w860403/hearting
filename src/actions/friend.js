import {
    Alert
} from 'react-native';
export const login_success = () => ({
    type: 'LOGIN_SUCCESS'
});

export const login_fail = () => ({
    type: 'LOGIN_FAIL'
});

export const get_friend_list = (account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_friend_list(account)
            .then(data => {
                console.log(data)
                dispatch({
                    type: 'GET_FRIEND_LIST_SUCCESS', friendsData: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_friend_request = (account) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_friend_request(account)
            .then(data => {
                console.log(data)
                dispatch({
                    type: 'GET_FRIEND_REQUEST_SUCCESS', friendsRequestData: data
                })
            })
            .catch(err => console.log(err));
    };
};
//reply_invation
export const reply_invation = (account, partner, isaccept) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_reply_invation(account, partner, isaccept)
            .then(data => {
                console.log(data)
                console.log(partner)
                api.fetch_get_friend_list(partner)
                    .then(data => {
                        console.log(data)
                        dispatch({
                            type: 'GET_FRIEND_LIST_SUCCESS', friendsData: data
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    };
};

//delete_friend
export const delete_friend = (account, partner) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_delete_friend(account, partner)
            .then(data => {
                console.log(data)
                // dispatch({
                //     type: 'GET_FRIEND_REQUEST_SUCCESS', friendsRequestData: data
                // })
            })
            .catch(err => console.log(err));
    };
};
//get_love_letter_status
export const get_love_letter_status = (account) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_love_letter_status(account)
            .then(data => {
                console.log(data)
                dispatch({
                    type: 'GET_LOVE_LETTER_STATUS_SUCCESS', LovaLetterStatusData: data
                })
            })
            .catch(err => console.log(err));
    };
};
//sent_love_letter
export const sent_love_letter = (account, partner, content) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_sent_love_letter(account, partner, content)
            .then(data => {
                console.log(data)
                api.fetch_get_love_letter_status(account)
                    .then(data => {
                        console.log(data)
                        dispatch({
                            type: 'GET_LOVE_LETTER_STATUS_SUCCESS', LovaLetterStatusData: data
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    };
};

//cancel_love_letter
export const cancel_love_letter = (account) => {
    const callback = (message) => {
        Alert.alert('還不可取消告白信', message)
    }
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_cancel_love_letter(account)
            .then(data => {
                console.log(data)
                if (data.status) {
                } else {
                    dispatch({
                        type: 'CANCEL_LOVE_LETTER_FAIL', Message: data.E_Msg, Callback: callback
                    })
                }
            })
            .catch(err => console.log(err));
    };
};
//get_love_letter_content
export const get_love_letter_content = (account) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_love_letter_content(account)
            .then(data => {
                console.log(data)
                dispatch({
                    type: 'GET_LOVE_LETTER_CONTENT_SUCCESS', content: data.Content
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_chat = (account, partner) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_chat(account, partner)
            .then(data => {
                dispatch({
                    type: 'GET_CHAT', chat: data
                })
            })
            .catch(err => console.log(err));
    };
};
//post_message
export const post_message = (account, partner, content) => {
    var account2 = account;
    var partner2 = partner;
    var content2 = content;
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_post_message(account, partner, content)
            .then(data => {
                api.fetch_get_chat(account2, partner2)
                    .then(data => {
                        dispatch({
                            type: 'GET_CHAT', chat: data
                        })
                    })
                    .catch(err => console.log(err));
                api.fetch_get_chat_list(account2)
                    .then(data => {
                        dispatch({
                            type: 'GET_CHAT_LIST', chatlist: data
                        })
                    })
                    .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
    };
};
export const get_chat_list = (account) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_chat_list(account)
            .then(data => {
                dispatch({
                    type: 'GET_CHAT_LIST', chatlist: data
                })
            })
            .catch(err => console.log(err));
    };
};