export const get_quiz_point = (mindwaveData) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_quiz_point(mindwaveData)
            .then(data => {
                // console.log(data)
                if (data) {
                    dispatch({
                        type: 'GET_QUIZ_POINT_SUCCESS', quizPoint: data.point
                    })
                }
            })
            .catch(err => console.log(err));
    };
};