export const get_rank = () => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_rank(0)
            .then(data => {
                dispatch({
                    type: 'RANK_GIRL', rank_girl: data
                })
            })
            .catch(err => console.log(err));
        api.fetch_get_rank(1)
            .then(data => {
                dispatch({
                    type: 'RANK_BOY', rank_boy: data
                })
            })
            .catch(err => console.log(err));
    };
};