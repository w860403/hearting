export const get_constellation = (sex,constellation,type) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_constellation(sex,constellation,type)
            .then(data => {
                dispatch({
                    type: 'GET_ANALYSIS', analysis: data
                })
            })
            .catch(err => console.log(err));
    };
};
export const get_personal = (account) => {
    // console.log({account:account,partner:partner,isaccept:isaccept});
    return (dispatch, getState, { api }) => {
        api.fetch_get_personal(account)
            .then(data => {
                dispatch({
                    type: 'GET_PERSONAL', analysis: data
                })
            })
            .catch(err => console.log(err));
    };
};