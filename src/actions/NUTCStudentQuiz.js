export const get_student_image = (sex) => {
    return (dispatch, getState, { api }) => {
        api.fetch_get_student_image(sex)
            .then(data => {
                // console.log(data)
                if (data) {
                    dispatch({
                        type: 'GET_STUDENT_IMAGE_SUCCESS', data: data
                    })
                }
            })
            .catch(err => console.log(err));
    };
};

const quizPointArray = []
export const get_student_quiz_point = (mindwaveData, imageArray, imageNumber, sex) => {
    return (dispatch, getState, { api }) => {
        if (sex == '男') {
            api.fetch_get_quiz_point(mindwaveData)
                .then(data => {
                    console.log(data)
                    console.log('算男生分數');
                    if (data) {
                        quizPointArray.push(data.point)
                        if (quizPointArray.length == imageNumber) {
                            console.log('測驗結束，取得以下值，準備Post');
                            console.log(imageArray)
                            console.log(quizPointArray)
                            api.fetch_save_student_quiz_history(imageArray, quizPointArray)
                                .then(data => {
                                    console.log(data)
                                    if (data) {
                                        dispatch({
                                            type: 'SAVE_STUDENT_QUIZ_HISTORY_SUCCESS', data: data
                                        })
                                        quizPointArray = []
                                    }
                                })
                                .catch(err => console.log(err));
                        }
                        dispatch({
                            type: 'GET_QUIZARRAY_POINT_SUCCESS', quizPointArray: quizPointArray
                        })
                    }
                })
                .catch(err => console.log(err));
        } else if (sex == '女') {
            api.fetch_get_quiz_pointw(mindwaveData)
                .then(data => {
                    console.log(data)
                    console.log('算女生分數');
                    if (data) {
                        quizPointArray.push(data.point)
                        if (quizPointArray.length == imageNumber) {
                            console.log('測驗結束，取得以下值，準備Post');
                            console.log(imageArray)
                            console.log(quizPointArray)
                            api.fetch_save_student_quiz_history(imageArray, quizPointArray)
                                .then(data => {
                                    console.log(data)
                                    if (data) {
                                        dispatch({
                                            type: 'SAVE_STUDENT_QUIZ_HISTORY_SUCCESS', data: data
                                        })
                                        quizPointArray = []
                                    }
                                })
                                .catch(err => console.log(err));
                        }
                        dispatch({
                            type: 'GET_QUIZARRAY_POINT_SUCCESS', quizPointArray: quizPointArray
                        })
                    }
                })
                .catch(err => console.log(err));
        }
    };
};

//save_as_charm
export const save_as_charm = (charmList, sex) => {
    return (dispatch, getState, { api }) => {
        api.fetch_save_as_charm(charmList, sex)
            .then(data => {
                console.log(data)
                if (data.status) {
                    dispatch({
                        type: 'SAVE_CHARM_SUCCESS', status: data.status
                    })
                } else {
                    dispatch({
                        type: 'SAVE_CHARM_FAIL', status: data.status
                    })
                }
            })
            .catch(err => console.log(err));
    };
};