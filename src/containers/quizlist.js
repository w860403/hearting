import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import QuizListComponent from '../components/quizlist';
// const mapStateToProps = (state) => ({
//     login_status: state.login_status,
//     login_data: state.login_data
// });

const mapDispatchToProps = (dispatch) => ({
    // on_login_button_click: (account, password) => {
    //     dispatch(login(account, password));
    // },
    on_quiz_button_click: (quizFunction) => {
        Actions.imagequiz({ quizFunction })
    },
    on_student_quiz_button_click: () => {
        Actions.nutcstudentquiz()
    },
    on_teacher_quiz_button_click: () => {
        Actions.imteacherquiz()
    },
    onWifeControlButtonClick: () => {
        Actions.wifecontrol()
    },
    onLiveActionQuizButtonClick: () => {
        Actions.liveactionquiz()
    }
}
);


export default connect(mapDispatchToProps)(QuizListComponent);
