import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import RegisterComponent from '../components/register';
import { check_account, upload_image, register_member } from '../actions/register';
import { upload_test_image } from '../actions/member';


const mapStateToProps = (state) => ({
    check_account_status: state.register.status,
    login_status: state.login_status,
});

const mapDispatchToProps = (dispatch) => ({
    checkAccount: (account) => {
        dispatch(check_account(account));
    },
    oploadImage: (account, fileStr) => {
        dispatch(upload_image(account, fileStr));
    },
    registerMember: (mamberData) => {
        dispatch(register_member(mamberData));
    },
    oploadTestImage: (account, fun, imageType, fileStr) => {
        dispatch(upload_test_image(account, fun, imageType, fileStr));
    },
    // on_register_button_click:() =>{
    //     Actions.register();
    // }
}
);
class RegisterContainer extends RegisterComponent {
    constructor(props) {
        super(props);
        let now = new Date();
        this.state = {
            check_account_status: this.props.check_account_status,
            slideIndex: 0,
            favoriteVisible: false,
            accountVisible: true,
            Account: '',
            Password: '',
            checkPassword: '',
            Name: '',
            Age: 0,
            Birthday: now,
            Sex: '',
            Aptitude: '',
            Email: '',
            Constellation: '',
            Blood: '',
            Height: -1,
            Weight: -1,
            Address: '',
            Image: '',
            isLove: '',
            Interest: '',
            d1: '0',
            d2: '0',
            d3: '0',
            d4: '0',
            d5: '0',
            d6: '0',
            d7: '0',
            d8: '0',
            d9: '0',
            d10: '0',
            d0: '0',
            isD1: false,
            isD2: false,
            isD3: false,
            isD4: false,
            isD5: false,
            isD6: false,
            isD7: false,
            isD8: false,
            isD9: false,
            isD10: false,
            isD0: false,
        }
    }
    componentWillReceiveProps(nextProps) {
        const previous_check_account_status = this.props.check_account_status
        const check_account_status = nextProps.check_account_status
        if (previous_check_account_status !== check_account_status) {
            if (check_account_status) {
                this.setState({
                    check_account_status: true,
                    selectedOption: '帳號密碼',
                })
            }
        }
        const { login_status: previous_login_status } = this.props;
        const { login_status } = nextProps;
        if (previous_login_status != login_status) {
            if (login_status == true) {
                Actions.pop();
            }
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);