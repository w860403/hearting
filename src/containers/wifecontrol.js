import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import WifeControlComponent from '../components/wifecontrol';
import MindWaveMobile from 'react-native-mindwave-mobile';
import { on_eeg_power_delta, on_eeg_power_low_beta, on_esense } from '../actions/mindwave';
import { get_quiz_point } from '../actions/livequiz';
const mwm = new MindWaveMobile()


const mapStateToProps = (state) => ({
    delta: state.eeg_power_delta.delta,
    highAlpha: state.eeg_power_delta.highAlpha,
    lowAplpha: state.eeg_power_delta.lowAplpha,
    theta: state.eeg_power_delta.theta,
    lowBeta: state.eeg_power_low_beta.lowBeta,
    midGamma: state.eeg_power_low_beta.midGamma,
    highBeta: state.eeg_power_low_beta.highBeta,
    lowGamma: state.eeg_power_low_beta.lowGamma,
    poorSignal: state.esense.poorSignal,
    mindwaveTimer: state.eeg_power_delta.mindwaveTimer,
    quizPoint: state.quiz_point
});

const mapDispatchToProps = (dispatch) => ({
    onEEGPowerDelta: (data, mindwaveTimer) => {
        dispatch(on_eeg_power_delta(data.delta, data.highAlpha, data.lowAplpha, data.theta, mindwaveTimer));
    },
    onEEGPowerLowBeta: (data) => {
        dispatch(on_eeg_power_low_beta(data.lowBeta, data.midGamma, data.highBeta, data.lowGamma));
    },
    onESense: (data) => {
        dispatch(on_esense(data.poorSignal));
    },
    getQuizPoint: (mindwaveData) => {
        dispatch(get_quiz_point(mindwaveData));
    }
}
);
export default connect(mapStateToProps, mapDispatchToProps)(WifeControlComponent);
