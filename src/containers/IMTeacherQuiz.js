import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import IMTeacherQuizComponent from '../components/IMTeacherQuiz10s';
import MindWaveMobile from 'react-native-mindwave-mobile';
import { get_teacher_quiz_point, get_teacher_image } from '../actions/IMTeacherQuiz';
import { on_eeg_power_delta, on_eeg_power_low_beta, on_esense } from '../actions/mindwave';
const mwm = new MindWaveMobile()

const mapStateToProps = (state) => ({
    delta: state.eeg_power_delta.delta,
    highAlpha: state.eeg_power_delta.highAlpha,
    lowAplpha: state.eeg_power_delta.lowAplpha,
    theta: state.eeg_power_delta.theta,
    lowBeta: state.eeg_power_low_beta.lowBeta,
    midGamma: state.eeg_power_low_beta.midGamma,
    highBeta: state.eeg_power_low_beta.highBeta,
    lowGamma: state.eeg_power_low_beta.lowGamma,
    poorSignal: state.esense.poorSignal,
    mindwaveTimer: state.eeg_power_delta.mindwaveTimer,
    quizPointArray: state.quiz_point_array,
    TeacherQuizResultData: state.teacher_quiz_resultdata,
    TeacherImageDataArray: state.teacher_image_data_array,
    account: state.login_data ? state.login_data : '',
    sex: state.login_sex ? state.login_sex : ''
});

const mapDispatchToProps = (dispatch) => ({
    onEEGPowerDelta: (data, mindwaveTimer) => {
        dispatch(on_eeg_power_delta(data.delta, data.highAlpha, data.lowAplpha, data.theta, mindwaveTimer));
    },
    onEEGPowerLowBeta: (data) => {
        dispatch(on_eeg_power_low_beta(data.lowBeta, data.midGamma, data.highBeta, data.lowGamma));
    },
    onESense: (data) => {
        dispatch(on_esense(data.poorSignal));
    },
    goToTeacherImageTest: () => {
        dispatch(get_teacher_image());
    },
    getQuizPoint: (mindwaveData, imageArray, imageNumber,sex) => {
        dispatch(get_teacher_quiz_point(mindwaveData, imageArray, imageNumber,sex));
    },
}
);

export default connect(mapStateToProps, mapDispatchToProps)(
    IMTeacherQuizComponent
);
