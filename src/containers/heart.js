import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import HeartComponent from '../components/heart';
import { login } from '../actions/login';


const mapStateToProps = (state) => ({
    showComponent: state.heart.component,
    account: state.login_data
});

class HeartContainer extends HeartComponent {
    constructor(props) {
        super(props)
        this.state = {
            showComponent: null,
            account: null,
            animating: true,
            loaded: false,
        }
    }
    componentWillReceiveProps(nextProps) {
        const { showComponent: previous_showComponent } = this.props;
        const { showComponent } = nextProps;
        if (previous_showComponent != showComponent) {
            if (showComponent) {
                this.setState({
                    showComponent: showComponent,
                })
            }
        }
    }
}
export default connect(mapStateToProps)(HeartContainer);
