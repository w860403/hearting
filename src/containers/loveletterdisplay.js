// dispatch(get_love_letter_content(account))
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import LoveLetterDisplayComponent from '../components/loveletterdisplay';
import { get_love_letter_content } from '../actions/friend';

const mapStateToProps = (state) => ({
    loveLetterContent: state.love_letter_content
});

const mapDispatchToProps = (dispatch) => ({
    commitLoveLetter: (account, partner, content) => {
        console.log({ acocunt: account, partner: partner, content: content })
    },
    getLoveLetterContent: (account) => {
        console.log(account)
        dispatch(get_love_letter_content(account))
    }
}
);


class LoveLetterDisplayContainer extends LoveLetterDisplayComponent {
    constructor(props) {
        super(props);
        this.props.getLoveLetterContent(this.props.account)
        // this.state = {
        //     loveLetterContent: ''
        // }
    }
    // componentWillReceiveProps(nextProps) {
    //     const { loveLetterContent: previous_loveLetterContent } = this.props;
    //     const { loveLetterContent } = nextProps;
    //     if (previous_loveLetterContent != loveLetterContent) {
    //         this.setState({
    //             loveLetterContent: loveLetterContent
    //         })
    //     }
    // }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoveLetterDisplayContainer);
