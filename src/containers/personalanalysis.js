import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import PersonalAnalysisComponent from '../components/personalanalysis';
import { get_personal } from '../actions/analysis';
const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
    analysis: state.analysis
});

const mapDispatchToProps = (dispatch) => ({
    getPersonal: (account) => {
        dispatch(get_personal(account))
    },
    // postMessage: (account, partner, content) => {
    //     dispatch(post_message(account, partner, content));
    // }
});

class PersonalAnalysisContainer extends PersonalAnalysisComponent {
    constructor(props) {
        super(props);

        this.state = {
            data: {},
            legend: {
                enabled: true,
                textSize: 14,
                form: 'CIRCLE',
                wordWrapEnabled: true
            },
            num:10
        };
    }
    componentWillMount() {
        this.props.getPersonal(this.props.account);
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PersonalAnalysisContainer);
