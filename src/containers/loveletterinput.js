import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import LoveLetterInputComponent from '../components/loveletterinput';
import { sent_love_letter } from '../actions/friend';

const mapStateToProps = (state) => ({
    loveLetterStatusData: state.love_letter_status_data,
});

const mapDispatchToProps = (dispatch) => ({
    commitLoveLetter: (account, partner, content) => {
        console.log({ acocunt: account, partner: partner, content: content })
        dispatch(sent_love_letter(account, partner, content))
    }
}
);


class LoveLetterInputContainer extends LoveLetterInputComponent {
    constructor(props) {
        super(props);
    }
    componentWillReceiveProps(nextProps) {
        const { loveLetterStatusData: previous_loveLetterStatusData } = this.props;
        const { loveLetterStatusData } = nextProps;
        if (previous_loveLetterStatusData != loveLetterStatusData) {
            if (loveLetterStatusData.Status == true) {
                Actions.pop();
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoveLetterInputContainer);
