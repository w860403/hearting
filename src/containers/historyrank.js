import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import HistoryRankComponent from '../components/historyrank';
import { get_history_rank } from '../actions/member';


const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
    historyRank: state.history_rank
});

const mapDispatchToProps = (dispatch) => ({
    getHistoryRank: (account) => {
        dispatch(get_history_rank(account));
    },
}
);


class HistoryRankContainer extends HistoryRankComponent {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: false,
        }
    }
    componentWillMount() {
        this.props.getHistoryRank(this.props.account);
    }
    componentWillReceiveProps(nextProps) {
        const { historyRank: previous_historyRank } = this.props;
        const { historyRank } = nextProps;
        if (previous_historyRank != historyRank) {
            // if (login_status == true) {
            //     Actions.home();
            // }
            this.setState({
                animating: false, loaded: true,
                selectedOption: '隨機',
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryRankContainer);
