import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import LoginComponent from '../components/login';
import { login } from '../actions/login';


const mapStateToProps = (state) => ({
    login_status: state.login_status,
    login_data: state.login_data
});

const mapDispatchToProps = (dispatch) => ({
    onLoginButtonClick: (account, password) => {
        dispatch(login(account, password));
    },
}
);


class LoginContainer extends LoginComponent {
    constructor(props){
        super(props);
    }
    componentWillReceiveProps(nextProps) {
        const { login_data: previous_login_data } = this.props;
        const { login_data } = nextProps;
        const { login_status } = nextProps;        
        if (previous_login_data != login_data) {
            if (login_status == true) {
                Actions.pop();
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
