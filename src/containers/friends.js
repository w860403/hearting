import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import FriendsComponent from '../components/friends';
import { get_friend_list, delete_friend, get_love_letter_status, cancel_love_letter, get_love_letter_content, get_chat_list } from '../actions/friend';
import {
    ListView,
    AlertIOS
} from 'react-native';
const mapStateToProps = (state) => ({
    friendsData: state.friends_data,
    loveLetterStatusData: state.love_letter_status_data,
    cancelLoveLetterMessage: state.cancel_love_letter_message,
    account: state.login_data ? state.login_data : '',
    chatList:state.chat_list
});

const mapDispatchToProps = (dispatch) => ({
    getFriendList: (account) => {
        dispatch(get_friend_list(account))
    },
    deleteFriend: (account, partner) => {
        dispatch(delete_friend(account, partner))
    },
    getLoveLetterStatus: (account) => {
        dispatch(get_love_letter_status(account))
    },
    goToLoveLetterInput: (account, partner) => {
        //console.log({ account: account, partner: partner })
        Actions.loveletterinput({ account, partner })
    },
    cancelLoveLetter: (account, callback) => {
        dispatch(cancel_love_letter(account, callback))
    },
    goToLoveLetterDisplay: (account) => {
        //console.log({ account: account, partner: partner })
        // dispatch(get_love_letter_content(account))
        Actions.loveletterdisplay({ account: account })
        console.log('account=' + account)
        // Actions.loveletterdisplay({ content })
    },
    getChat: (partner, name) => {
        Actions.chat({ partner, name })
    },
    getChatList: (account) => {
        dispatch(get_chat_list(account))
    }
});

var timecounter2 = 0
class FriendsContainer extends FriendsComponent {
    constructor(props) {
        super(props)

        this.props.getFriendList(this.props.account)
        this.props.getLoveLetterStatus(this.props.account)
        this.props.getChatList(this.props.account)
        
    }
    componentDidMount() {
        this.timer2 = setInterval(
            () => {
                timecounter2++
                console.log('getChatList2' + timecounter2)
                this.props.getChatList(this.props.account)
            }, 10000)
    }
    componentWillUnmount() {
        console.log('stop2');
        timecounter2=0;
        clearTimeout(this.timer2);
    }
    componentWillReceiveProps(nextProps) {
        const { friendsData: previous_friendsData } = this.props;
        const { friendsData } = nextProps;
        const { loveLetterStatusData } = nextProps;
        this.setState({
            loveLetterStatusData: loveLetterStatusData
        }, function () {
            console.log('setLoveLetterStatusData')
            console.log(this.state.loveLetterStatusData)
        })
        if (previous_friendsData != friendsData) {
            console.log("componentWillReceiveProps:friendsData:")
            console.log(friendsData);
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.setState({
                dataSource: ds.cloneWithRows(friendsData),
                friendsData: friendsData,
                ds: ds,
            })
        }
        const { chatList: previous_chatList } = this.props;
        const { chatList } = nextProps;
        if (previous_chatList != chatList) {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.setState({
                chatListSource: ds.cloneWithRows(chatList),
                animating: false, loaded: true,
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FriendsContainer);
