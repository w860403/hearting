import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import HistoryQuizComponent from '../components/historyquiz';
import { get_history_quiz,get_history_quiz_detail } from '../actions/member';


const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
    history_quiz_random: state.history_quiz_random,
    history_quiz_back: state.history_quiz_back,
    history_quiz_angle: state.history_quiz_angle,
});

const mapDispatchToProps = (dispatch) => ({
    getHistoryQuiz: (account) => {
        dispatch(get_history_quiz(account));
    },
    getHistoryQuizDetail: (account,fun,date) => {
        dispatch(get_history_quiz_detail(account,fun,date));
        Actions.historyquizdetail();
    },
}
);


class HistoryQuizContainer extends HistoryQuizComponent {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: false,
        }
    }
    componentWillMount() {
        this.props.getHistoryQuiz(this.props.account);
    }
    componentWillReceiveProps(nextProps) {
        const { history_quiz_random: previous_history_quiz_random } = this.props;
        const { history_quiz_random } = nextProps;
        if (previous_history_quiz_random != history_quiz_random) {
            // if (login_status == true) {
            //     Actions.home();
            // }
            this.setState({
                animating: false, loaded: true,
                selectedOption: '隨機',
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryQuizContainer);
