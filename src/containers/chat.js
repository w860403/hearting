import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import ChatComponent from '../components/chat';
import { get_chat, post_message } from '../actions/friend';
const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
    chat: state.chat
});

const mapDispatchToProps = (dispatch) => ({
    getChat: (account, partner) => {
        dispatch(get_chat(account, partner))
    },
    postMessage: (account, partner, content) => {
        dispatch(post_message(account, partner, content));
    }
});

var timecounter = 0
class ChatContainer extends Component {
  
    componentWillMount() {
        this.props.getChat(this.props.account, this.props.partner);
    }
    componentDidMount() {
        // this._listView.scrollToEnd({animated: true})
        // this.refs._listView.scrollToEnd({ animated: true });
        // super.refs._listview.scrollToEnd({ animated: true })
        this.timer = setInterval(
            () => {
                timecounter++
                console.log('check connection delay ' + timecounter)
                this.props.getChat(this.props.account, this.props.partner)
            }, 3000)
    }
    componentWillUnmount() {
        console.log('stop');
        timecounter = 0;
        clearTimeout(this.timer);
    }
    // componentWillReceiveProps(nextProps) {
    //     const { chat: previous_chat } = this.props;
    //     const { chat } = nextProps;
    //     if (previous_chat != chat) {
    //         this.setState({
    //             animating: false, loaded: true,
    //         })
    //     }
    // }

    render() {
        return (<ChatComponent {...this.props} />);
    }
}

ChatContainer.propTypes = {
    getChat: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatContainer);
