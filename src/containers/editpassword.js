import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import EditPasswordComponent from '../components/editpassword';
import { edit_password } from '../actions/member';


const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
});

const mapDispatchToProps = (dispatch) => ({
    editPassword: (account) => {
        dispatch(edit_password(account));
    }
}
);


class EditPasswordContainer extends EditPasswordComponent {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentWillReceiveProps(nextProps) {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPasswordContainer);
