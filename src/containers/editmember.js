import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import EditMemberComponent from '../components/editmember';
import { edit_member_data, edit_member_info, get_member_info } from '../actions/member';
import { upload_image } from '../actions/register';


const mapStateToProps = (state) => ({
    memberData: state.member_data ? state.member_data : '',
    memberInfo: state.member_info ? state.member_info : '',

});

const mapDispatchToProps = (dispatch) => ({
    editMemberData: (data) => {
        dispatch(edit_member_data(data));
    },
    editMemberInfo: (data) => {
        dispatch(edit_member_info(data));
    },
    getMemberInfo: (account) => {
        dispatch(get_member_info(account));
    },
    oploadImage: (account, fileStr) => {
        dispatch(upload_image(account, fileStr));
    },
});

class EditMemberContainer extends EditMemberComponent {
    constructor(props) {
        super(props);
        this.sstate = {
            avatarSource: null,
            selectedOption: '基本資料',
            d1: '0',
            d2: '0',
            d3: '0',
            d4: '0',
            d5: '0',
            d6: '0',
            d7: '0',
            d8: '0',
            d9: '0',
            d10: '0',
            d0: '0',
            isD1: false,
            isD2: false,
            isD3: false,
            isD4: false,
            isD5: false,
            isD6: false,
            isD7: false,
            isD8: false,
            isD9: false,
            isD10: false,
            isD0: false,
            checkNum:0
        }
    }
    componentWillMount() {
        this.setState({ memberData: this.props.memberData });
        this.setState({ memberInfo: this.props.memberInfo });
        switch (this.props.memberInfo.M_Hairstyle) {
            case "0":
                this.setState({ M_Hairstyle_text: '微捲暖男造型' });
                break;
            case "1":
                this.setState({ M_Hairstyle_text: '二分區式造型' });
                break;
            case "2":
                this.setState({ M_Hairstyle_text: '後吹露額造型' });
                break;
            case "3":
                this.setState({ M_Hairstyle_text: '分線油頭造型' });
                break;
            case "4":
                this.setState({ M_Hairstyle_text: '平瀏海造型' });
                break;
            default:
                this.setState({ M_Hairstyle_text: '' });
                break;
        }
        switch (this.props.memberInfo.W_Hairstyle) {
            case "0":
                this.setState({ W_Hairstyle_text: '氣質長直髮' });
                break;
            case "1":
                this.setState({ W_Hairstyle_text: '知性中長髮' });
                break;
            case "2":
                this.setState({ W_Hairstyle_text: '讓漫長捲髮' });
                break;
            case "3":
                this.setState({ W_Hairstyle_text: '甜美及肩髮' });
                break;
            case "4":
                this.setState({ W_Hairstyle_text: '輕熟女短髮' });
                break;
            default:
                this.setState({ W_Hairstyle_text: '' });
                break;
        }
        switch (this.props.memberInfo.M_myStyle) {
            case "0":
                this.setState({ M_myStyle_text: '陽光帥氣型' });
                break;
            case "1":
                this.setState({ M_myStyle_text: '文藝青年型' });
                break;
            case "2":
                this.setState({ M_myStyle_text: '沈穩憨厚型' });
                break;
            case "3":
                this.setState({ M_myStyle_text: '成熟大叔型' });
                break;
            case "4":
                this.setState({ M_myStyle_text: '書生氣息型' });
                break;
            case "5":
                this.setState({ M_myStyle_text: '韓系型男型' });
                break;
            case "6":
                this.setState({ M_myStyle_text: '質感宅男型' });
                break;
            case "7":
                this.setState({ M_myStyle_text: '肌肉型男型' });
                break;
            default:
                this.setState({ M_myStyle_text: '' });
                break;
        }
        switch (this.props.memberInfo.W_myStyle) {
            case "0":
                this.setState({ W_myStyle_text: '甜美可愛型' });
                break;
            case "1":
                this.setState({ W_myStyle_text: '火辣性感型' });
                break;
            case "2":
                this.setState({ W_myStyle_text: '優雅氣質型' });
                break;
            case "3":
                this.setState({ W_myStyle_text: '健美陽光型' });
                break;
            case "4":
                this.setState({ W_myStyle_text: '個性男孩型' });
                break;
            case "5":
                this.setState({ W_myStyle_text: '棉花糖風型' });
                break;
            case "6":
                this.setState({ W_myStyle_text: '溫暖淳樸型' });
                break;
            case "7":
                this.setState({ W_myStyle_text: '文藝少女型' });
                break;
            default:
                this.setState({ W_myStyle_text: '' });
                break;
        }
        switch (this.props.memberInfo.M_Style) {
            case "0":
                this.setState({ M_Style_text: '陽光帥氣型' });
                break;
            case "1":
                this.setState({ M_Style_text: '文藝青年型' });
                break;
            case "2":
                this.setState({ M_Style_text: '沈穩憨厚型' });
                break;
            case "3":
                this.setState({ M_Style_text: '成熟大叔型' });
                break;
            case "4":
                this.setState({ M_Style_text: '書生氣息型' });
                break;
            case "5":
                this.setState({ M_Style_text: '韓系型男型' });
                break;
            case "6":
                this.setState({ M_Style_text: '質感宅男型' });
                break;
            case "7":
                this.setState({ M_Style_text: '肌肉型男型' });
                break;
            default:
                this.setState({ M_Style_text: '' });
                break;
        }
        switch (this.props.memberInfo.W_Style) {
            case "0":
                this.setState({ W_Style_text: '甜美可愛型' });
                break;
            case "1":
                this.setState({ W_Style_text: '火辣性感型' });
                break;
            case "2":
                this.setState({ W_Style_text: '優雅氣質型' });
                break;
            case "3":
                this.setState({ W_Style_text: '健美陽光型' });
                break;
            case "4":
                this.setState({ W_Style_text: '個性男孩型' });
                break;
            case "5":
                this.setState({ W_Style_text: '棉花糖風型' });
                break;
            case "6":
                this.setState({ W_Style_text: '溫暖淳樸型' });
                break;
            case "7":
                this.setState({ W_Style_text: '文藝少女型' });
                break;
            default:
                this.setState({ W_Style_text: '' });
                break;
        }
        switch (this.props.memberInfo.M_Stature) {
            case "0":
                this.setState({ M_Stature_text: '圓胖西洋梨型' });
                break;
            case "1":
                this.setState({ M_Stature_text: '精瘦小黃瓜型' });
                break;
            case "2":
                this.setState({ M_Stature_text: '肌肉磚塊型' });
                break;
            case "3":
                this.setState({ M_Stature_text: '扁寬倒三角形' });
                break;
            case "4":
                this.setState({ M_Stature_text: '乾扁豌豆型' });
                break;
            default:
                this.setState({ M_Stature_text: '' });
                break;
        }
        switch (this.props.memberInfo.W_Stature) {
            case "0":
                this.setState({ W_Stature_text: '曲線沙漏型' });
                break;
            case "1":
                this.setState({ W_Stature_text: '纖瘦骨感型' });
                break;
            case "2":
                this.setState({ W_Stature_text: '下身肉肉型' });
                break;
            case "3":
                this.setState({ W_Stature_text: '運動線條型' });
                break;
            case "4":
                this.setState({ W_Stature_text: '腰寬蘋果型' });
                break;
            default:
                this.setState({ W_Stature_text: '' });
                break;
        }
        switch (this.props.memberInfo.W_Height) {
            case "0":
                this.setState({ W_Height_text: '150以下' });
                break;
            case "1":
                this.setState({ W_Height_text: '150~155' });
                break;
            case "2":
                this.setState({ W_Height_text: '156~160' });
                break;
            case "3":
                this.setState({ W_Height_text: '161~165' });
                break;
            case "4":
                this.setState({ W_Height_text: '166~170' });
                break;
            case "5":
                this.setState({ W_Height_text: '170以上' });
                break;
            default:
                this.setState({ W_Height_text: '' });
                break;
        }
        switch (this.props.memberInfo.M_Height) {
            case "0":
                this.setState({ M_Height_text: '165以下' });
                break;
            case "1":
                this.setState({ M_Height_text: '165~170' });
                break;
            case "2":
                this.setState({ M_Height_text: '171~175' });
                break;
            case "3":
                this.setState({ M_Height_text: '176~180' });
                break;
            case "4":
                this.setState({ M_Height_text: '181~185' });
                break;
            case "5":
                this.setState({ M_Height_text: '185以上' });
                break;
            default:
                this.setState({ M_Height_text: '' });
                break;
        }
        switch (this.props.memberInfo.FirstSee) {
            case "0":
                this.setState({ FirstSee_text: '臉' });
                break;
            case "1":
                this.setState({ FirstSee_text: '胸' });
                break;
            case "2":
                this.setState({ FirstSee_text: '腿' });
                break;
            case "3":
                this.setState({ FirstSee_text: '其他' });
                break;
            default:
                this.setState({ FirstSee_text: '' });
                break;
        }
        switch (this.props.memberInfo.LikeAge) {
            case "0":
                this.setState({ LikeAge_text: '小五歲以上' });
                break;
            case "1":
                this.setState({ LikeAge_text: '小2~5歲' });
                break;
            case "2":
                this.setState({ LikeAge_text: '差兩歲左右' });
                break;
            case "3":
                this.setState({ LikeAge_text: '大2~5歲' });
                break;
            case "4":
                this.setState({ LikeAge_text: '大5歲以上' });
                break;
            default:
                this.setState({ LikeAge_text: '' });
                break;
        }
        switch (this.props.memberInfo.FaceType) {
            case "0":
                this.setState({ FaceType_text: '國字臉' });
                break;
            case "1":
                this.setState({ FaceType_text: '圓臉' });
                break;
            case "2":
                this.setState({ FaceType_text: '瓜子臉' });
                break;
            case "3":
                this.setState({ FaceType_text: '鵝蛋臉' });
                break;
            case "4":
                this.setState({ FaceType_text: '長臉' });
                break;
            default:
                this.setState({ FaceType_text: '' });
                break;
        }
        var array = this.props.memberData.Interest.split("");
        var num=0;
        var self=this;
        array.map(function (data, index) {
            if (data == "1") {
                num++;
                switch (index) {
                    case 0:
                        self.setState({ d1: "0", isD0:true});
                        break;
                    case 1:
                        self.setState({ d1: "1", isD1:true});                    
                        break;
                    case 2:
                        self.setState({ d2: "1", isD2:true});                    
                        break;
                    case 3:
                        self.setState({ d3: "1", isD3:true});                    
                        break;
                    case 4:
                        self.setState({ d4: "1", isD4:true});                    
                        break;
                    case 5:
                        self.setState({ d5: "1", isD5:true});                    
                        break;
                    case 6:
                        self.setState({ d6: "1", isD6:true});                    
                        break;
                    case 7:
                        self.setState({ d7: "1", isD7:true});                    
                        break;
                    case 8:
                        self.setState({ d8: "1", isD8:true});                    
                        break;
                    case 9:
                        self.setState({ d9: "1", isD9:true});                    
                        break;
                    case 10:
                        self.setState({ d10: "1", isD10:true});                    
                        break;
                    default:
                        break;
                }
            }
        })
        this.setState({checkNum:num})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMemberContainer);
