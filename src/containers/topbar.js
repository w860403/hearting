import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import TopBarComponent from '../components/topbar';
import MindWaveMobile from 'react-native-mindwave-mobile';
const mwm = new MindWaveMobile()

const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) => ({
    onBackButtonClick: (closeMindWaveListener) => {
        if (closeMindWaveListener) {
            mwm.disconnect()
            mwm.removeAllListeners()
            Actions.pop();
        } else {
            Actions.pop();
        }
    },
    onAddFriendButtonClick: () => {
        Actions.friendsrequest()
    }
}
);

export default connect(mapStateToProps, mapDispatchToProps)(TopBarComponent);