import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import SignAnalysisComponent from '../components/signAnalysis';
import { get_constellation } from '../actions/analysis';
import {
    processColor
} from 'react-native';

const mapStateToProps = (state) => ({
    analysis: state.analysis,
});

const mapDispatchToProps = (dispatch) => ({
    getAnalysis: (sex, constellation, type) => {
        dispatch(get_constellation(sex, constellation + "座", type));
    },
}
);


class SignAnalysisContainer extends SignAnalysisComponent {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: true,
            modalVisible: true,
            constellation: '摩羯',
            sex: '男',
            type: 'Hairstyle',
            other: '女',
            typeName: '髮型',
            isModal: false,
            data: {},
            legend: {
                enabled: true,
                textColor: processColor('blue'),
                textSize: 12,
                position: 'BELOW_CHART_RIGHT',
                form: 'SQUARE',
                formSize: 14,
                xEntrySpace: 20,
                yEntrySpace: 20,
                formToTextSpace: 10,
                wordWrapEnabled: true,
                maxSizePercent: 0.5,
                custom: {
                    colors: [processColor('green')],
                    labels: ['星座分析']
                }
            },
            marker: {
                enabled: true,
                backgroundTint: processColor('teal'),
                markerColor: processColor('#F0C0FF8C'),
                textColor: processColor('white'),
            }
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignAnalysisContainer);
