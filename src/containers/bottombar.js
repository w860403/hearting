import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import BottomBarComponent from '../components/bottombar';
import { on_button_click } from '../actions/bottombar';


const mapStateToProps = (state) => ({
    
});
const mapDispatchToProps = (dispatch) => ({
    onButtonClick: (newTabIndex) => {
        dispatch(on_button_click(newTabIndex));
    }
}
);

export default connect(mapStateToProps, mapDispatchToProps)(BottomBarComponent);
