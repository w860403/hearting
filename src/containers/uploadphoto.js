import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import UploadPhotoComponent from '../components/uploadphoto';
import { upload_test_image, get_now_image } from '../actions/member';


const mapStateToProps = (state) => ({
    imageType: state.save_image_data.imageType,
    account: state.login_data ? state.login_data : '',
    now_image: state.now_image,
});

const mapDispatchToProps = (dispatch) => ({
    oploadTestImage: (account, fun, imageType, fileStr) => {
        dispatch(upload_test_image(account, fun, imageType, fileStr));
    },

}
);


class UploadPhotoContainer extends UploadPhotoComponent {
    constructor(props) {
        super(props);
        var list = [{ index: 0, title: "正面" }, { index: 1, title: "背面" }, { index: 2, title: "正面" }];
        this.state = {
            json: [{ index: 0, title: "正面" }, { index: 1, title: "背面" }, { index: 2, title: "正面" }],
            selectedOption: '基本資料',
            slideIndex: 0,
            index: 0,
            length: list.length,
            imageArray: [],
            animating: true, loaded: false
        }
    }
    componentWillMount() {
        const array = [];
        var self=this;
        this.props.imageType.type.map(function (data, index) {
            if (self.props.now_image.length !== 0) {
                array.push({ uri: 'http://114.35.74.209/Hearting/Upload/' + self.props.now_image.ImgName });
            }
            else {
                array.push({ uri: 'http://114.35.74.209/Hearting/img/upload.png' });
            }

        })
        this.setState({
            imageArray: array, animating: false, loaded: true
        })
    }
    componentWillReceiveProps(nextProps) {
        const { now_image: previous_now_image } = this.props;
        const { now_image } = nextProps;
        var self=this;
        const array = [];
        if (previous_now_image != now_image) {
            now_image.map(function (data, index) {
                if (now_image.length !== 0) {
                    array.push({ uri: 'http://114.35.74.209/Hearting/Upload/' + data.ImgName });
                }
                else {
                    array.push({ uri: 'http://114.35.74.209/Hearting/img/upload.png' });
                }

            })
            this.setState({
                imageArray: array, animating: false, loaded: true
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadPhotoContainer);
