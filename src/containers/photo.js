import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import PhotoComponent from '../components/photo';

const mapStateToProps = (state) => ({
    member_photo_random: state.member_photo_random,
    member_photo_back: state.member_photo_back,
    member_photo_angle: state.member_photo_angle,
});

const mapDispatchToProps = (dispatch) => ({
}
);
class PhotoContainer extends PhotoComponent {
    constructor(props) {
        super(props);
        this.state = {
            random: { fun: 'randomTest', type: [{ index: 0, title: 'random' }] },
            back: { fun: 'backTest', type: [{ index: 0, title: 'positive' }, { index: 1, title: 'back' }] },
            angle: { fun: 'angleTest', type: [{ index: 0, title: 'positive' }, { index: 1, title: 'right' }, { index: 2, title: 'left' }] },

        }
    }
    componentWillMount() {
        switch (this.props.type) {
            case 'random':
                this.setState({ photoList: this.props.member_photo_random });
                break;
            case 'back':
                this.setState({ photoList: this.props.member_photo_back });
                break;
            case 'angle':
                this.setState({ photoList: this.props.member_photo_angle });
                break;
            default:
                break;
        }
    }
    componentWillReceiveProps(nextProps) {
        const { memberData: previous_memberData } = this.props;
        const { memberData } = nextProps;
        if (previous_memberData != memberData) {
            // if (login_status == true) {
            //     Actions.home();
            // }
            this.setState({ animating: false, loaded: true });
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PhotoContainer);