import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import NoLoginComponent from '../components/nologin';

const mapDispatchToProps = (dispatch) => ({
    onLoginButtonClick: () => {
        Actions.login();
    },
    onRegisterButtonClick: () => {
        Actions.register();
    },
}
);

export default connect(mapDispatchToProps)(NoLoginComponent);
