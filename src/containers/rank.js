import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import RankComponent from '../components/rank';

const mapStateToProps = (state) => ({
    rank_girl:state.rank_girl,
    rank_boy:state.rank_boy,
});

const mapDispatchToProps = (dispatch) => ({
}
);
class RankContainer extends RankComponent {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: '隨機',
            slideIndex: 0,
        }
    }
    componentWillMount() {
    }
    componentWillReceiveProps(nextProps) {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RankContainer);