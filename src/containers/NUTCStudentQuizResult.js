import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import NUTCStudentQuizResultComponent from '../components/NUTCStudentQuizResult';
import {} from '../actions/imagequiz';

const mapStateToProps = (state) => ({
    saveCharmStatus: state.save_charm_status,
    account: state.login_data ? state.login_data : '',
});

const mapDispatchToProps = (dispatch) => ({
}
);
class NUTCStudentQuizResultContainer extends NUTCStudentQuizResultComponent {
    constructor(props) {
        super(props);
    }
    // componentWillReceiveProps(nextProps) {
    //     const previous_saveCharmStatus = this.props.saveCharmStatus
    //     const saveCharmStatus = nextProps.saveCharmStatus
    //     if (saveCharmStatus !== previous_saveCharmStatus) {
    //         if (saveCharmStatus) {
    //             this.setState({ modalVisible: false })
    //         }
    //     }
    // }
}
export default connect(mapStateToProps, mapDispatchToProps)(NUTCStudentQuizResultContainer);
