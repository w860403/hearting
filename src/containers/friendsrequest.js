import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import FriendsRequestComponent from '../components/friendsrequest';
import { get_friend_request, reply_invation, get_friend_list } from '../actions/friend';
import {
    ListView
} from 'react-native';
const mapStateToProps = (state) => ({
    friendsRequestData: state.friends_request_data,
    account: state.login_data ? state.login_data : '',
});

const mapDispatchToProps = (dispatch) => ({
    getFriendList: (account) => {
        dispatch(get_friend_list(account))
    },
    getFriendRequest: (account) => {
        dispatch(get_friend_request(account))
    },
    replyInvationAccept: (account, partner) => {
        // console.log({ Account: account, Partner: partner })
        dispatch(reply_invation(account, partner, 1))
    },
    replyInvationRefuse: (account, partner) => {
        dispatch(reply_invation(account, partner, 0))
    }
});


class FriendsRequestContainer extends FriendsRequestComponent {
    constructor(props) {
        super(props)
        this.props.getFriendRequest(this.props.account)
    }

    componentWillReceiveProps(nextProps) {
        const { friendsRequestData: previous_friendsRequestData } = this.props;
        const { friendsRequestData } = nextProps;
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.setState({
            dataSource: ds.cloneWithRows(friendsRequestData),
            friendsRequestData: friendsRequestData,
        })
        if (previous_friendsRequestData != friendsRequestData) {
            console.log("componentWillReceiveProps:friendsRequestData:")
            console.log(friendsRequestData);
            this.setState({
                friendsRequestData: friendsRequestData,
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FriendsRequestContainer);
