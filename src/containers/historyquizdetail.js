import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import HistoryQuizDetailComponent from '../components/historyquizdetail';
import { get_history_quiz,get_history_quiz_detail } from '../actions/member';


const mapStateToProps = (state) => ({
    account: state.login_data ? state.login_data : '',
    history_quiz_detail: state.history_quiz_detail,
});

const mapDispatchToProps = (dispatch) => ({
}
);


class HistoryQuizContainer extends HistoryQuizDetailComponent {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: false,
        }
    }
    componentWillReceiveProps(nextProps) {
        const { history_quiz_detail: previous_history_quiz_detail } = this.props;
        const { history_quiz_detail } = nextProps;
        if (previous_history_quiz_detail != history_quiz_detail) {
            this.setState({
                animating: false, loaded: true,
                selectedOption: '隨機',index:0 ,score:history_quiz_detail[0].Score,
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryQuizContainer);
