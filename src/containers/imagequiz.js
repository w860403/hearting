import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import ImageQuizComponent from '../components/imagequiz10s';
import MindWaveMobile from 'react-native-mindwave-mobile';
import { get_image, get_quiz_point, save_quiz_history } from '../actions/imagequiz';
import { on_eeg_power_delta, on_eeg_power_low_beta, on_esense } from '../actions/mindwave';
const mwm = new MindWaveMobile()

const mapStateToProps = (state) => ({
    delta: state.eeg_power_delta.delta,
    highAlpha: state.eeg_power_delta.highAlpha,
    lowAplpha: state.eeg_power_delta.lowAplpha,
    theta: state.eeg_power_delta.theta,
    lowBeta: state.eeg_power_low_beta.lowBeta,
    midGamma: state.eeg_power_low_beta.midGamma,
    highBeta: state.eeg_power_low_beta.highBeta,
    lowGamma: state.eeg_power_low_beta.lowGamma,
    poorSignal: state.esense.poorSignal,
    mindwaveTimer: state.eeg_power_delta.mindwaveTimer,
    quizPointArray: state.quiz_point_array,
    quizResultData: state.quiz_result_data,
    imageDataArray: state.image_data_array,
    account: state.login_data ? state.login_data : '',
    sex: state.login_sex ? state.login_sex : ''
});

const mapDispatchToProps = (dispatch) => ({
    onEEGPowerDelta: (data, mindwaveTimer) => {
        dispatch(on_eeg_power_delta(data.delta, data.highAlpha, data.lowAplpha, data.theta, mindwaveTimer));
    },
    onEEGPowerLowBeta: (data) => {
        dispatch(on_eeg_power_low_beta(data.lowBeta, data.midGamma, data.highBeta, data.lowGamma));
    },
    onESense: (data) => {
        dispatch(on_esense(data.poorSignal));
    },
    goToImageTest: (account, quizFunction) => {
        dispatch(get_image(account, quizFunction));
    },
    getQuizPoint: (mindwaveData, imageArray, account, imageNumber, sex) => {
        console.log('account=' + account);
        console.log('sex=' + sex);
        dispatch(get_quiz_point(mindwaveData, imageArray, account, imageNumber, sex));
    },
}
);

export default connect(mapStateToProps, mapDispatchToProps)(
    ImageQuizComponent
);
