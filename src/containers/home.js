import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import HomeComponent from '../components/home';
import { get_rank } from '../actions/home';


const mapStateToProps = (state) => ({
    login_status: state.login_status,
    login_data: state.login_data,
    rank_girl:state.rank_girl,
    rank_boy:state.rank_boy,
});

const mapDispatchToProps = (dispatch) => ({
    getRank:()=>{
        dispatch(get_rank());
    },
    onRankButtonClick:(sex)=>{
        Actions.rank({sex:sex});
    },
    onSignButtonClick:()=>{
        Actions.signanalysis();
    },
}
);


class HomeContainer extends HomeComponent {
    constructor(props){
        super(props);
    }
    componentWillMount() {
        this.props.getRank();
    }
    componentWillReceiveProps(nextProps) {
        const { rank_girl: previous_rank_girl } = this.props;
        const { rank_girl } = nextProps;
        if (previous_rank_girl != rank_girl) {
            this.setState({
                animating: false, loaded: true,
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
