import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import MemberComponent from '../components/member';
import { save_upload_image_data, get_member_data, get_member_info, get_member_photo, get_now_image, logout } from '../actions/member';
import moment from 'moment';

const mapStateToProps = (state) => ({
    memberData: state.member_data ? state.member_data : '',
    memberInfo: state.member_info ? state.member_info : '',
    account: state.login_data ? state.login_data : '',
    member_photo_random: state.member_photo_random,
    member_photo_back: state.member_photo_back,
    member_photo_angle: state.member_photo_angle,
});

const mapDispatchToProps = (dispatch) => ({
    onEditMemberButtonClick: () => {
        Actions.editmember();
    },
    onUploadPhotoButtonClick: (image) => {
        dispatch(save_upload_image_data(image));
        Actions.uploadphoto();
    },
    onPhotoButtonClick: (image) => {
        Actions.uploadphoto();
    },
    getNowImage: (account, fun) => {
        dispatch(get_now_image(account, fun));
    },
    onHistoryQuizButtonClick: () => {
        Actions.historyquiz();
    },
    onHistoryRankButtonClick: () => {
        Actions.historyrank();
    },
    onPhotokButtonClick: (type) => {
        Actions.photo({ testType: type });
    },
    getMemberData: (account) => {
        dispatch(get_member_data(account));
    },
    getMemberInfo: (account) => {
        dispatch(get_member_info(account));
    },
    getMemberPhoto: (account) => {
        dispatch(get_member_photo(account));
    },
    logout: () => {
        dispatch(logout());
    }
}
);


class MemberContainer extends MemberComponent {
    constructor(props) {
        super(props);
        this.state = {
            random: { fun: 'randomTest', type: [{ index: 0, title: 'random' }] },
            back: { fun: 'backTest', type: [{ index: 0, title: 'positive' }, { index: 1, title: 'back' }] },
            angle: { fun: 'angleTest', type: [{ index: 0, title: 'positive' }, { index: 1, title: 'right' }, { index: 2, title: 'left' }] },
            year: '',
            month: '',
            date: ''
        }
    }
    componentWillMount() {
        this.props.getMemberData(this.props.account);
        this.props.getMemberInfo(this.props.account);
        this.props.getMemberPhoto(this.props.account);
    }
    componentWillReceiveProps(nextProps) {
        const { memberData: previous_memberData } = this.props;
        const { memberData } = nextProps;
        if (previous_memberData != memberData) {
            this.setState({
                animating: false,
                loaded: true,
                date: moment(memberData.Birthday).format('YYYY-MM-DD')
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemberContainer);
