import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import QuizResultComponent from '../components/quizresult';
import { send_invitation, save_as_charm } from '../actions/imagequiz';

const mapStateToProps = (state) => ({
    saveCharmStatus: state.save_charm_status,
    account: state.login_data ? state.login_data : '',
});

const mapDispatchToProps = (dispatch) => ({
    sendInvitation: (account, partner) => {
        dispatch(send_invitation(account, partner));
    },
    on_quiz_button_click: () => {
        Actions.checkmindwaveconnect();
    },
    saveAsCharm: (charmList, sex) => {
        dispatch(save_as_charm(charmList, sex));
    }
}
);
class QuizResultContainer extends QuizResultComponent {
    constructor(props) {
        super(props);
    }
    // componentWillReceiveProps(nextProps) {
    //     const previous_saveCharmStatus = this.props.saveCharmStatus
    //     const saveCharmStatus = nextProps.saveCharmStatus
    //     if (saveCharmStatus !== previous_saveCharmStatus) {
    //         if (saveCharmStatus) {
    //             this.setState({ modalVisible: false })
    //         }
    //     }
    // }
}
export default connect(mapStateToProps, mapDispatchToProps)(QuizResultContainer);
