import React, { Component, PropTypes } from "react";
import {
    PickerIOS,
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Modal,
    Dimensions,
    Image
} from "react-native";

import PickerAndroid, { PickerItemAndroid } from './androidimagepicker';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

let _Picker = Platform.OS === 'ios' ? PickerIOS : PickerAndroid;
let _PickerItem = Platform.OS === 'ios' ? _Picker.Item : PickerItemAndroid;

class ImagePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selectedOption: this.props.options.filter((op) => op.key === this.props.defaultSelectedValue)[0] || {},
            selectedImage: this.props.options.filter((op) => op.key === this.props.defaultSelectedValue)[0] || '',
            selectedFolder: this.props.options.filter((op) => op.key === this.props.defaultSelectedValue)[0] || '',
        }
    }

    static propTypes = {
        options: PropTypes.array.isRequired,
        defaultSelectedValue: PropTypes.any,
        onConfirm: PropTypes.func,
        onSelect: PropTypes.func,
        onCancel: PropTypes.func,
        style: PropTypes.object
    };

    static defaultProps = {
        style: { backgroundColor: "white" },
        onConfirm: () => {
        },
        onSelect: () => {
        },
        onCancel: () => {
        }
    };

    show() {
        this.setState(Object.assign({}, this.state, { modalVisible: true }));
    }

    hide() {
        this.setState({ modalVisible: false });
    }

    render() {
        return (
            <Modal
                animationType={'slide'}
                transparent={true}
                onRequestClose={() => { }}
                visible={this.state.modalVisible}>
                <View style={{ flex: 1, backgroundColor: 'rgba(52,52,52,0.5)' }}>
                    {
                        (this.state.selectedImage === '' || this.state.selectedFolder === '') ?
                            <View></View>
                            :
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 100 }}>
                                <Image source={{ uri: `http://114.35.74.209/Hearting/img/${this.state.selectedFolder}` + this.state.selectedImage }} style={styles.square} />
                                <Text>{`http://114.35.74.209/Hearting/img/${this.state.selectedFolder}` + this.state.selectedImage}</Text>
                            </View>
                    }

                    <View style={styles.basicContainer}>
                        <View style={[styles.modalContainer, this.props.style]}>
                            <View style={{ backgroundColor: '#CACACA', height: 0.5, width: SCREEN_WIDTH }} />
                            <View style={styles.buttonView}>
                                <TouchableOpacity style={[styles.button, styles.buttonLeft]} onPress={() => {
                                    this.props.onCancel();
                                    this.setState({ modalVisible: false });
                                }}>
                                    <Text style={{ marginLeft: 10 }}>取消</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.button, styles.buttonRight]} onPress={() => {
                                    if (this.props.onConfirm) {
                                        if (!this.state.selectedOption.key && this.state.selectedOption.key !== 0) {
                                            let submitData = this.props.options[0] || {};
                                            if (!submitData && this.props.defaultSelectedValue) {
                                                submitData = this.props.options.filter((op) => op.key === this.props.defaultSelectedValue)[0];
                                            }
                                            this.props.onConfirm(submitData);
                                        } else {
                                            this.props.onConfirm(this.state.selectedOption);
                                        }
                                    }
                                    this.setState({ modalVisible: false });
                                }}>
                                    <Text style={{ marginRight: 10 }}>確認</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ backgroundColor: '#CACACA', height: 0.5, width: SCREEN_WIDTH }} />
                            <View style={styles.mainBox}>
                                <_Picker
                                    ref={'picker'}
                                    style={styles.bottomPicker}
                                    selectedValue={(this.state.selectedOption.key || this.state.selectedOption === 0) ? this.state.selectedOption.key : this.props.defaultSelectedValue}
                                    onValueChange={val => {
                                        let curOption = this.props.options.filter((op) => op.key === val)[0];
                                        this.props.onSelect(curOption);
                                        this.setState(
                                            Object.assign({}, this.state, { selectedOption: curOption, selectedImage: curOption.image, selectedFolder: curOption.folder }));
                                    }}>
                                    {this.props.options.map((option, i) => {
                                        return (
                                            <_PickerItem
                                                key={i}
                                                value={option.key}
                                                label={option.value}
                                            />
                                        )
                                    })}
                                </_Picker>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

var styles = StyleSheet.create({
    basicContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    modalContainer: {
        width: SCREEN_WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0
    },
    buttonView: {
        height: 30,
        width: SCREEN_WIDTH,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    bottomPicker: {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT / 3 * 1,
    },
    mainBox: {},
    button: {
        flex: 0.5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonLeft: {
        justifyContent: 'flex-start'
    },
    buttonRight: {
        justifyContent: 'flex-end'
    },
    square: {
        height: SCREEN_WIDTH * 0.7,
        width: SCREEN_WIDTH * 0.7,
        borderWidth: 3,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
});
export default ImagePicker;
