import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Entypo';
import CheckBox from 'react-native-icon-checkbox';

class Doomed extends Component {
    constructor() {
        super();
    }
    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
        return (
            <View style={styles.bigview}>
                <View style={contentStyle.content}>
                    <View style={contentStyle.titleViw}>
                        <Text style={contentStyle.titleText}>命中注定</Text>
                        <Text style={contentStyle.titleText}>小遊戲</Text>
                    </View>
                    <View style={contentStyle.discribeView}>
                        <Text style={contentStyle.discribe}>快來尋找，</Text>
                        <Text style={contentStyle.discribe}>屬於你命中注定的好友吧！</Text>
                    </View>
                    <View style={contentStyle.startBtn}>
                        <TouchableOpacity>
                            <Text style={{ color: 'white', fontSize: 20 }}>開始</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <CheckBox
                            label="開啟命中注定配對功能"
                            size={35}
                            checked={false}
                        />
                    </View>
                    <View style={contentStyle.memoView}>
                        <Text style={contentStyle.memo}>開啟此功能，</Text>
                        <Text style={contentStyle.memo}>就能和命中注定的人加為好友呦！</Text>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        // alignItems: 'center',
    },

});
const contentStyle = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleViw: {
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    titleText: {
        fontSize: 30,
        marginBottom: 10
    },
    startBtn: {
        paddingLeft: width * 0.04,
        paddingRight: width * 0.04,
        paddingTop: width * 0.02,
        paddingBottom: width * 0.02,
        backgroundColor: 'black',
        marginTop: 40,
        marginBottom: 30,
    },
    memo: {
        color: 'red',
        marginBottom: 10
    },
    memoView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    discribeView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
        marginTop: 30
    },
    discribe: {
        color: 'black',
        marginBottom: 10,
        fontSize: 18
    }
})

export default Doomed;

