import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    NavigatorIOS,
    AsyncStorage,
    AlertIOS,
    ActivityIndicator,
} from 'react-native';
var { height, width } = Dimensions.get('window');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Account: '',
            Password: '',
            animating: true,
            loaded: false,
            loginSuccess: false
        };
    }
    render() {
        return (
            <View style={styles.bigview}>
                <Text style={styles.loginText}>
                    Login
                        </Text>
                <View>
                    <TextInput
                        placeholder="UserName..."
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Account) => this.setState({ Account })}
                        value={this.state.Account}
                        autoCapitalize="none"
                    />
                </View>
                <View style={styles.space}></View>
                <View>
                    <TextInput
                        placeholder="Password..."
                        placeholderTextColor="gray"
                        style={styles.textInput}
                        onChangeText={(Password) => this.setState({ Password })}
                        value={this.state.Password}
                        autoCapitalize="none"
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.button}>
                    <Button 
                        onPress={() => {
                            if (this.state.Account == '' || this.state.Password == '') {
                                alert('請輸入正確！！');
                            } else {
                                this.props.onLoginButtonClick(this.state.Account, this.state.Password)
                            }
                        }}
                        title="Sign In"
                        color="white"
                    >
                    </Button>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    loginText: {
        fontSize: 40,
        textAlign: 'center',
        margin: 20,
        fontFamily: 'Euphemia UCAS',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    bigview: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    textInput: {
        width: 200,
        height: 30,
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',
    },
    space: {
        height: 20,
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'black',
        marginTop: 20,
    },
    error_text: {
        color: '#FF3333'
    }
});
export default Login;
