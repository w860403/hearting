import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  PixelRatio,
  AlertIOS,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import SinglePicker from 'mkp-react-native-picker';
import ImagePicker from './imagepicker';
import ImagePicker2 from 'react-native-image-picker';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
class Register extends Component {
  constructor(props) {
    super(props);
    let now = new Date();
    this.state = {
      selectedOption: '帳號密碼',
      slideIndex: 0,
      Account: "",
      "Password": "",
      "Name": "",
      "Age": 0,
      Birthday: now,
      "Sex": "",
      "Aptitude": "",
      "Email": "",
      "Constellation": "",
      "Blood": "",
      "Height": 0,
      "Weight": 0,
      "Address": "",
      "Image": "",
      "isLove": "",
      avatarSource: null
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectedOption == '基本資料' && this.state.slideIndex !== 1) {
      this._carousel.snapToItem(1)
      console.log('選擇 基本資料')
    } else if (this.state.selectedOption == '個人喜好' && this.state.slideIndex !== 2) {
      this._carousel.snapToItem(2)
      console.log('選擇 個人喜好')
    } else if (this.state.selectedOption == '帳號密碼' && this.state.slideIndex !== 0) {
      this._carousel.snapToItem(0)
      console.log('選擇 帳號密碼')
    }
  }
  //判斷星座
  _Constellation = (date) => {
    var birthday = new Date(date);
    var year = birthday.getFullYear();
    var mouth = birthday.getMonth();
    var day = birthday.getDay();
    this.setState({ Age: 2017 - year });
    if (mouth === 0) {
      if (day < 21) {
        this.setState({ Constellation: "摩羯座" });
      }
      else {
        this.setState({ Constellation: "水瓶座" });
      }
    } else if (mouth === 1) {
      if (day < 21) {
        this.setState({ Constellation: "水瓶座" });
      }
      else {
        this.setState({ Constellation: "雙魚座" });
      }

    } else if (mouth === 2) {
      if (day < 21) {
        this.setState({ Constellation: "雙魚座" });
      }
      else {
        this.setState({ Constellation: "牡羊座" });
      }

    } else if (mouth === 3) {
      if (day < 21) {
        this.setState({ Constellation: "牡羊座" });
      }
      else {
        this.setState({ Constellation: "金牛座" });
      }

    } else if (mouth === 4) {
      if (day < 21) {
        this.setState({ Constellation: "金牛座" });
      }
      else {
        this.setState({ Constellation: "雙子座" });
      }

    } else if (mouth === 5) {
      if (day < 21) {
        this.setState({ Constellation: "雙子座" });
      }
      else {
        this.setState({ Constellation: "巨蟹座" });
      }
    } else if (mouth === 6) {
      if (day < 21) {
        this.setState({ Constellation: "巨蟹座" });
      }
      else {
        this.setState({ Constellation: "獅子座" });
      }
    } else if (mouth === 7) {
      if (day < 21) {
        this.setState({ Constellation: "獅子座" });
      }
      else {
        this.setState({ Constellation: "處女座" });
      }
    } else if (mouth === 8) {
      if (day < 21) {
        this.setState({ Constellation: "處女座" });
      }
      else {
        this.setState({ Constellation: "天枰座" });
      }
    } else if (mouth === 9) {
      if (day < 21) {
        this.setState({ Constellation: "天枰座" });
      }
      else {
        this.setState({ Constellation: "天蠍座" });
      }
    } else if (mouth === 10) {
      if (day < 21) {
        this.setState({ Constellation: "天蠍座" });
      }
      else {
        this.setState({ Constellation: "射手座" });
      }
    } else if (mouth === 11) {
      if (day < 21) {
        this.setState({ Constellation: "射手座" });
      }
      else {
        this.setState({ Constellation: "摩羯座" });
      }
    }
  }
  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker2.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.setState({
          avatarSource: source,
          imageData: response.data,
          image: response
        });
      }
      this.props.oploadImage(this.state.Account, this.state.imageData)
    });
  }
  //檢查密碼
  _checkPassword(password) {
    if (password !== this.state.Password) {
      return (
        AlertIOS.alert(
          "警告",
          "密碼不一致",
          [
            { text: 'OK', onPress: () => { this.setState({ Password: '', checkPassword: '' }) } },
          ]
        )
      );
    }
  }
  render() {
    const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
    const camera = (<Icon name="camera" size={30} color="gray" />)
    const address = [
      { key: 0, value: "台北市" },
      { key: 1, value: "新北市" },
      { key: 2, value: "桃園市" },
      { key: 3, value: "新竹市" },
      { key: 4, value: "新竹縣" },
      { key: 5, value: "苗栗縣" },
      { key: 6, value: "台中市" },
      { key: 7, value: "彰化縣" },
      { key: 8, value: "南投縣" },
      { key: 9, value: "雲林縣" },
      { key: 10, value: "嘉義市" },
      { key: 11, value: "嘉義縣" },
      { key: 12, value: "嘉義縣" },
      { key: 13, value: "台南市" },
      { key: 14, value: "高雄市" },
      { key: 15, value: "屏東縣" },
      { key: 16, value: "宜蘭縣" },
      { key: 17, value: "基隆市" },
      { key: 18, value: "花蓮縣" },
      { key: 19, value: "台東縣" },
      { key: 20, value: "澎湖縣" },
      { key: 21, value: "金門縣" },
      { key: 22, value: "連江縣" }
    ];
    const blood = [
      { key: 0, value: "A" },
      { key: 1, value: "B" },
      { key: 2, value: "O" },
      { key: 3, value: "AB" }
    ];
    const M_Hairstyle = [
      { key: 0, value: "==請選擇==", image: '', folder: '' },
      { key: 1, value: "微捲暖男造型", image: '1.jpg', folder: '男生髮型/' },
      { key: 2, value: "二分區式造型", image: '2.jpg', folder: '男生髮型/' },
      { key: 3, value: "後吹露額造型", image: '3.png', folder: '男生髮型/' },
      { key: 4, value: "分線油頭造型", image: '4.jpg', folder: '男生髮型/' },
      { key: 5, value: "平瀏海造型", image: '5.jpg', folder: '男生髮型/' }
    ];
    const W_Hairstyle = [
      { key: 0, value: "==請選擇==", image: '1.jpg', folder: '男生髮型/' },
      { key: 0, value: "氣質長直髮", image: '1.jpg', folder: '女生髮型/' },
      { key: 1, value: "知性中長髮", image: '2.jpg', folder: '女生髮型/' },
      { key: 2, value: "讓漫長捲髮", image: '3.jpg', folder: '女生髮型/' },
      { key: 3, value: "甜美及肩髮", image: '4.jpg', folder: '女生髮型/' },
      { key: 4, value: "輕熟女短髮", image: '5.jpg', folder: '女生髮型/' }
    ];
    const isLove = [
      { key: 0, value: "單身" },
      { key: 1, value: "一言難盡" },
      { key: 2, value: "穩定交往" },
      { key: 3, value: "已婚" }
    ];
    const sex = [
      { key: 0, value: "男", },
      { key: 1, value: "女", }
    ];
    const aptitude = [
      { key: 0, value: "男" },
      { key: 1, value: "女" },
      { key: 2, value: "雙性" }
    ];
    const M_myStyle = [
      { key: 0, value: "陽光帥氣型" },
      { key: 1, value: "文藝青年型" },
      { key: 2, value: "沈穩憨厚型" },
      { key: 3, value: "成熟大叔型" },
      { key: 4, value: "書生氣息型" },
      { key: 5, value: "韓系型男型" },
      { key: 6, value: "質感宅男型" },
      { key: 7, value: "肌肉型男型" }
    ];
    const W_myStyle = [
      { key: 0, value: "甜美可愛型" },
      { key: 1, value: "火辣性感型" },
      { key: 2, value: "優雅氣質型" },
      { key: 3, value: "健美陽光型" },
      { key: 4, value: "個性男孩型" },
      { key: 5, value: "棉花糖風型" },
      { key: 6, value: "溫暖淳樸型" },
      { key: 7, value: "文藝少女型" }
    ];
    const W_Headhair = [
      { key: 0, value: "==請選擇==", image: '', folder: '' },
      { key: 1, value: "咩咩頭", image: '1.jpg', folder: '女生瀏海/' },
      { key: 2, value: "斜劉海", image: '2.jpg', folder: '女生瀏海/' },
      { key: 3, value: "中分", image: '3.jpg', folder: '女生瀏海/' },
      { key: 4, value: "旁分", image: '4.jpg', folder: '女生瀏海/' },
      { key: 5, value: "空氣瀏海", image: '5.jpg', folder: '女生瀏海/' },
      { key: 6, value: "allback", image: '6.png', folder: '女生瀏海/' },
      { key: 7, value: "無", image: '', folder: '' }
    ];
    const M_Stature = [
      { value: '圓胖西洋梨型', key: 0 },
      { value: '精瘦小黃瓜型', key: 1 },
      { value: '肌肉磚塊型', key: 2 },
      { value: '扁寬倒三角形', key: 3 },
      { value: '乾扁豌豆型', key: 4 },
    ];
    const W_Stature = [
      { value: '曲線沙漏型', key: 0 },
      { value: '纖瘦骨感型', key: 1 },
      { value: '下身肉肉型', key: 2 },
      { value: '運動線條型', key: 3 },
      { value: '腰寬蘋果型', key: 4 },
    ];
    const W_Height = [
      { value: '150以下', key: 0 },
      { value: '150~155', key: 1 },
      { value: '156~160', key: 2 },
      { value: '161~165', key: 3 },
      { value: '166~170', key: 4 },
      { value: '170以上', key: 5 },
    ];
    const M_Height = [
      { value: '165以下', key: 0 },
      { value: '165~170', key: 1 },
      { value: '171~175', key: 2 },
      { value: '176~180', key: 3 },
      { value: '181~185', key: 4 },
      { value: '185以上', key: 5 },
    ];
    const FirstSee = [
      { value: '臉', key: 0 },
      { value: '胸', key: 1 },
      { value: '腿', key: 2 },
      { value: '其他', key: 3 },
    ];
    const LikeAge = [
      { value: '小五歲以上', key: 0 },
      { value: '小2~5歲', key: 1 },
      { value: '差兩歲左右', key: 2 },
      { value: '大2~5歲', key: 3 },
      { value: '大5歲以上', key: 4 },
    ];
    var FaceType = [
      { key: 0, value: "==請選擇==", image: '', folder: '' },
      { value: '國字臉', key: 1, image: '1.png', folder: '臉型/' },
      { value: '圓臉', key: 2, image: '2.png', folder: '臉型/' },
      { value: '瓜子臉', key: 3, image: '3.png', folder: '臉型/' },
      { value: '鵝蛋臉', key: 4, image: '4.png', folder: '臉型/' },
      { value: '長臉', key: 5, image: '5.png', folder: '臉型/' },
    ];
    const options = [
      '帳號密碼',
      '基本資料',
      '個人喜好'
    ];
    const image = [{ image: '3-randomTest-random-1.png' }, { image: '1-randomTest-random-1.png' }]
    function setSelectedOption(selectedOption) {
      this.setState({
        selectedOption: selectedOption
      });
    }
    function renderOption(option, selected, onSelect, index) {
      const style = selected ? { fontWeight: 'bold' } : {};
      return (
        <TouchableWithoutFeedback onPress={onSelect} key={index}>
          <Text style={style}>{option}</Text>
        </TouchableWithoutFeedback>
      );
    }

    function renderContainer(optionNodes) {

      return <View>{optionNodes}</View>;
    }
    return (
      <View style={styles.bigview}>
        <View style={styles.toparea}>
          <SegmentedControls
            tint={'rgba(52, 52, 52, 0.8)'}
            selectedTint={'#FFFFFF'}
            selectedBackgroundColor={'rgba(52, 52, 52, 0.8)'}
            backTint={'rgba(52, 52, 52, 0)'}
            options={options}
            allowFontScaling={false} // default: true
            onSelection={setSelectedOption.bind(this)}
            selectedOption={this.state.selectedOption}
            optionStyles={{ fontFamily: 'AvenirNext-Medium' }}
            optionContainerStyle={{ flex: 1 }}
          />
        </View>
        <Carousel
          ref={(carousel) => { this._carousel = carousel; }}
          sliderWidth={width}
          itemWidth={width}
          directionalLockEnabled={true}
          centerContent={true}
          // horizontal={true}
          currentIndex={this.state.currentIndex}
          onSnapToItem={(item) => {
            if (item === 0) {
              console.log('0')
              this.setState({
                selectedOption: '帳號密碼',
                slideIndex: 0
              });
            } else if (item === 1) {
              console.log('1')
              this.setState({
                selectedOption: '基本資料',
                slideIndex: 1
              });
            }
            else if (item === 2) {
              console.log('2')
              this.setState({
                selectedOption: '個人喜好',
                slideIndex: 2
              });
            }
          }}>
          <View style={{ padding: width * 0.12, width: width, height: 500, }}>
            <View style={styles.content}>
              <Text style={styles.columnText}>帳號</Text>
              <TextInput
                placeholder="帳號..."
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(Account) => this.setState({ Account })}
                autoCapitalize="none"
                onEndEditing={() => this.props.checkAccount(this.state.Account)}
                value={this.state.Account}
              />
            </View>
            {
              this.state.check_account_status ?
                [
                  AlertIOS.alert(
                    "警告",
                    "此帳號已被使用",
                    [
                      { text: 'OK', onPress: () => { this.setState({ Account: '', check_account_status: false }), this.props.checkAccount(this.state.Account) } },
                    ]
                  )
                ]
                :
                []
            }
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>密碼</Text>
              <TextInput
                placeholder="密碼..."
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(Password) => this.setState({ Password })}
                autoCapitalize="none"
                secureTextEntry={true}
                value={this.state.Password}
              />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>確認</Text>
              <TextInput
                placeholder="確認密碼..."
                placeholderTextColor="gray"
                style={styles.textInput}
                autoCapitalize="none"
                secureTextEntry={true}
                onChangeText={(Password) => this.setState({ checkPassword: Password })}
                onEndEditing={() => this._checkPassword(this.state.checkPassword)}
                value={this.state.checkPassword}
              />
            </View>
            <View style={styles.space}></View>
            <View style={styles.btnContent}>

              <View style={styles.button}>
                <Button onPress={() => { this._carousel.snapToNext(); }}
                  title="Next >"
                  color="white"
                >
                </Button>
              </View>
            </View>

          </View>
          <View  >
            <ScrollView
              automaticallyAdjustContentInsets={false}
              contentInset={{ top: 0 }}
              style={styles.scrollView}>
              <View style={styles.content}>
                <View style={styles.imageView}>
                  <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                      <View style={[styles.avatar, styles.avatarContainer]}>
                        {this.state.avatarSource === null ? <Text>Select a Photo</Text> :
                          <Image style={styles.avatar} source={this.state.avatarSource} />
                        }
                        {console.log(this.state.avatarSource)}
                      </View>
                      <Text style={{ fontSize: 18, backgroundColor: 'rgba(0,0,0,0)', marginLeft: 20 }}>{camera} 上傳大頭貼</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.space}></View>

              <View style={styles.content}>
                <Text style={styles.columnText}>姓名</Text>
                <TextInput
                  placeholder="姓名..."
                  placeholderTextColor="gray"
                  style={styles.textInput}
                  onChangeText={(Name) => this.setState({ Name })} />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>生日</Text>
                <DatePicker
                  style={{ width: 200 }}
                  date={this.state.Birthday}
                  mode="date"
                  placeholder="placeholder"
                  format="YYYY/MM/DD"
                  minDate="1900/01/01"
                  maxDate="2016/06/01"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  onDateChange={(Birthday) => { this.setState({ Birthday: Birthday }); this._Constellation(Birthday) }}
                />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>信箱</Text>
                <TextInput
                  placeholder="信箱..."
                  placeholderTextColor="gray"
                  style={styles.textInput}
                  onChangeText={(Email) => this.setState({ Email })}
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.space}></View>

              <View style={styles.content}>
                <Text style={styles.columnText}>性別</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.sex.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.Sex}
                  />
                </TouchableOpacity>
                <SinglePicker
                  lang="en-US"
                  ref={ref => this.sex = ref}
                  onConfirm={(option) => {
                    this.setState({ Sex: option.value })
                  }}
                  options={sex} />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>身高</Text>
                <TextInput
                  placeholder="身高..."
                  placeholderTextColor="gray"
                  keyboardType='number-pad'
                  style={styles.textInput}
                  onChangeText={(Height) => this.setState({ Height })}
                />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>體重</Text>
                <TextInput
                  placeholder="體重..."
                  placeholderTextColor="gray"
                  keyboardType='number-pad'
                  style={styles.textInput}
                  onChangeText={(Weight) => this.setState({ Weight })}
                />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>居住</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.address.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                  />
                </TouchableOpacity>
                <SinglePicker
                  lang="en-US"
                  ref={ref => this.address = ref}
                  onConfirm={(option) => {
                    this.setState({ Address: option.value })
                  }}
                  options={address} />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>性向</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.aptitude.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                  />
                </TouchableOpacity>
                <SinglePicker
                  lang="en-US"
                  ref={ref => this.aptitude = ref}
                  onConfirm={(option) => {
                    this.setState({ Aptitude: option.value })
                  }}
                  options={aptitude} />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>血型</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.bloodPicker.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                  />
                </TouchableOpacity>
                <SinglePicker
                  lang="en-US"
                  ref={ref => this.bloodPicker = ref}
                  onConfirm={(option) => {
                    this.setState({ Blood: option.value })
                  }}
                  options={blood} />

              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>戀愛狀況</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.isLovePicker.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                  />
                </TouchableOpacity>
                <SinglePicker
                  lang="en-US"
                  ref={ref => this.isLovePicker = ref}
                  onConfirm={(option) => {
                    this.setState({ isLove: option.value })
                  }}
                  options={isLove} />

              </View>
              <View style={styles.space}></View>
              <View style={styles.content}>
                <Text style={styles.columnText}>我的類型</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.myStyle.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                  />
                </TouchableOpacity>
                {
                  this.state.Sex === '男' ?
                    <SinglePicker
                      lang="en-US"
                      ref={ref => this.myStyle = ref}
                      onConfirm={(option) => {
                        this.setState({ M_myStyle: option })
                      }}
                      options={M_myStyle} />
                    :
                    <SinglePicker
                      lang="en-US"
                      ref={ref => this.myStyle = ref}
                      onConfirm={(option) => {
                        this.setState({ W_myStyle: option })
                      }}
                      options={W_myStyle} />
                }
              </View>
              <View style={styles.btnContent}>

                <View style={styles.button}>
                  <Button onPress={() => { this._carousel.snapToNext(); }}
                    title="Next >"
                    color="white"
                  >
                  </Button>
                </View>

              </View>
            </ScrollView>
          </View>

          <View>
            <ScrollView
              automaticallyAdjustContentInsets={true}
              contentInset={{ top: 0 }}
              style={styles.scrollView}>
              <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                <Text style={styles.columnText}>喜歡{this.state.Sex === '男' ? '女' : '男'}生的...</Text>
              </View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>髮型</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.hairStyle.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.Sex === '男' ? this.state.W_Hairstyle : this.state.M_Hairstyle}
                  />
                </TouchableOpacity>
                {
                  this.state.Sex === '男' ?
                    <ImagePicker
                      ref={ref => this.hairStyle = ref}
                      onConfirm={(option) => {
                        this.setState({ W_Hairstyle: option.value })
                      }}
                      options={W_Hairstyle} />
                    :
                    <ImagePicker
                      lang="en-US"
                      ref={ref => this.hairStyle = ref}
                      onConfirm={(option) => {
                        this.setState({ M_Hairstyle: option.value })
                      }}
                      options={M_Hairstyle} />
                }
              </View>
              {this.state.Sex === '男' ?
                <View>
                  <View style={styles.space}></View>
                  <View style={styles.content2}>
                    <Text style={styles.columnText}>瀏海</Text>
                    <TouchableOpacity
                      onPress={() => {
                        this.headHair.show();
                      }}>
                      <TextInput
                        placeholder="請選擇"
                        placeholderTextColor="gray"
                        style={styles.dropdown}
                        editable={false}
                        value={this.state.Sex === '男' ? this.state.W_Hairstyle : this.state.M_Hairstyle}
                      />
                    </TouchableOpacity>
                    <ImagePicker
                      ref={ref => this.headHair = ref}
                      onConfirm={(option) => {
                        this.setState({ W_Headhair: option.value })
                      }}
                      options={W_Headhair} />
                  </View>
                  <View style={styles.space}></View>
                </View>
                :
                <View style={styles.space}></View>}

              <View style={styles.content2}>
                <Text style={styles.columnText}>身材</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.Stature.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.Sex === '男' ? this.state.W_Stature : this.state.M_Stature}
                  />
                </TouchableOpacity>
                {
                  this.state.Sex === '男' ?
                    <SinglePicker
                      ref={ref => this.Stature = ref}
                      onConfirm={(option) => {
                        this.setState({ W_Stature: option.value })
                      }}
                      options={W_Stature} />
                    :
                    <SinglePicker
                      lang="en-US"
                      ref={ref => this.Stature = ref}
                      onConfirm={(option) => {
                        this.setState({ M_Stature: option.value })
                      }}
                      options={M_Stature} />
                }
              </View>
              <View style={styles.space}></View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>身高</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.Height.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.Sex === '男' ? this.state.W_Height : this.state.M_Height}
                  />
                </TouchableOpacity>
                {
                  this.state.Sex === '男' ?
                    <SinglePicker
                      ref={ref => this.Height = ref}
                      onConfirm={(option) => {
                        this.setState({ W_Height: option.value })
                      }}
                      options={W_Height} />
                    :
                    <SinglePicker
                      lang="en-US"
                      ref={ref => this.Height = ref}
                      onConfirm={(option) => {
                        this.setState({ M_Height: option.value })
                      }}
                      options={M_Height} />
                }
              </View>
              <View style={styles.space}></View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>外觀</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.Style.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.Sex === '男' ? this.state.W_Style : this.state.M_Style}
                  />
                </TouchableOpacity>
                {
                  this.state.Sex === '男' ?
                    <SinglePicker
                      ref={ref => this.Style = ref}
                      onConfirm={(option) => {
                        this.setState({ W_Style: option.value })
                      }}
                      options={W_myStyle} />
                    :
                    <SinglePicker
                      lang="en-US"
                      ref={ref => this.Style = ref}
                      onConfirm={(option) => {
                        this.setState({ M_Style: option.value })
                      }}
                      options={M_myStyle} />
                }
              </View>
              <View style={styles.space}></View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>年齡</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.LikeAge.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.LikeAge}
                  />
                </TouchableOpacity>
                <SinglePicker
                  ref={ref => this.LikeAge = ref}
                  onConfirm={(option) => {
                    this.setState({ LikeAge: option.value })
                  }}
                  options={LikeAge} />
              </View>
              <View style={styles.space}></View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>臉型</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.FaceType.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.FaceType}
                  />
                </TouchableOpacity>
                <ImagePicker
                  ref={ref => this.FaceType = ref}
                  onConfirm={(option) => {
                    this.setState({ FaceType: option.value })
                  }}
                  options={FaceType} />
              </View>
              <View style={styles.space}></View>
              <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
                <Text style={styles.columnText}>第一眼看{this.state.Sex === '男' ? '女' : '男'}生的...</Text>
              </View>
              <View style={styles.content2}>
                <Text style={styles.columnText}>部位</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.FirstSee.show();
                  }}>
                  <TextInput
                    placeholder="請選擇"
                    placeholderTextColor="gray"
                    style={styles.dropdown}
                    editable={false}
                    value={this.state.FirstSee}
                  />
                </TouchableOpacity>
                <SinglePicker
                  ref={ref => this.FirstSee = ref}
                  onConfirm={(option) => {
                    this.setState({ FirstSee: option.value })
                  }}
                  options={FirstSee} />
              </View>
              <View style={styles.content2}>

                <View style={styles.button}>
                  <Button onPress={() => { this._carousel.snapToNext(); }}
                    title="登入"
                    color="white"
                  >
                  </Button>
                </View>
              </View>

            </ScrollView>

          </View>
        </Carousel>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toparea: {
    marginTop: height * 0.04,
    marginBottom: height * 0.01,
    width: width * 0.8,
    height: height * 0.06
  },
  imageView: {
    // width: width - width * 0.1,
    // zIndex: 1,
    // position: 'absolute',
    // alignItems: 'center',
  },
  cricle: {
    height: 80,
    width: 80,
    borderWidth: 3,
    borderColor: 'rgba(52, 52, 52, 0.8)',
    borderRadius: 40,
    marginBottom: 10,
    marginLeft: 20,
    // justifyContent:'center',
  },
  loginText: {
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 40,
    width: 80,
    height: 80
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection:'row'
  },
  bigview: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',

  },
  content: {
    // justifyContent: 'center',s
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnContent: {
    justifyContent: 'center',
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content2: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  radioContent: {
    alignItems: 'center',
    // flex: 1,
    flexDirection: 'row',
  },
  columnText: {
    fontSize: 20,
    marginRight: 10,
    marginTop: 5
  },
  columnRadio: {
    fontSize: 20,
    marginRight: 10,
    // marginTop: 12,
    marginLeft: 5,
  },
  textInput: {
    width: width * 0.6,
    // alignItems: 'stretch',
    height: 30,
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 5,
    fontFamily: 'Euphemia UCAS',

  },
  space: {
    height: 20,
  },
  radioSpace: {
    height: 10,
  },
  button: {
    width: width * 0.25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'black',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slide1: {
    justifyContent: 'center',
    padding: width * 0.1,
    width: width,
    height: 600,
    alignItems: 'center',
  },
  scrollView: {
    paddingHorizontal: width * 0.12,
    width: width,
    height: 500,
  },
  radioView: {
    // flex:1,
    flexDirection: 'row',
  },
  passwordBtn: {
    paddingLeft: width * 0.04,
    paddingRight: width * 0.04,
    paddingTop: width * 0.02,
    paddingBottom: width * 0.02,

    backgroundColor: 'black'
  },
  dropdown: {
    width: width * 0.4,
    // alignItems: 'stretch',
    height: 30,
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 5,
    fontFamily: 'Euphemia UCAS',
  }
});

export default Register;

