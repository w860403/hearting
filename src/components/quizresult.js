import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  AlertIOS,
  ListView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
var { height, width } = Dimensions.get('window');
import CharmQuestionModal from './charmquestionmodal'
class QuizResult extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    // console.log('on quizresult component')
    // console.log(this.props.quizResultData)
    // console.log(this.props.quizResultData.PersonList)
    this.state = {
      ds: ds,
      dataSource: ds.cloneWithRows(this.props.quizResultData.PersonList),
      personList: this.props.quizResultData.PersonList,
      sex: this.props.quizResultData.Sex,
      charmList: [],
      imageUrl: 'http://114.35.74.209/Hearting/Upload/',
      relateDataSource: ds.cloneWithRows(this.props.quizResultData.RecommendList),
      recommendList: this.props.quizResultData.RecommendList,
      modalVisible: true
    };
  }
  componentWillMount() {
    var self = this
    this.props.quizResultData.PersonList.map(function (data) {
      if (data.Score > 70) {
        var object = { Image: data.Image, Account: data.Account, Score: data.Score, Style: null }
        self.state.charmList.push(object)
      }
      console.log('charmList:'); 
      console.log(self.state.charmList);
     })
  }
  setModalVisible(bool) {
    this.setState({
      modalVisible: bool
    })
  }
  pressRow(rowID) {
    console.log(rowID)
    var newDs = [];
    newDs = this.state.personList.slice();
    newDs[rowID].Status = false;
    newDs[rowID].E_Msg = "已送出";
    console.log(newDs)
    this.setState({
      dataSource: this.state.ds.cloneWithRows(newDs),
      personList: newDs
    })
  }
  render() {
    return (
      <View style={styles.bigView} >
        <CharmQuestionModal
          modalVisible={this.state.modalVisible}
          charmList={this.state.charmList}
          sex={this.state.sex}
          setModalVisible={(bool) => {
            this.setModalVisible(bool)
          }}
          saveAsCharm={(charmList, sex) => {
            this.props.saveAsCharm(charmList, sex)
          }}
        />
        <View style={styles.titleView}>
          <View style={styles.titleLeftView}>
          </View>
          <View style={styles.titleMiddleView}>
            <View style={styles.titleMiddleView1}>
              <Text style={styles.titleText}>心動指數</Text></View>
            <View style={styles.titleMiddleView2}>
              <Text style={styles.titleText}>交往契合度</Text></View>
          </View>
          <View style={styles.titleRightView}>
            <Text style={styles.titleText}>交友邀請</Text>
          </View>
        </View>
        <View style={styles.listView}>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={(rowData, sectionID, rowID) =>
              <View style={styles.listViewContent}>
                <View style={styles.listViewContentLeftView}>
                  <Image source={{ uri: this.state.imageUrl + rowData.Image }} style={styles.image} />
                </View>
                <View style={styles.listViewContentMiddleView}>
                  <View style={styles.listViewContentMiddleView1}>
                    <Text style={styles.pointText}>{rowData.Score}</Text>
                  </View>
                  <View style={styles.listViewContentMiddleView2}>
                    <Text style={styles.pointText}>{rowData.MatchPercent}%</Text>
                  </View>
                </View>
                <View style={styles.listViewContentRightView}>
                  {rowData.Status ? <TouchableHighlight
                    style={styles.button}
                    activeOpacity={0.8}
                    underlayColor='rgba(52,52,52,0.5)'
                    onPress={() => AlertIOS.alert('確認送出好友邀請', '一天只有三次機會送出交友邀請，確定要送出嗎？', [{
                      text: '確認', onPress: () => {
                        this.pressRow(rowID)
                        this.props.sendInvitation(this.props.account, rowData.Account)
                      }
                    },
                    { text: '取消', onPress: () => console.log('取消') }])}>
                    <Text style={styles.addFriendText}>加好友</Text>
                  </TouchableHighlight> : <Text style={styles.E_MsgText}>{rowData.E_Msg}</Text>}
                </View>
              </View>
            }
          />
        </View>
        <View style={styles.relateListView}>
          <View>
            <View>
              <Text style={styles.relateTitleText}>你可能也會對他們心動...</Text>
            </View>
          </View>
          <ListView
            horizontal
            dataSource={this.state.relateDataSource}
            renderRow={(rowData) =>
              <View style={styles.relateListViewContent}>
                <TouchableHighlight
                  style={styles.relatetouch}
                  activeOpacity={0.8}
                  underlayColor='rgba(0,0,0,0)'
                  onPress={() => this.render}>
                  <Image source={{ uri: this.state.imageUrl + rowData.ImgName }} style={styles.relateImage} >
                    <View style={styles.relateImageCover}>
                      <Text style={styles.relateImageCoverText}>?</Text>
                    </View>
                  </Image>
                </TouchableHighlight>
              </View>
            }
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  bigView: {
    alignItems: 'center',
    flex: 1,
  },
  titleView: {
    marginTop: 30,
    flexDirection: 'row',
    width: width * 0.95,
    height: 30,
  },
  titleLeftView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  titleMiddleView: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    flex: 2.5,
    justifyContent: 'flex-end',
  },
  titleMiddleView1: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleMiddleView2: {
    alignItems: 'center',
    flex: 1.5,
    justifyContent: 'flex-end',
  },
  titleRightView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleText: {
    fontSize: 18,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  listView: {
    flex: 1,
    marginTop: 5,
    width: width * 0.95,
  },
  listViewContent: {
    flexDirection: 'row',
    width: width * 0.95,
    height: 70,
    borderColor: 'rgba(52,52,52,0.5)',
    borderTopWidth: 1,
    alignItems: 'center',
  },
  listViewContentLeftView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  listViewContentMiddleView: {
    alignItems: 'center',
    flexDirection: 'row',
    flex: 2.5
  },
  listViewContentMiddleView1: {
    alignItems: 'center',
    flex: 1
  },
  listViewContentMiddleView2: {
    alignItems: 'center',
    flex: 1.5
  },
  listViewContentRightView: {
    alignItems: 'center',
    flex: 1
  },
  image: {
    borderColor: 'rgba(52,52,52,0.5)',
    borderWidth: 1,
    width: 50,
    height: 50
  },
  pointText: {
    fontSize: 25,
    color: '#E86868',
    backgroundColor: 'rgba(52,52,52,0)',
  },
  button: {
    backgroundColor: 'rgba(52,52,52,0.6)',
    width: 60,
    height: 30,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addFriendText: {
    color: 'white',
    fontSize: 15
  },
  E_MsgText: {
    backgroundColor: 'rgba(52,52,52,0)',
    fontSize: 15
  },
  relateListView: {
    height: 120,
    width: width * 1,
    backgroundColor: 'rgba(52,52,52,0.5)',
    marginBottom: 30,
    marginTop: 30,
    paddingLeft: 5,
    paddingRight: 5
  },
  relateListViewContent: {
  },
  relateImageCover: {
    width: 80,
    height: 80,
    backgroundColor: 'rgba(52,52,52,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  relateImageCoverText: {
    color: 'white',
    fontSize: 50
  },
  relateImage: {
    borderColor: 'rgba(52,52,52,0.5)',
    borderWidth: 1,
    width: 80,
    height: 80,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  relateTitleText: {
    marginTop: 3,
    fontSize: 20,
    color: 'white'
  }
});

export default QuizResult;

