import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Entypo';
import CheckBox from 'react-native-icon-checkbox';

class DoomedResult extends Component {
    constructor() {
        super();

    }

    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
        return (
            <View style={styles.bigview}>
                <View style={contentStyle.content}>
                    <View style={contentStyle.titleViw}>
                        <Text style={contentStyle.titleText}>屬於你/妳的命中注定</Text>
                    </View>
                    <View>
                        <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/5-randomTest-random-1.png' }} style={contentStyle.square} />
                    </View>
                    <View style={contentStyle.style}>
                        <Text style={{ color: '#383838', fontSize: 23 }}>霸氣總裁型</Text>
                    </View>
                    <View style={contentStyle.startBtn}>
                        <TouchableOpacity>
                            <Text style={{ color: 'white', fontSize: 20 }}>開始聊天</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        // alignItems: 'center',
    },

});
const contentStyle = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleViw: {
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
        marginBottom:10
    },
    titleText: {
        fontSize: 28,
        marginBottom: 10,
        color:"#383838"
    },
    square: {
        height: height * 0.55,
        width: width * 0.8,
        borderWidth: 5,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
    startBtn: {
        paddingLeft: width * 0.04,
        paddingRight: width * 0.04,
        paddingTop: width * 0.02,
        paddingBottom: width * 0.02,
        backgroundColor: '#383838',
        marginTop: 20,
        marginBottom: 30,
    },
    style:{
        marginTop:20,
        backgroundColor:'rgba(0,0,0,0)'
    }
})

export default DoomedResult;

