import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  NavigatorIOS,
  Dimensions,
  Image,
  ActivityIndicator,
  Button,
  TouchableOpacity,
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view';
import Icon from 'react-native-vector-icons/Entypo';
var { height, width } = Dimensions.get('window');

class member extends Component {
  constructor(props) {
    super(props);
    this.state = {
      year: '',
      month: '',
      date: ''
    }
  }
  render() {
    const myIcon = (<Icon name="calendar" size={20} color="gray" />)
    const address = (<Icon name="address" size={20} color="gray" />)
    const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
    var data = ''
    if (!this.state.loaded)
      return (
        <ActivityIndicator
          animating={this.state.animating}
          style={[styles.centering, { height: 80 }]}
          size="large"
        />
      );
    return (
      <View style={styles.bigview}>
        <View style={styles.userData}>
          <View style={styles.imageView}>
            <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + this.props.memberData.Image }} style={styles.cricle} />
          </View>
          <View style={styles.detail}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[styles.name, { flex: 1 }]}>{this.props.memberData.Name}</Text>
              <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', marginRight: 10 }}>
                <TouchableOpacity onPress={() => this.props.logout()}>
                  <Text >登出</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Text style={styles.memo}>{this.props.memberInfo.Profile}</Text>
            <View style={styles.profile}>
              <Text style={styles.profileText}>{myIcon}</Text>
              <Text style={styles.profileText}> {this.state.date}</Text>
            </View>
            <View style={[styles.profile]}>
              <Text style={[styles.profileText, { flex: 0.5 }]}>{address}</Text>
              <Text style={[styles.profileText, { flex: 1 }]}> {this.props.memberData.Address}</Text>
              <View style={{ flex: 5,justifyContent: 'flex-end', alignItems: 'flex-end', marginRight: 10 }}>
                <TouchableOpacity onPress={() => this.props.onEditMemberButtonClick()}>
                  <Text>編輯個人資料</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <ScrollableTabView
          tabBarActiveTextColor="#ED6D86"
          tabBarUnderlineStyle={{ backgroundColor: '#ED6D86', height: 2 }}
          // tabBarActiveTextColor="#1fb5ec"
          tabBarTextStyle={{ fontSize: 17, backgroundColor: 'rgba(0,0,0,0)' }}
        >
          <View tabLabel='Quiz' style={styles.tab}>
            <TouchableOpacity onPress={() => { this.props.onUploadPhotoButtonClick(this.state.random), this.props.getNowImage(this.props.account, 'randomTest') }} style={styles.quizContent}>
              <View>
                <Image source={require('../images/handsome03.jpg')} style={styles.square} />
              </View>
              <View style={styles.btnRight}>
                <Text style={styles.btnTitle}> 隨機</Text>
                <Text style={styles.btnSub}> 上傳 1 張測驗照片 </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={() => { this.props.onUploadPhotoButtonClick(this.state.back), this.props.getNowImage(this.props.account, 'backTest') }} 
              style={styles.quizContent}
              onPress={() => alert('coming soon')}
            >
              <View>
                <Image source={require('../images/handsome04.jpg')} style={styles.square} />
              </View>
              <View style={styles.btnRight}>
                <Text style={styles.btnTitle}> 背影殺手</Text>
                <Text style={styles.btnSub}> 上傳 2 張測驗照片 </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={() => { this.props.onUploadPhotoButtonClick(this.state.angle), this.props.getNowImage(this.props.account, 'angleTest') }} 
              style={styles.quizContent}
              onPress={() => alert('coming soon')}
            >
              <View>
                <Image source={require('../images/handsome05.jpg')} style={styles.square} />
              </View>
              <View style={styles.btnRight}>
                <Text style={styles.btnTitle}> 零死角</Text>
                <Text style={styles.btnSub}> 上傳 3 張測驗照片 </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View tabLabel='History' style={styles.tab}>
            <View style={styles.historyContent}>

              <TouchableOpacity onPress={() => this.props.onHistoryQuizButtonClick()} style={styles.historyLeft}>
                <View style={styles.historyLeftConent}>
                  <Image source={require('../images/quiz.png')} style={styles.historyImage1} />
                  <Text style={styles.histroyText1}>測驗</Text>
                  <Text style={styles.histroyText1}>測驗</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.props.onHistoryRankButtonClick()} style={styles.historyRight}>
                <View style={styles.historyRightConent}>
                  <Image source={require('../images/rank.png')} style={styles.historyImage2} />
                  <Text style={styles.histroyText2}>排名</Text>
                  <Text style={styles.histroyText2}>排名</Text>
                </View>
              </TouchableOpacity>

            </View>
          </View>
          <View tabLabel='Photo' style={styles.tab}>
            <TouchableOpacity style={styles.photoContent} onPress={() => this.props.onPhotokButtonClick('random')}>
              <View style={styles.photoTop}>
                <Text style={styles.photoTitle}>隨機</Text>
                <View style={styles.photoNumView}>
                  <Text style={styles.photoNum}>{this.props.member_photo_random === null ? 0 : this.props.member_photo_random.length}</Text>
                </View>
              </View>
              <View style={styles.photoBottom}>
                {
                  this.props.member_photo_random !== null ?
                    this.props.member_photo_random.map(function (data, index) {
                      if (index <= 3) {
                        return <Image key={index} source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + data.ImgName }} style={styles.square} />
                      }
                    }) :
                    <Text style={styles.noImageText}>此相簿無圖片</Text>
                }
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.photoContent} >
              <View style={styles.photoTop}>
                <Text style={styles.photoTitle}>背影殺手</Text>
                <View style={styles.photoNumView}>
                  <Text style={styles.photoNum}>{this.props.member_photo_back !== null ? this.props.member_photo_back.length : 0}</Text>
                </View>
              </View>
              <View style={styles.photoBottom}>
                {
                  this.props.member_photo_back !== null ?
                    this.props.member_photo_back.map(function (data, index) {
                      if (index <= 3) {
                        return <Image key={index} source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + data.ImgName }} style={styles.square} />
                      }
                    }) :
                    <Text style={styles.noImageText}>此相簿無圖片</Text>
                }
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.photoContent}>
              <View style={styles.photoTop}>
                <Text style={styles.photoTitle}>零死角</Text>
                <View style={styles.photoNumView}>
                  <Text style={styles.photoNum}>{this.props.member_photo_angle !== null ? this.props.member_photo_angle.length : 0}</Text>
                </View>
              </View>
              <View style={styles.photoBottom}>
                {
                  this.props.member_photo_angle !== null ?
                    this.props.member_photo_angle.map(function (data, index) {
                      if (index <= 3) {
                        return <Image key={index} source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + data.ImgName }} style={styles.square} />
                      }
                    }) :
                    <Text style={styles.noImageText}>此相簿無圖片</Text>
                }
              </View>
            </TouchableOpacity>
          </View>
        </ScrollableTabView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  cricle: {
    height: 80,
    width: 80,
    borderRadius: 40,
    marginBottom: 10,
    marginLeft: 20,
    // justifyContent:'center',    
  },
  square: {
    height: 60,
    width: 60,
    opacity: 0.85,
    marginRight: 7,
    // justifyContent:'center',    
  },
  historyImage1: {
    marginLeft: 10,
    marginTop: 15,
  },
  historyImage2: {
    marginLeft: 17,
    marginTop: 20,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    // justifyContent:'center',
    // alignItems:'center',
    // flexDirection:'row'
  },
  bigview: {
    // justifyContent:'center',
    flex: 1,
    flexDirection: 'column',
    // alignItems:'center',
  },
  userData: {
    marginTop: height * 0.04,
    marginLeft: width * 0.05,
    marginBottom: width * 0.03
  },
  imageView: {
    width: width - width * 0.1,
    zIndex: 1,
    position: 'absolute',
    alignItems: 'center',
  },
  detail: {
    marginTop: height * 0.06,
    paddingTop: height * 0.04,
    paddingBottom: 10,
    backgroundColor: 'rgba(52, 52, 52, 0.1)',
    width: width - width * 0.1,
    borderRadius: 10,
  },
  name: {
    marginLeft: 20,
    marginBottom: 5,
    fontSize: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  memo: {
    marginLeft: 20,
    fontSize: 15,
    marginBottom: 10,
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  profile: {
    marginLeft: 20,
    flexDirection: 'row',
    alignItems: 'center',
    width: width - width * 0.1 - 20,
  },
  profileText: {
    fontSize: 13,
    color: 'gray',
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  button: {
    // paddingTop:3,
    // paddingBottom:3,
    paddingLeft: width * 0.08,
    // paddingRight:10,
  },
  underLine: {
    backfaceVisibility: 'hidden',
    borderBottomColor: "#EDAFCE"
  },
  tab: {
    width: width,
    alignItems: 'center',
    padding: 20,
  },
  quizContent: {
    marginBottom: 20,
    padding: 15,
    width: width * 0.7,
    // alignItems:'center',    
    // justifyContent:'center',    
    // flex:1,
    borderColor: '#515151',
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  btnRight: {
    flexDirection: 'column',
  },
  btnTitle: {
    fontSize: 20,
    marginTop: 4,
    marginLeft: 5,
    color: '#515151',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  btnSub: {
    // fontSize:20,
    marginTop: 10,
    marginLeft: 5,
    color: '#515151',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  //historyContent
  historyContent: {
    width: width * 0.7,
    flexDirection: 'row',
  },
  historyLeft: {
    width: width * 0.7 / 2,
    height: height * 0.4,
    backgroundColor: 'rgba(113, 246, 251, 0.3)',
    // opacity:0.5            
  },
  historyRight: {
    width: width * 0.7 / 2,
    height: height * 0.4,
    backgroundColor: 'rgba(237, 176, 207, 0.5)',
  },
  historyLeftConent: {
    width: width * 0.7 / 2 * 0.8,
    height: height * 0.4 * 0.8,
    marginLeft: width * 0.7 / 2 * 0.2,
    marginTop: height * 0.4 * 0.1,
    marginBottom: height * 0.4 * 0.1,
    // backgroundColor:'rgba(237, 176, 207, 0.5)',   
    borderColor: 'white',
    // flexDirection:'row',    
    borderStyle: 'solid',
    // borderWidth:1,  
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderLeftWidth: 2,

  },
  historyRightConent: {
    width: width * 0.7 / 2 * 0.8,
    height: height * 0.4 * 0.8,
    marginRight: width * 0.7 / 2 * 0.2,
    marginTop: height * 0.4 * 0.1,
    marginBottom: height * 0.4 * 0.1,
    // backgroundColor:'rgba(237, 176, 207, 0.5)',   
    borderColor: 'white',
    // flexDirection:'row',    
    borderStyle: 'solid',
    // borderWidth:1,  
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderRightWidth: 2,
  },
  histroyText1: {
    fontSize: 23,
    marginLeft: width * 0.7 / 2 * 0.2,
    marginTop: height * 0.4 * 0.05
  },
  histroyText2: {
    fontSize: 23,
    marginLeft: width * 0.7 / 2 * 0.2,
    marginTop: height * 0.4 * 0.07
  },
  photoContent: {
    marginBottom: 10,
    // padding:10,
    width: width * 0.7,
    // alignItems:'center',    
    // justifyContent:'center',    
    // flex:1,
    borderColor: '#515151',
    flexDirection: 'column',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  photoTop: {
    backgroundColor: 'white',
    height: height * 0.05,
    flexDirection: 'row',
    // justifyContent:'flex-end',

  },
  photoTitle: {
    marginTop: 10,
    marginLeft: 10,
    fontSize: 15,
    flex: 1
  },
  photoNumView: {
    backgroundColor: 'gray'

  },
  photoNum: {
    fontSize: 24,
    marginLeft: width * 0.02,
    marginRight: width * 0.02,
    fontFamily: 'Euphemia UCAS',
    color: 'white',
  },
  photoBottom: { flexDirection: 'row', height: 60, alignItems: 'center' },
  noImageText: {
    fontSize: 15,
    marginLeft: 10
  }
});
export default member;

