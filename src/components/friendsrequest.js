import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  ListView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
var { height, width } = Dimensions.get('window');
class QuizResult extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      friendsRequestData: this.props.friendsRequestData ? this.props.friendsRequestData : [],
      ds: ds,
      dataSource: ds.cloneWithRows(this.props.friendsRequestData),
      imageurl: 'http://114.35.74.209/Hearting/Upload/',
    };
  }
  pressRow(rowID) {
    console.log(rowID)
    var newDs = [];
    newDs = this.state.friendsRequestData.slice();
    console.log('newDS=')
    console.log(newDs)
    newDs.splice(rowID, 1)
    console.log('newDS after splice=')
    console.log(newDs)
    this.setState({
      dataSource: this.state.ds.cloneWithRows(newDs),
      friendsRequestData: newDs
    })
  }
  render() {
    return (
      <View style={styles.bigview}>
        <View style={styles.titleview}>
          <View style={styles.titleleftview}>
          </View>
          <View style={styles.titlemiddleview}>
            <Text style={styles.titletext}>類型</Text>
          </View>
          <View style={styles.titlerightview}>
            <Text style={styles.titletext}>是否接受</Text>
          </View>
        </View>
        <View style={styles.listview}>
          {this.props.friendsRequestData.length != 0 ?
            <ListView
              dataSource={this.state.dataSource}
              renderRow={(rowData, sectionID, rowID) =>
                <View style={styles.listviewcontent}>
                  <View style={styles.listviewcontentleftview}>
                    <Image source={{ uri: this.state.imageurl + rowData.Image }} style={styles.image} />
                  </View>
                  <View style={styles.listviewcontentmiddleview}>
                    <Text style={styles.styletext}>{rowData.Com_txt}</Text>
                  </View>
                  <View style={styles.listviewcontentrightview}>
                    <TouchableHighlight
                      style={styles.button}
                      activeOpacity={0.8}
                      underlayColor='rgba(52,52,52,0.5)'
                      onPress={() => {
                        this.props.replyInvationAccept(rowData.Account, this.props.account)
                        this.pressRow(rowID)
                      }}>
                      <Text style={styles.addfriendtext}>是</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                      style={styles.button}
                      activeOpacity={0.8}
                      underlayColor='rgba(52,52,52,0.5)'
                      onPress={() => {
                        this.props.replyInvationRefuse(rowData.Account, this.props.account)
                        this.pressRow(rowID)
                      }}>
                      <Text style={styles.addfriendtext}>否</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              }
            />
            :
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
              <Text style={{ fontSize: 20, backgroundColor: 'rgba(0,0,0,0)' }}>無好友邀請</Text>
            </View>
          }
        </View>
        <View style={styles.footerView}>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  bigview: {
    alignItems: 'center',
    flex: 1,
  },
  scrollview: {
    width: width * 0.9,
    height: height * 0.6,
  },
  titleview: {
    marginTop: 30,
    flexDirection: 'row',
    width: width * 0.9,
    height: 30,
    borderColor: 'rgba(52,52,52,0.5)',
    borderBottomWidth: 1,
  },
  titleleftview: {
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  titlemiddleview: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  titlerightview: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(52,52,52,0)'
  },
  titletext: {
    fontSize: 18,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  listview: {
    flex: 1,
    marginTop: 5,
    width: width * 0.9,
  },
  listviewcontent: {
    flexDirection: 'row',
    width: width * 0.9,
    height: 70,
    alignItems: 'center',
  },
  listviewcontentleftview: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.6,
  },
  listviewcontentmiddleview: {
    alignItems: 'center',
    flex: 1
  },
  listviewcontentrightview: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  image: {
    borderColor: 'rgba(52,52,52,0.5)',
    borderWidth: 1,
    width: 50,
    height: 50
  },
  styletext: {
    fontSize: 18,
    backgroundColor: 'rgba(52,52,52,0)',
  },
  button: {
    backgroundColor: 'rgba(52,52,52,0.6)',
    width: 50,
    height: 30,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 3,
    marginTop: 3
  },
  addfriendtext: {
    color: 'white',
    fontSize: 15
  },
  footerView: {
    height: 30,
    width: width,
  }
});

export default QuizResult;

