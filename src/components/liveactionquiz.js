import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    AlertIOS
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import MindWaveMobile from 'react-native-mindwave-mobile';
import TopBar from '../containers/topbar';
const mwm = new MindWaveMobile()
var { height, width } = Dimensions.get('window');
var poorSingalTimer = 0
const poorSingalTimerTimeMax = 5
class ImageQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //前端資料
            titleText: this.props.titleText ? this.props.titleText : '現場真人配對',
            quizFunction: this.props.quizFunction ? this.props.quizFunction : '',
            //確認裝置連接
            animating: true,
            deviceFound: false,
            mindwaveConnected: false,
            devices: [],
            mindwaveTimer: 0,
            //確認訊號值歸零
            poorSignalChecked: false,
            poorSingalTimer: poorSingalTimerTimeMax,
            //開始測驗按鈕
            startQuiz: false,
            //計時資訊
            time: 4,
            timerCounter: 0,
            timeCounterMinus: 10,
            //腦波數據
            delta: this.props.delta ? this.props.delta : null, delta_max: 0.00, delta_min: 0.00, delta_avg: 0.00, delta_sd: 0.00, deltaArray: [],
            highAlpha: this.props.highAlpha ? this.props.highAlpha : null, highAlpha_max: 0.00, highAlpha_min: 0.00, highAlpha_avg: 0.00, highAlpha_sd: 0.00, highAlphaArray: [],
            lowAplpha: this.props.lowAplpha ? this.props.lowAplpha : null, lowAplpha_max: 0.00, lowAplpha_min: 0.00, lowAplpha_avg: 0.00, lowAplpha_sd: 0.00, lowAplphaArray: [],
            theta: this.props.theta ? this.props.theta : null, theta_max: 0.00, theta_min: 0.00, theta_avg: 0.00, theta_sd: 0.00, thetaArray: [],
            lowBeta: this.props.lowBeta ? this.props.lowBeta : null, lowBeta_max: 0.00, lowBeta_min: 0.00, lowBeta_avg: 0.00, lowBeta_sd: 0.00, lowBetaArray: [],
            midGamma: this.props.midGamma ? this.props.midGamma : null, midGamma_max: 0.00, midGamma_min: 0.00, midGamma_avg: 0.00, midGamma_sd: 0.00, midGammaArray: [],
            highBeta: this.props.highBeta ? this.props.highBeta : null, highBeta_max: 0.00, highBeta_min: 0.00, highBeta_avg: 0.00, highBeta_sd: 0.00, highBetaArray: [],
            lowGamma: this.props.lowGamma ? this.props.lowGamma : null, lowGamma_max: 0.00, lowGamma_min: 0.00, lowGamma_avg: 0.00, lowGamma_sd: 0.00, lowGammaArray: [],
            poorSignal: this.props.poorSignal ? this.props.poorSignal : 0,
            //腦波分數
            point: this.props.quizPoint ? this.props.quizPoint : 0,
            PointArray: [],
            sex: '男',
            text: '數值初始化中...'

        };
        console.log(this.state.imageArray)
    }
    //----腦波運算function----
    //取得最大值（傳入四個值,回傳最大值）
    _getMax(array) {
        var dataMax = array[0];
        for (i = 0; i < array.length; i++) {
            if (dataMax < array[i]) {
                dataMax = array[i]
            }
        }
        //console.log(dataMax)
        return dataMax
    }
    //取得最小值（傳入四個值，回傳最小值）
    _getMin(array) {
        var dataMin = array[0];
        for (i = 0; i < array.length; i++) {
            if (dataMin > array[i]) {
                dataMin = array[i]
            }
        }
        //console.log(dataMin)
        return dataMin
    }
    //計算平均（傳入四個值，回傳平均值）
    _getAvg(array) {
        //console.log(dataAvg)
        var Sum = 0
        var Avg = 0
        for (i = 0; i < array.length; i++) {
            Sum += array[i]
        }
        var Avg = parseFloat(((Sum) / array.length).toFixed(2))
        return Avg
    }
    //計算標準差（傳入四個值，回傳標準差）
    _getSD(array) {
        var Sum = 0
        var Avg = 0
        for (i = 0; i < array.length; i++) {
            Sum += array[i]
        }
        var Avg = parseFloat(((Sum) / array.length).toFixed(2))
        //console.log("average="+average)
        var SD = 0
        for (i = 0; i < array.length; i++) {
            SD += (array[i] - Avg) * (array[i] - Avg)
        }
        SD = parseFloat(Math.sqrt(SD / array.length).toFixed(2))
        return SD
    }
    //----腦波操作function----
    _devicescan() {
        mwm.scan()
    }
    _deviceconnect() {
        console.log("Try To Connect To Device " + this.state.devices[0].id + "_" + this.state.devices[0].name)
        mwm.connect(this.state.devices[0].id)
    }
    _devicedisconnect() {
        console.log("Stop Connect To Device ")
        mwm.disconnect()
    }
    _removeAllListeners() {
        console.log("Remove All Listener")
        mwm.removeAllListeners()
    }
    componentWillUnmount() {
        clearTimeout(this.timerScan)
    }
    componentDidMount() {
        this.timerScan = setInterval(
            () => {
                mwm.scan()
                console.log('scan');
            }, 1000)
        mwm.onFoundDevice(device => {
            console.log(device)
            this.state.devices.push(device)
            this.setState({
                deviceFound: true
            });
            clearTimeout(this.timerScan)
            this._deviceconnect()
        })
        mwm.onConnect(state => {
            if (!this.state.mindwaveConnected && this.state.deviceFound) {
                console.log(state.success = true ? "Connect Success" : "Connect Faild")
                var checkMindWaveConnectionDelayTimer = 0
                this.timer = setInterval(
                    () => {
                        checkMindWaveConnectionDelayTimer++
                        console.log('CheckConnectionDelay : ' + checkMindWaveConnectionDelayTimer)
                        if (checkMindWaveConnectionDelayTimer == 3) {
                            clearTimeout(this.timer)
                            this.setState({
                                mindwaveConnected: true,
                            });
                        }
                    }, 1000)
                var quizFunction = this.props.quizFunction
            }
        })
        mwm.onDisconnect(state => {
            if (!this.state.mindwaveConnected) {
                this.setState({
                    mindwaveConnected: false
                });
            }
            console.log(state.success = true ? "Disconnect Success" : "Disconnect Faild")
        })
        mwm.onEEGPowerDelta(data => {
            this.setState({
                mindwaveTimer: this.state.mindwaveTimer + 1
            })
            this.props.onEEGPowerDelta(data, this.state.mindwaveTimer)
            console.log('onEEGPowerDelta');
            console.log(data);
        })
        mwm.onEEGPowerLowBeta(data => {
            this.props.onEEGPowerLowBeta(data)
            console.log('onEEGPowerLowBeta');
            console.log(data);
        })
        mwm.onESense(data => {
            this.props.onESense(data)
        })
    }
    startQuiz() {
        this.setState({
            startQuiz: true
        })
    }
    popToHeart() {
        mwm.disconnect()
        mwm.removeAllListeners()
        Actions.liveactionquizresult()
    }
    componentWillReceiveProps(nextProps) {
        //耳機訊號傳回時間
        const { mindwaveTimer: previous_mindwaveTimer } = this.props;
        const { mindwaveTimer } = nextProps;
        //檢查訊號值正常
        const { poorSignal } = nextProps;
        if (poorSignal == 0 && !this.state.poorSignalChecked && mindwaveTimer != previous_mindwaveTimer && this.state.mindwaveConnected) {
            poorSingalTimer++
            timeCounterMinus = poorSingalTimerTimeMax - poorSingalTimer
            this.setState({
                poorSingalTimer: timeCounterMinus
            })
            if (poorSingalTimer == poorSingalTimerTimeMax) {
                this.setState({
                    poorSignalChecked: true,
                }, function () {
                    this.setState({
                        poorSingalTimer: poorSingalTimerTimeMax
                    })
                    poorSingalTimer = 0
                    timeCounterMinus = 0
                })
            }
            if (this.state.timerCounter > 5) {
                this.setState({
                    text: ''
                })
            }
            console.log('PoorSignalCount ' + poorSingalTimer)
        }
        if (poorSignal != 0 && !this.state.checkPoorSignal && mindwaveTimer != previous_mindwaveTimer && this.state.mindwaveConnected) {
            poorSingalTimer = 0
            console.log('PoorSignal Is Not 0')
            this.setState({
                poorSingalTimer: poorSingalTimerTimeMax
            })
        }
        //時間到，跳轉頁面
        if (this.state.timeCounterMinus == 0) {
            var sum = 0
            var avg = 0
            for (i = 0; i < this.state.PointArray.length; i++) {
                sum += this.state.PointArray[i]
            }
            avg = (sum / 5)
            console.log('avg:' + avg);
            mwm.disconnect()
            mwm.removeAllListeners()
            Actions.liveactionquizresult({ avg: avg, type: 'replace' })
        }
        //腦波運算
        if (previous_mindwaveTimer != mindwaveTimer && this.state.startQuiz) {
            if (poorSignal == 0) {
                console.log(nextProps.poorSignal)
                console.log(nextProps.quizPoint)
                this.setState({
                    poorSignal: nextProps.poorSignal,
                    point: nextProps.quizPoint ? nextProps.quizPoint : 0
                })
                this.setState({
                    timerCounter: this.state.timerCounter + 1,
                    timeCounterMinus: this.state.timeCounterMinus - 1
                })
                if (this.state.timeCounterMinus <= 5) {
                    this.state.PointArray.push(parseInt(nextProps.quizPoint))
                    console.log(this.state.PointArray);
                }
                this.state.deltaArray.push(nextProps.delta)
                this.state.highAlphaArray.push(nextProps.highAlpha)
                this.state.lowAplphaArray.push(nextProps.lowAplpha)
                this.state.thetaArray.push(nextProps.theta)
                this.state.lowBetaArray.push(nextProps.lowBeta)
                this.state.midGammaArray.push(nextProps.midGamma)
                this.state.highBetaArray.push(nextProps.highBeta)
                this.state.lowGammaArray.push(nextProps.lowGamma)
                console.log({
                    delta: nextProps.delta, highAlpha: nextProps.highAlpha, lowAplpha: nextProps.lowAplpha, theta: nextProps.theta,
                    lowBeta: nextProps.lowBeta, midGamma: nextProps.midGamma, highBeta: nextProps.highBeta, lowGamma: nextProps.lowGamma
                })
                console.log('訊號正常每秒跳一次，目前數值：' + this.state.timerCounter)
            } else {
                this.setState({
                    poorSignal: nextProps.poorSignal,
                })
                console.log('訊號不正常，請調整腦波耳機穿戴位置')
            }
            if (this.state.timerCounter >= this.state.time && this.state.timerCounter != 0 && this.props.poorSignal == 0) {
                this.setState({
                    delta_max: this._getMax(this.state.deltaArray),
                    delta_min: this._getMin(this.state.deltaArray),
                    delta_sd: this._getSD(this.state.deltaArray),
                    delta_avg: this._getAvg(this.state.deltaArray),
                    highAlpha_max: this._getMax(this.state.highAlphaArray),
                    highAlpha_min: this._getMin(this.state.highAlphaArray),
                    highAlpha_sd: this._getSD(this.state.highAlphaArray),
                    highAlpha_avg: this._getAvg(this.state.highAlphaArray),
                    lowAplpha_max: this._getMax(this.state.lowAplphaArray),
                    lowAplpha_min: this._getMin(this.state.lowAplphaArray),
                    lowAplpha_sd: this._getSD(this.state.lowAplphaArray),
                    lowAplpha_avg: this._getAvg(this.state.lowAplphaArray),
                    theta_max: this._getMax(this.state.thetaArray),
                    theta_min: this._getMin(this.state.thetaArray),
                    theta_sd: this._getSD(this.state.thetaArray),
                    theta_avg: this._getAvg(this.state.thetaArray),
                    lowBeta_max: this._getMax(this.state.lowBetaArray),
                    lowBeta_min: this._getMin(this.state.lowBetaArray),
                    lowBeta_sd: this._getSD(this.state.lowBetaArray),
                    lowBeta_avg: this._getAvg(this.state.lowBetaArray),
                    midGamma_max: this._getMax(this.state.midGammaArray),
                    midGamma_min: this._getMin(this.state.midGammaArray),
                    midGamma_sd: this._getSD(this.state.midGammaArray),
                    midGamma_avg: this._getAvg(this.state.midGammaArray),
                    highBeta_max: this._getMax(this.state.highBetaArray),
                    highBeta_min: this._getMin(this.state.highBetaArray),
                    highBeta_sd: this._getSD(this.state.highBetaArray),
                    highBeta_avg: this._getAvg(this.state.highBetaArray),
                    lowGamma_max: this._getMax(this.state.lowGammaArray),
                    lowGamma_min: this._getMin(this.state.lowGammaArray),
                    lowGamma_sd: this._getSD(this.state.lowGammaArray),
                    lowGamma_avg: this._getAvg(this.state.lowGammaArray),
                }, function () {
                    console.log({
                        "deltaBig": this.state.delta_max, "deltaSmall": this.state.delta_min, "deltaAverage": this.state.delta_avg, "deltaSD": this.state.delta_sd,
                        "thetaBig": this.state.theta_max, "thetaSmall": this.state.theta_min, "thetaAverage": this.state.theta_avg, "thetaSD": this.state.theta_sd,
                        "lowAlphaBig": this.state.lowAplpha_max, "lowAlphaSmall": this.state.lowAplpha_min, "lowAlphaAverage": this.state.lowAplpha_avg, "lowAlphaSD": this.state.lowAplpha_sd,
                        "highAlphaBig": this.state.highAlpha_max, "highAlphaSmall": this.state.highAlpha_min, "highAlphaAverage": this.state.highAlpha_avg, "highAlphaSD": this.state.highAlpha_sd,
                        "lowBetaBig": this.state.lowBeta_max, "lowBetaSmall": this.state.lowBeta_min, "lowBetaAverage": this.state.lowBeta_avg, "lowBetaSD": this.state.lowBeta_sd,
                        "highBetaBig": this.state.highBeta_max, "highBetaSmall": this.state.highBeta_min, "highBetaAverage": this.state.highBeta_avg, "highBetaSD": this.state.highBeta_sd,
                        "lowGammaBig": this.state.lowGamma_max, "lowGammaSmall": this.state.lowGamma_min, "lowGammaAverage": this.state.lowGamma_avg, "lowGammaSD": this.state.lowGamma_sd,
                        "midGammaBig": this.state.midGamma_max, "midGammaSmall": this.state.midGamma_min, "midGammaAverage": this.state.midGamma_avg, "midGammaSD": this.state.midGamma_sd,
                    })
                    this.props.getQuizPoint({
                        "deltaBig": this.state.delta_max, "deltaSmall": this.state.delta_min, "deltaAverage": this.state.delta_avg, "deltaSD": this.state.delta_sd,
                        "thetaBig": this.state.theta_max, "thetaSmall": this.state.theta_min, "thetaAverage": this.state.theta_avg, "thetaSD": this.state.theta_sd,
                        "lowAlphaBig": this.state.lowAplpha_max, "lowAlphaSmall": this.state.lowAplpha_min, "lowAlphaAverage": this.state.lowAplpha_avg, "lowAlphaSD": this.state.lowAplpha_sd,
                        "highAlphaBig": this.state.highAlpha_max, "highAlphaSmall": this.state.highAlpha_min, "highAlphaAverage": this.state.highAlpha_avg, "highAlphaSD": this.state.highAlpha_sd,
                        "lowBetaBig": this.state.lowBeta_max, "lowBetaSmall": this.state.lowBeta_min, "lowBetaAverage": this.state.lowBeta_avg, "lowBetaSD": this.state.lowBeta_sd,
                        "highBetaBig": this.state.highBeta_max, "highBetaSmall": this.state.highBeta_min, "highBetaAverage": this.state.highBeta_avg, "highBetaSD": this.state.highBeta_sd,
                        "lowGammaBig": this.state.lowGamma_max, "lowGammaSmall": this.state.lowGamma_min, "lowGammaAverage": this.state.lowGamma_avg, "lowGammaSD": this.state.lowGamma_sd,
                        "midGammaBig": this.state.midGamma_max, "midGammaSmall": this.state.midGamma_min, "midGammaAverage": this.state.midGamma_avg, "midGammaSD": this.state.midGamma_sd,
                    }, this.state.sex);
                    this.state.deltaArray.splice(0, 1)
                    this.state.highAlphaArray.splice(0, 1)
                    this.state.lowAplphaArray.splice(0, 1)
                    this.state.thetaArray.splice(0, 1)
                    this.state.lowBetaArray.splice(0, 1)
                    this.state.midGammaArray.splice(0, 1)
                    this.state.highBetaArray.splice(0, 1)
                    this.state.lowGammaArray.splice(0, 1)
                })
            }
        }
    }
    render() {
        {
            if (!this.state.mindwaveConnected) {
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/home.png')} style={styles.backGroundImage}>
                            <View style={styles.bigView}>
                                <View style={styles.topView}>
                                    <TopBar title='確認裝置連接' closeMindWaveListener={true} />
                                </View>
                                <View style={styles.contentView}>
                                    <View>
                                        <ActivityIndicator
                                            animating={this.state.animating}
                                            style={[styles.centering, { height: 30 }]}
                                            size="large" />
                                    </View>
                                    <View style={styles.checkMindWaveTextView}>
                                        <Text style={styles.checkMindWaveText}>
                                            腦波耳機連線中
                                        </Text>
                                        <Text style={styles.checkMindWaveText}>
                                            請稍候
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </Image>
                    </View>
                );
            }
            else if (!this.state.poorSignalChecked) {
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/home.png')} style={styles.backGroundImage}>
                            <View style={styles.bigView}>
                                <View style={styles.topView}>
                                    <TopBar title='檢查腦波訊號' closeMindWaveListener={true} />
                                </View>
                                <View style={styles.contentView}>
                                    <View>
                                        <Text style={styles.poorSignalText}>{this.props.poorSignal}</Text>
                                    </View>
                                    <View>
                                        <ActivityIndicator
                                            animating={this.state.animating}
                                            style={[styles.centering, { height: 80 }]}
                                            size="large"
                                        />
                                    </View>
                                    <View style={styles.poorSignalTextView}>
                                        <Text style={styles.checkPoorSignalText}>
                                            請調整腦波耳機位置
                            </Text>
                                        <Text style={styles.checkPoorSignalText}>
                                            直到訊號值歸零
                            </Text>
                                        <View>
                                            {
                                                this.props.poorSignal == 0 ? <Text style={styles.checkPoorSignalTimerMessage}>倒數{this.state.poorSingalTimer}秒</Text> : null
                                            }
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </Image>
                    </View>
                );
            }
            else if (!this.state.startQuiz) {
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/home.png')} style={styles.backGroundImage}>
                            <View style={styles.bigView}>
                                <View style={styles.topView}>
                                    <TopBar title='測驗確認' closeMindWaveListener={true} />
                                </View>
                                <View style={styles.contentView}>
                                    <TouchableHighlight style={styles.startQuizButton} onPress={() =>
                                        this.startQuiz()
                                    }>
                                        <Text style={styles.startQuizButtonText}>開始測驗</Text>
                                    </TouchableHighlight>
                                    <View style={styles.startQuizTextView}>
                                        <Text style={styles.startQuizTextMessage}>
                                            ＊請盡量避免頭部晃動
                                        </Text>
                                    </View>
                                </View>

                            </View>
                        </Image>
                    </View>
                );
            }
            else {
                const pointstandard = 70
                const point = this.state.point
                const poorSignal = this.state.poorSignal
                return (
                    <View style={styles.container}>
                        <Image source={require('../images/home.png')} style={styles.backGroundImage}>
                            <View style={styles.LiveActionQuizBigView}>
                                <View style={styles.titleView}>
                                    <Text style={styles.titleText}>
                                        {this.state.titleText}
                                    </Text>
                                </View>
                                <View style={styles.liveActionQuizTopView}>
                                    <Text style={styles.topText}>
                                        心動指數：
                                    </Text>
                                </View>
                                <View style={styles.secondView}>
                                    <Text style={styles.pointText}>
                                        {
                                            this.state.timerCounter <= 5 ? '--' :
                                                poorSignal != 0 ? '??' : point

                                        } 分
                                    </Text>
                                </View>
                                <View style={styles.thirdView}>
                                    <View style={styles.messageView}>
                                        {poorSignal != 0 ? <Text style={styles.messageText}>
                                            訊號不正常
                                    </Text> :
                                            <Text style={styles.messageText}>
                                                {this.state.timerCounter <= 5 ? this.state.text
                                                    :
                                                    point < pointstandard ? '猴!' : '哇！'
                                                }
                                            </Text>
                                        }
                                        {poorSignal != 0 ?
                                            <Text style={styles.messageText}>
                                                請調整腦波耳機
                                        </Text> :
                                            <Text style={styles.messageText}>
                                                {this.state.timerCounter <= 5 ? null
                                                    :
                                                    point < pointstandard ? '你有一點心動了唷' : '心動指數要爆表了'
                                                }
                                                {}
                                            </Text>
                                        }
                                    </View>
                                </View>
                                <View style={styles.bottomView}>
                                    <Text style={styles.timeCounterText}>剩餘</Text>
                                    <Text style={styles.timeCounterBigText}>{this.state.timeCounterMinus}</Text>
                                    <Text style={styles.timeCounterText}>秒</Text>
                                </View>
                            </View>
                        </Image>
                    </View>
                );
            }
        }
    }
}
const styles = StyleSheet.create({
    bigView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
    },
    LiveActionQuizBigView: {
        justifyContent: 'flex-start',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    backGroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    topView: {
        alignItems: 'center',
        width: width,
    },
    contentView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
    },
    //檢查腦波裝置連接
    checkMindWaveTextView: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkMindWaveText: {
        fontSize: 25,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        fontFamily: 'Euphemia UCAS',
    },
    //檢查訊號值是否正常
    poorSignalText: {
        fontSize: 80,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        fontFamily: 'Euphemia UCAS',
    },
    poorSignalTextView: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    checkPoorSignalText: {
        fontSize: 25,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        fontFamily: 'Euphemia UCAS',
    },
    checkPoorSignalTimerMessage: {
        fontSize: 30,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        fontFamily: 'Euphemia UCAS',
    },
    //開始測驗頁面
    startQuizButton: {
        width: width * 0.5,
        height: width * 0.5,
        justifyContent: 'center',
        marginBottom: height * 0.05,
        alignItems: 'center',
        borderRadius: 100,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },
    startQuizButtonText: {
        fontSize: 30,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        color: 'white',
        fontFamily: 'Euphemia UCAS',
    },
    startQuizTextView: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    startQuizMessage: {
        fontSize: 25,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        color: 'rgba(250, 0, 0, 1)',
        fontFamily: 'Euphemia UCAS',
    },
    //LiveActionQuiz
    titleView: {
        width: width,
        alignItems: 'center',
        marginTop: height * 0.1,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        flex: 0
    },
    liveActionQuizTopView: {
        width: width,
        height: 50,
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(52, 52, 52, 0)',
        marginLeft: 20,
        marginTop: height * 0.05,
        flex: 0
    },
    secondView: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0
    },
    thirdView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * 0.3,
        flex: 0
    },
    bottomView: {
        height: height * 0.08,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'rgba(52, 52, 52, 0)',
        flex: 0
    },
    pointText: {
        fontSize: 90,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        color: '#E86868',
        fontFamily: 'Euphemia UCAS',
    },
    button: {
        backgroundColor: 'rgba(52,52,52,0.6)',
        width: 100,
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 35
    },
    topText: {
        fontSize: 25
    },
    messageView: {
        backgroundColor: 'rgba(52, 52, 52, 0.7)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageViewHide: {
        backgroundColor: 'rgba(52, 52, 52, 0)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageText: {
        fontSize: 30,
        color: 'white'
    },
    timeCounterText: {
        fontSize: 30
    },
    timeCounterBigText: {
        fontSize: 50
    }

});

export default ImageQuiz;
