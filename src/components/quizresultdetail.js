import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import SinglePicker from 'mkp-react-native-picker';

class QuizResultDetail extends Component {
    constructor() {
        super();
        var list = [{ index: 0, img: "1-randomTest-random-1.png", score: 92 },
        { index: 1, img: "4-randomTest-random-1.png", score: 95 }, { index: 3, img: "3-randomTest-random-1.png", score: 88 }];
        this.state = {
            selectedOption: '基本資料',
            slideIndex: 0,
            index: 0,
            page: 1,
            score: list[0].score,
            length: list.length,
        }
    }
    left() {
        const left = (<Icon name="chevron-thin-left" size={40} color="gray" />)
        if (this.state.index !== 0) {
            return (
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor='rgba(52,52,52,0)'
                    onPress={() => this._carousel.snapToPrev()}>
                    {left}
                </TouchableHighlight>
            );
        }
    }
    right() {
        const right = (<Icon name="chevron-thin-right" size={40} color="gray" />)
        if (this.state.index !== (this.state.length - 1)) {
            return (
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor='rgba(52,52,52,0)'
                    onPress={() => this._carousel.snapToNext()}>
                    {right}
                </TouchableHighlight>
            );
        }
    }
    render() {
        const camera = (<Icon name="camera" size={30} color="gray" />)

        var list = [{ index: 0, img: "1-randomTest-random-1.png", score: 92 },
        { index: 1, img: "4-randomTest-random-1.png", score: 95 }, { index: 2, img: "3-randomTest-random-1.png", score: 88 }];
        var self = this;
        var content = list.map(function (data, index) {
            return (
                <View key={index} style={styles.slide1}>
                    <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + data.img }} style={styles.square} />
                </View>);
        })
        return (
            <View style={styles.bigview}>
                <View style={styles.topView}>
                    <View style={styles.counterView}>
                        <Text style={{ fontSize: 20, color: 'white' }}>{this.state.index + 1}/{this.state.length}</Text>
                    </View>
                </View>
                <View style={styles.carouselView}>
                    <View style={styles.leftView}>
                        {this.left()}
                    </View>
                    <Carousel
                        ref={(carousel) => { this._carousel = carousel; }}
                        sliderWidth={width - 80}
                        itemWidth={width - 80}
                        directionalLockEnabled={true}
                        centerContent={true}
                        currentIndex={this.state.currentIndex}
                        onSnapToItem={(item) => {
                            // var self=this;
                            list.map(function (data, index) {
                                if (item === data.index) {
                                    self.setState({
                                        score: data.score,
                                        index: index
                                    })
                                }
                            })
                        }}>
                        {content}

                    </Carousel>
                    <View style={styles.leftView}>
                        {this.right()}
                    </View>

                </View>
                <View style={styles.content}>
                    <Image source={require('../images/heartpersent.png')} style={styles.heartpersent} />
                </View>
                <View >
                    <Text style={styles.score}>心動分數：{this.state.score}</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    square: {
        height: height * 0.5,
        width: width * 0.75,
        borderWidth: 5,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
    heartpersent: {
        height: 80,
        width: 80,
        borderColor: 'rgba(52, 52, 52, 0.8)',
        marginBottom: 15,
        marginTop: 15,
    },
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        // flexDirection: 'column',
        alignItems: 'center',
    },
    leftView: {
        width: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    carouselView: {
        height: height * 0.5,
        // backgroundColor: 'red',
        flexDirection: 'row'
    },
    slide1: {
        justifyContent: 'center',
        paddingLeft: width * 0.1,
        paddingRight: width * 0.1,
        width: width - 80,
        alignItems: 'center',
        flexDirection: 'column',
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#494949',
        marginTop: 20,
    },
    score: {
        fontSize: 23,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    topView: {
        marginTop: height * 0.05,
        marginBottom: height * 0.02,
        padding: 8,
        backgroundColor: 'rgba(52,52,52,0.5)',
    },
});

export default QuizResultDetail;

