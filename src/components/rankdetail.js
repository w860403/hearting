import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ListView
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import SinglePicker from 'mkp-react-native-picker';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

class RankDetail extends Component {
    constructor() {
        super();
        // var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            selectedOption: '隨機',
            slideIndex: 0,
            dataSource: {}
        }
    }
    _renderRow(rowData, sectionID, rowID) {
        console.log("start renderRow");
        console.log(rowData);
        const down = (<Icon name="chevron-down" size={20} color="#383838" />)
        return (
            <TouchableOpacity>
                <View style={styles.listView}>
                    <View style={styles.listRank}>
                        <Text style={styles.numText}>
                            {rowData.rank}
                        </Text>
                    </View>
                    <View style={styles.imageView}>
                        <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.img }} style={styles.square} />
                    </View>
                    <View style={styles.listNum}>
                        <Text style={styles.numText}>
                            {rowData.people}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedOption == '隨機' && this.state.slideIndex !== 0) {
            this._carousel.snapToItem(0)
            console.log('選擇 隨機')
        } else if (this.state.selectedOption == '單人' && this.state.slideIndex !== 1) {
            this._carousel.snapToItem(1)
            console.log('選擇 單人')
        } else if (this.state.selectedOption == '背影' && this.state.slideIndex !== 2) {
            this._carousel.snapToItem(2)
            console.log('選擇 背影')
        } else if (this.state.selectedOption == '零死角' && this.state.slideIndex !== 3) {
            this._carousel.snapToItem(3)
            console.log('選擇 零死角')
        }

    }

    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
        const options = [
            '隨機',
            '單人',
            '背影',
            '零死角'
        ];
        function setSelectedOption(selectedOption) {
            this.setState({
                selectedOption: selectedOption
            });
        }
        function renderOption(option, selected, onSelect, index) {
            const style = selected ? { fontWeight: 'bold' } : {};
            return (
                <TouchableWithoutFeedback onPress={onSelect} key={index}>
                    <Text style={style}>{option}</Text>
                </TouchableWithoutFeedback>
            );
        }

        function renderContainer(optionNodes) {
            return <View>{optionNodes}</View>;
        }
        var list = [
            { index: 0, title: "隨機", data: [{ img: '3-randomTest-random-1.png', rank: '1', people: '134' }, { img: '1-randomTest-random-1.png', rank: '2', people: '345' }] },
            { index: 1, title: "單人", data: [{ img: '2-randomTest-random-1.png', rank: '1', people: '45' }] },
            { index: 2, title: "背影", data: [{ img: '4-randomTest-random-1.png', rank: '1', people: '345' }, { img: '6-randomTest-random-1.png', rank: '2', people: '134' }] },
            { index: 3, title: "零死角", data: [{ img: '5-randomTest-random-1.png', rank: '1', people: '2' }, { img: '7-randomTest-random-1.png', rank: '2', people: '134' }] }
        ];
        var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var self = this;
        var content = list.map(function (data, index) {
            console.log(data.data);
            var dataSource = ds.cloneWithRows(data.data)
            return (
                <View key={index} style={styles.slide1} >
                    <View style={styles.content}>
                        <View style={styles.titleListRank}>
                            <Text style={styles.listTitleText}>
                                名次
                            </Text>
                        </View>
                        <View style={styles.titleListImg}>
                            <Text style={styles.listTitleText}>
                                
                            </Text>
                        </View>
                        <View style={styles.titleListNum}>
                            <Text style={styles.listTitleText}>
                                心動人數
                            </Text>
                        </View>
                    </View>
                    <View style={styles.contentList}>
                        <ListView
                            dataSource={dataSource}
                            renderRow={self._renderRow}
                            style={{ flexDirection: 'column', }}
                        />
                    </View>
                </View>);
        })
        return (
            <View style={styles.bigview}>
                <View style={styles.toparea}>
                    <SegmentedControls
                        tint={'rgba(52, 52, 52, 0.8)'}
                        selectedTint={'#FFFFFF'}
                        selectedBackgroundColor={'rgba(52, 52, 52, 0.9)'}
                        backTint={'rgba(52, 52, 52, 0)'}
                        options={options}
                        allowFontScaling={false} // default: true
                        onSelection={setSelectedOption.bind(this)}
                        selectedOption={this.state.selectedOption}
                        optionStyles={{ fontFamily: 'AvenirNext-Medium' }}
                        optionContainerStyle={{ flex: 1 }}
                    />
                </View>
                {
                    //list
                }
                <Carousel
                    ref={(carousel) => { this._carousel = carousel; }}
                    sliderWidth={width}
                    itemWidth={width}
                    directionalLockEnabled={true}
                    centerContent={true}
                    // horizontal={true}
                    currentIndex={this.state.currentIndex}
                    onSnapToItem={(item) => {
                        if (item === 0) {
                            console.log('0')
                            this.setState({
                                selectedOption: '隨機',
                                slideIndex: 0
                            });
                        } else if (item === 1) {
                            console.log('1')
                            this.setState({
                                selectedOption: '單人',
                                slideIndex: 1
                            });
                        } else if (item === 2) {
                            console.log('2')
                            this.setState({
                                selectedOption: '背影',
                                slideIndex: 2
                            });
                        } else if (item === 3) {
                            console.log('3')
                            this.setState({
                                selectedOption: '零死角',
                                slideIndex: 3
                            });
                        }
                    }}>
                    {content}
                </Carousel>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    toparea: {
        marginTop: height * 0.04,
        marginBottom: height * 0.01,
        width: width * 0.85,
        height: height * 0.07
    },
    bigview: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',

    },
    square: {
        height: 90,
        width: 90,
        borderWidth: 3,
        borderColor: 'rgba(52, 52, 52, 0.8)',
        marginLeft:10
    },
    content: {
        flexDirection: 'row',
        paddingLeft: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        paddingBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',

    },
    slide1: {
        // justifyContent: 'center',
        paddingLeft: width * 0.1,
        paddingRight: width * 0.1,

        width: width,
        height: 600,
        // alignItems: 'center',
    },
    titleListImg: {
        width: width * 0.35,
    },
    titleListRank: {
        width: width * 0.15,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    titleListNum: {
        width: width * 0.25,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    listView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        justifyContent: 'center',
    },
    listRank: {
        width: width * 0.1,
        // justifyContent: 'center',
        alignItems: 'center',
    },
    imageView: {
        width: width * 0.4,
    },
    listNum: {
        width: width * 0.25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    numText: {
        fontSize: 35,
        color: '#383838',
        backgroundColor: 'rgba(0,0,0,0)',
        fontFamily: 'Euphemia UCAS',
    },
    listTitleText: {
        fontSize: 18,
        color: '#383838',
        fontFamily: 'Euphemia UCAS',
        backgroundColor: 'rgba(0,0,0,0)'
    }
});

export default RankDetail;

