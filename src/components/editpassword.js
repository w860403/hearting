import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
    PixelRatio,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import SinglePicker from 'mkp-react-native-picker';
import CheckBox from 'react-native-icon-checkbox';
import ImagePicker from './imagepicker';
import ImagePicker2 from 'react-native-image-picker';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
class EditMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <View style={styles.bigview}>
                <View >
                    <ScrollView
                        automaticallyAdjustContentInsets={false}
                        contentInset={{ top: 0 }}
                        style={styles.scrollView}>
                        <View style={styles.space}></View>
                        <View style={styles.content}>
                            <TextInput
                                placeholder="新密碼..."
                                placeholderTextColor="gray"
                                style={styles.textInput}
                                onChangeText={
                                    (Name) =>
                                        this.setState({
                                            memberData: {
                                                ...this.state.memberData,
                                                Name
                                            }
                                        })
                                }
                                value={this.state.memberData.Name} />
                        </View>
                        <View style={styles.space}></View>
                        <View style={styles.content}>
                            <TextInput
                                placeholder="確認密碼..."
                                placeholderTextColor="gray"
                                style={styles.textInput}
                                onChangeText={
                                    (Email) =>
                                        this.setState({
                                            memberData: {
                                                ...this.state.memberData,
                                                Email
                                            }
                                        })
                                }
                                value={this.state.memberData.Email}
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={styles.space}></View>
                        <View style={styles.content}>
                            <Text style={styles.columnText}>密碼</Text>
                            <View style={styles.passwordBtn}>
                                <TouchableOpacity>
                                    <Text style={{ color: 'white', fontSize: 15 }}>修改密碼</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    bigview: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',

    },
    content: {
        // justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',

    },
    columnText: {
        fontSize: 20,
        marginRight: 10,
        marginTop: 5
    },
    textInput: {
        width: width * 0.6,
        // alignItems: 'stretch',
        height: 30,
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',

    },
    space: {
        height: 20,
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'black',
        marginTop: 20,

    },
    scrollView: {
        paddingHorizontal: width * 0.12,
        width: width,
        height: 500,
    },
});

export default EditMember;

