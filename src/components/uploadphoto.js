import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import ImagePicker2 from 'react-native-image-picker';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

class UploadPhoto extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatarSource: null
        }
    }
    left() {
        const left = (<Icon name="chevron-thin-left" size={40} color="gray" />)
        if (this.state.index !== 0) {
            return (
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor='rgba(52,52,52,0)'
                    onPress={() => this._carousel.snapToPrev()}>
                    {left}
                </TouchableHighlight>
            );
        }
    }
    right() {
        const right = (<Icon name="chevron-thin-right" size={40} color="gray" />)
        if (this.state.index !== (this.props.imageType.type.length - 1)) {
            return (
                <TouchableHighlight
                    activeOpacity={0.8}
                    underlayColor='rgba(52,52,52,0)'
                    onPress={() => this._carousel.snapToNext()}>
                    {right}
                </TouchableHighlight>
            );
        }
    }
    selectPhotoTapped = (index) => {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        ImagePicker2.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.state.imageArray[index] = source
                this.setState({
                    imageData: response.data,
                    imageArray: this.state.imageArray
                });
                this.props.oploadTestImage(this.props.account, this.props.imageType.fun, this.props.imageType.type[index].title, this.state.imageData)
            }
        });
    }
    render() {
        var self = this;
        var content = this.props.imageType.type.map(function (data, index) {
            return (
                <View key={index} style={styles.slide1}>
                    {
                        <Image source={self.state.imageArray[index]} style={styles.square} />
                    }
                    <Text style={styles.uploadphotoDetail}>{data.title} 0/10000</Text>
                </View>);
        })
        if (!this.state.loaded)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={[styles.centering, { height: 80 }]}
                    size="large"
                />
            );
        return (
            <View style={styles.bigview}>
                <View style={styles.topView}>
                    <View >
                        <Text style={styles.uploadphotoText}>請上傳 {this.props.imageType.type.length} 張照片</Text>
                    </View>
                    <View >
                        <Text style={styles.warnText}>＊請將照片傳好傳滿</Text>
                    </View>
                    <View style={styles.counterView}>
                        <Text style={{ fontSize: 20, color: 'white' }}>{this.state.index + 1}/{this.props.imageType.type.length}</Text>
                    </View>
                </View>
                <View style={styles.carouselView}>
                    <View style={styles.leftView}>
                        {this.left()}
                    </View>
                    <Carousel
                        ref={(carousel) => { this._carousel = carousel; }}
                        sliderWidth={width - 80}
                        itemWidth={width - 80}
                        directionalLockEnabled={true}
                        centerContent={true}
                        currentIndex={this.state.currentIndex}
                        onSnapToItem={(item) => {
                            // var self=this;
                            // this.props.imageType.type.map(function (data, index) {
                            //     if (item === data.index) {
                            //         self.setState({
                            //             index: index
                            //         })
                            //     }
                            // })
                            this.props.imageType.type.map((data, key) => {
                                this.setState({ index: key })
                            })
                        }}>
                        {content}

                    </Carousel>
                    <View style={styles.leftView}>
                        {this.right()}
                    </View>

                </View>
                <TouchableOpacity onPress={() => this.selectPhotoTapped(this.state.index)}>
                    <View style={styles.button}>
                        <Text>上傳圖片</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({

    square: {
        height: width * 0.7,
        width: width * 0.7,
        borderColor: 'rgba(52, 52, 52, 0.8)',
        marginBottom: 5,
        borderWidth: 5,
    },
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',

    },
    leftView: {
        width: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    carouselView: {
        height: height * 0.45,
        // backgroundColor: 'red',
        flexDirection: 'row'
    },
    uploadphotoText: {
        fontSize: 25,
        color: "#494949",
        backgroundColor: 'rgba(0,0,0,0)',
        marginBottom: 10,
        marginTop: 10
    },
    warnText: {
        color: 'red',
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 17,
        marginBottom: 20,
    },
    uploadphotoDetail: {
        fontSize: 15,
        color: "#494949",
        backgroundColor: 'rgba(0,0,0,0)',
    },
    slide1: {
        justifyContent: 'center',
        paddingLeft: width * 0.1,
        paddingRight: width * 0.1,
        width: width - 80,
        alignItems: 'center',
        flexDirection: 'column',
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#494949',
        marginTop: 15,
    },
    topView: {
        marginTop: height * 0.03,
        marginBottom: height * 0.01,
        alignItems: 'center',
        justifyContent: 'center',
    },
    counterView: {
        padding: 8,
        backgroundColor: 'rgba(52,52,52,0.5)',
    }
});

export default UploadPhoto;
