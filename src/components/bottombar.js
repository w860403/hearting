import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TextInput,
    AlertIOS,
    AsyncStorage,
    ActivityIndicator
} from 'react-native';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
// import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon from 'react-native-vector-icons/Entypo';
var { height, width } = Dimensions.get('window');

class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const member = (<Icon name="v-card" size={20} color="#515151" />)
        const friend = (<Icon name="users" size={20} color="#515151" />)
        const quiz = (<Icon name="heart-outlined" size={20} color="#515151" />)
        const chart = (<Icon name="pie-chart" size={20} color="#515151" />)
        const home = (<Icon name="home" size={20} color="#515151" />)

        const member2 = (<Icon name="v-card" size={20} color="#EDAFCE" />)
        const friend2 = (<Icon name="users" size={20} color="#EDAFCE" />)
        const quiz2 = (<Icon name="heart" size={20} color="#EDAFCE" />)
        const chart2 = (<Icon name="pie-chart" size={20} color="#EDAFCE" />)
        const home2 = (<Icon name="home" size={20} color="#EDAFCE" />)
        return (

            <BottomNavigation
                labelColor="white"
                rippleColor="white"
                style={{ height: 56, elevation: 8, position: 'absolute', left: 0, bottom: 0, right: 0 }}
                onTabChange={(newTabIndex) => this.props.onButtonClick(newTabIndex)}
            >
                <Tab
                    barBackgroundColor="rgba(52,52,52,0.4)"
                    label="首頁"
                    icon={home}
                    activeIcon={home2}
                />
                <Tab
                    barBackgroundColor="rgba(52,52,52,0.4)"
                    label="戀功房"
                    icon={chart}
                    activeIcon={chart2}
                />
                <Tab
                    barBackgroundColor="rgba(52,52,52,0.4)"
                    label="測驗"
                    icon={quiz}
                    activeIcon={quiz2}
                />
                <Tab
                    barBackgroundColor="rgba(52,52,52,0.4)"
                    label="好友"
                    icon={friend}
                    activeIcon={friend2}
                />
                <Tab
                    barBackgroundColor="rgba(52,52,52,0.4)"
                    label="會員"
                    icon={member}
                    activeIcon={member2}
                />
            </BottomNavigation>
        );
    }
}

const styles = StyleSheet.create({

});
export default Home;