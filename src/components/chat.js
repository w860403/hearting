import _ from 'lodash';
import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
    TextInput,
    ListView,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Entypo';
import CheckBox from 'react-native-icon-checkbox';

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: false,
            content: '',
            listHeight: 0,
            should_scrolltoend: false
        }
    }

    componentWillReceiveProps(nextProps) {
        const { chat: previous_chat } = this.props;
        const { chat } = nextProps;
        if (previous_chat != chat) {
            // this.state.loaded = true;
            this.setState({
                should_scrolltoend: true
            })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.should_scrolltoend) {
            this._scrollToEnd();
        }
    }

    _renderRow = (rowData, sectionID, rowID) => {
        if (rowData.account === this.props.account) {
            return (
                <TouchableOpacity>
                    <View style={contentStyle.listViewRight}>
                        <View style={contentStyle.borderView}>
                            <Text style={contentStyle.contentTextRight}>{rowData.Content}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        } else if (rowData.account === this.props.partner) {
            return (
                <TouchableOpacity>
                    <View style={contentStyle.listViewLeft}>
                        <View style={contentStyle.borderView}>
                            <Text style={contentStyle.contentTextLeft}>{rowData.Content}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }

    }
    clearText = () => {
        this.setState({ content: '' })
    }

    _on_content_size_change(contentWidth, contentHeight) {
        clearTimeout(this._pending_scroll_to_end);
        this._pending_scroll_to_end = setTimeout(() => this._scrollToEnd(), 150);
    }

    _scrollToEnd() {
        this.refs._listView.scrollToEnd({ animated: false });
        this.setState({
            should_scrolltoend: false
        });
    }

    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
        var list = [
            { index: 0, content: "你好" },
            { index: 1, content: "哈囉～我是許瑋芩" },
            { index: 0, content: "零死角" },
            { index: 1, content: "背影哈囉～我是許瑋芩哈囉～我是許瑋芩哈囉～我是許瑋芩哈囉～我是許瑋芩哈囉～我是許瑋芩哈囉～我是許瑋芩哈囉～我是許瑋芩" },
            { index: 0, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },
            { index: 0, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },
            { index: 0, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },
            { index: 1, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },
            { index: 1, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },
            { index: 0, content: "零死角零死角零死角零死角零死角零死角零死角零死角零死角" },

        ];
        if (!this.props.chat)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={[styles.centering, { height: 80 }]}
                    size="large"
                />
            );
        var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var dataSource = ds.cloneWithRows(this.props.chat)
        // var content = this.props.chat.map(function (data, index) {
        //     if (data.account === this.props.account) {
        //         return (
        //             <TouchableOpacity>
        //                 <View style={contentStyle.listViewRight}>
        //                     <View style={contentStyle.borderView}>
        //                         <Text style={contentStyle.contentTextRight}>{data.Content}</Text>
        //                     </View>
        //                 </View>
        //             </TouchableOpacity>
        //         )
        //     } else if (data.account === this.props.partner) {
        //         return (
        //             <TouchableOpacity>
        //                 <View style={contentStyle.listViewLeft}>
        //                     <View style={contentStyle.borderView}>
        //                         <Text style={contentStyle.contentTextLeft}>{data.Content}</Text>
        //                     </View>
        //                 </View>
        //             </TouchableOpacity>
        //         )
        //     }
        // })
        let content = _.map(this.props.chat, this._renderRow);
        var self = this;
        return (
            <View style={styles.bigview}>

                <KeyboardAvoidingView keyboardVerticalOffset={50} behavior='position' style={contentStyle.content}>
                    <View style={contentStyle.chatView}>
                        <View style={styles.contentList}>
                            {/*<ListView
                                // ref={(listView) => { this._listView = listView; }}
                                ref='_listView'
                                dataSource={dataSource}
                                renderRow={self._renderRow}
                                style={{ flexDirection: 'column', height: height * 0.75, }}
                                snapToAlignment='end'
                                onContentSizeChange={() => this._on_content_size_change()}
                            />*/}
                            <ScrollView onContentSizeChange={() => this._on_content_size_change()} ref='_listView' style={{ height: height * 0.7, width: width * 0.85 }}>
                                {content}
                            </ScrollView>
                        </View>
                    </View>
                    <View style={contentStyle.inputVuew}>
                        <TextInput
                            placeholder="輸入..."
                            placeholderTextColor="gray"
                            style={contentStyle.textInput}
                            onChangeText={(content) => this.setState({ content })}
                            value={this.state.content}
                        />
                        <View style={contentStyle.startBtn}>
                            <TouchableOpacity onPress={() => {
                                if (this.state.content == '') {
                                    
                                } else {
                                    this.props.postMessage(this.props.account, this.props.partner, this.state.content), this.clearText()

                                }
                            }}>
                                <Text style={{ color: 'black', fontSize: 15 }}>傳送</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        // alignItems: 'center',
    },

});
const contentStyle = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    chatView: {
        height: height * 0.75,
        width: width * 0.85,
        borderWidth: 1,
        borderColor: 'rgba(52, 52, 52, 0.8)',
        backgroundColor: 'rgba(255, 255, 255, 0.7)'
    },
    inputVuew: {
        flexDirection: 'row',
        marginTop: 10
    },
    textInput: {
        width: width * 0.6,
        // alignItems: 'stretch',
        height: 30,
        borderColor: '#383838',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',

    },
    startBtn: {
        marginLeft: 5,
        paddingLeft: width * 0.03,
        paddingRight: width * 0.03,
        paddingTop: width * 0.02,
        paddingBottom: width * 0.02,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        borderWidth: 1,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
    listViewRight: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        // width:width*0.6,        
        paddingTop: 10,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    listViewLeft: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingTop: 10,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
    },
    borderView: {
        borderRadius: 5,
        overflow: 'hidden'
    },
    contentTextRight: {
        maxWidth: width * 0.6,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#a8e7f1',
        fontSize: 15,
    },
    contentTextLeft: {
        maxWidth: width * 0.6,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'pink',
        fontSize: 15,
    }
})

export default Chat;

