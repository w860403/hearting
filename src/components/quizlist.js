import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  TouchableWithoutFeedback
} from 'react-native';
var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import TopBar from '../containers/topbar'
import CharmQuestionModal from './charmquestionmodal'
class QuizList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: '圖片測驗',
      slideIndex: 0
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectedOption == '圖片測驗' && this.state.slideIndex != 0) {
      this._carousel.snapToItem(0)
      console.log('選擇 圖片測驗')
    } else if (this.state.selectedOption == '真人測驗' && this.state.slideIndex != 1) {
      this._carousel.snapToItem(1)
      console.log('選擇 真人測驗')
    }
  }
  render() {
    const options = [
      '圖片測驗',
      '真人測驗'
    ];
    function setSelectedOption(selectedOption) {
      this.setState({
        selectedOption: selectedOption
      });
    }
    function renderOption(option, selected, onSelect, index) {
      const style = selected ? { fontWeight: 'bold' } : {};
      return (
        <TouchableWithoutFeedback onPress={onSelect} key={index}>
          <Text style={style}>{option}</Text>
        </TouchableWithoutFeedback>
      );
    }

    function renderContainer(optionNodes) {

      return <View>{optionNodes}</View>;
    }
    return (
      //從bigview開始寫，container與backgroundimage請放scene
      <View style={styles.bigView}>
        <TopBar title='測驗清單' backButtonVisible={false} lefttext='Back' />
        <View style={styles.topArea}>
          <SegmentedControls
            tint={'rgba(52, 52, 52, 1)'}
            selectedTint={'#FFFFFF'}
            selectedBackgroundColor={'rgba(52, 52, 52, 0.8)'}
            backTint={'rgba(52, 52, 52, 0)'}
            options={options}
            allowFontScaling={false} // default: true
            onSelection={setSelectedOption.bind(this)}
            selectedOption={this.state.selectedOption}
            optionStyles={{ fontFamily: 'AvenirNext-Medium' }}
            optionContainerStyle={{ flex: 1 }}
          />
        </View>
        <View>
        </View>
        <Carousel
          ref={(carousel) => { this._carousel = carousel; }}
          sliderWidth={width}
          itemWidth={width * 0.7 + width * 0.035 * 2}
          directionalLockEnabled={true}
          centerContent={true}
          horizontal={true}
          currentIndex={this.state.currentIndex}
          onSnapToItem={(item) => {
            if (item === 0) {
              console.log('0')
              this.setState({
                selectedOption: '圖片測驗',
                slideIndex: 0
              });
            } else if (item === 1) {
              console.log('1')
              this.setState({
                selectedOption: '真人測驗',
                slideIndex: 1
              });
            }
          }}>
          <View style={styles.slide}>
            <TouchableHighlight
              style={styles.button}
              activeOpacity={0.8}
              underlayColor='rgba(255,255,255,0.5)'
              onPress={() => this.props.on_quiz_button_click('randomTest')}>
              <View style={styles.buttonView}>
                <View style={styles.buttonImageView}>
                  <Image source={require('../images/handsome01.jpg')} style={styles.titleImage}></Image>
                </View>
                <View style={styles.buttonTextView}>
                  <Text style={styles.textView}>會員隨機測驗</Text>
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.button}
              activeOpacity={0.8}
              underlayColor='rgba(255,255,255,0.5)'
              onPress={() => this.props.on_student_quiz_button_click()}>
              <View style={styles.buttonView}>
                <View style={styles.buttonImageView}>
                  <Image source={require('../images/handsome03.jpg')} style={styles.titleImage}></Image>
                </View>
                <View style={styles.buttonTextView}>
                  <Text style={styles.textView}>校園心動人氣王</Text>
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.button}
              activeOpacity={0.8}
              underlayColor='rgba(255,255,255,0.5)'
              onPress={() => this.props.on_teacher_quiz_button_click()}>
              <View style={styles.buttonView}>
                <View style={styles.buttonImageView}>
                  <Image source={require('../images/handsome02.jpg')} style={styles.titleImage}></Image>
                </View>
                <View style={styles.buttonTextView}>
                  <Text style={styles.textView}>資管七王子</Text>
                </View>
              </View>
            </TouchableHighlight>
          </View>
          <View style={styles.slide} >
            <TouchableHighlight
              style={styles.button}
              activeOpacity={0.8}
              underlayColor='rgba(255,255,255,0.5)'
              onPress={() => this.props.onLiveActionQuizButtonClick()}>
              <View style={styles.buttonView}>
                <View style={styles.buttonImageView}>
                  <Image source={require('../images/handsome05.jpg')} style={styles.titleImage}></Image>
                </View>
                <View style={styles.buttonTextView}>
                  <Text style={styles.textView}>現場真人配對</Text>
                </View>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.button}
              activeOpacity={0.8}
              underlayColor='rgba(255,255,255,0.5)'
              onPress={() => this.props.onWifeControlButtonClick()}>
              <View style={styles.buttonView}>
                <View style={styles.buttonImageView}>
                  <Image source={require('../images/handsome04.jpg')} style={styles.titleImage}></Image>
                </View>
                <View style={styles.buttonTextView}>
                  <Text style={styles.textView}>妻管嚴</Text>
                </View>
              </View>
            </TouchableHighlight>
          </View>
        </Carousel>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  topArea: {
    marginTop: height * 0.05,
    marginBottom: height * 0.05,
    width: width * 0.45,
    height: height * 0.05
  },
  titleImage: {
    height: 60,
    width: 60
  },
  bigView: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  slide: {
    justifyContent: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    borderRadius: 3,
    margin: width * 0.035,
    width: width * 0.7,
    height: 400,
    alignItems: 'center',
  },
  textView: {
    marginLeft: width * 0.02,
    fontSize: 18,
    fontFamily: 'Euphemia UCAS',
  },
  button: {
    width: width * 0.6,
    height: height * 0.11,
    backgroundColor: 'rgba(255,255,255,0.7)',
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 5,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonView: {
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonImageView: {
    flex: 0.5
  },
  buttonTextView: {
    flex: 1
  }
});

export default QuizList;

