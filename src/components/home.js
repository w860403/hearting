import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TextInput,
    AlertIOS,
    AsyncStorage,
    ActivityIndicator,
    ListView,
    ScrollView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
// import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon from 'react-native-vector-icons/Entypo';
var { height, width } = Dimensions.get('window');

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            isLogin: false,
            imageUrl: 'http://114.35.74.209/Hearting/img/',
            imageUpload: 'http://114.35.74.209/Hearting/Upload/',
            animating: true,
            loaded: false,
        };
    }
    render() {
        if (!this.state.loaded)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={{ height: 80, justifyContent: 'center', alignItems: 'center' }}
                    size="large"
                />
            );
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var rank_girl = ds.cloneWithRows(this.props.rank_girl);
        var rank_boy = ds.cloneWithRows(this.props.rank_boy);
        return (

            <View style={styles.bigview}>
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    contentInset={{ top: 0 }}
                    style={styles.scrollView}>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 50, marginBottom: 20 }}>
                        <TouchableHighlight
                            activeOpacity={0.8}
                            underlayColor='rgba(0,0,0,0)'
                            onPress={() => this.props.onSignButtonClick()}>
                            <View style={{ backgroundColor: '#ffd0da', marginRight: 20, borderWidth: 5, borderColor: 'gray' }}>
                                <Image source={{ uri: this.state.imageUrl + 'hair.png' }} style={{ width: width * 0.4, height: width * 0.4 }} />
                            </View>
                        </TouchableHighlight>
                        <View style={{ backgroundColor: '#ffd0da', borderWidth: 5, borderColor: 'gray' }}>
                            <Image source={{ uri: this.state.imageUrl + 'proposal.png' }} style={{ width: width * 0.4, height: width * 0.4 }} />
                        </View>
                    </View>
                    <TouchableHighlight
                        activeOpacity={0.8}
                        underlayColor='rgba(0,0,0,0)'
                        onPress={() => this.props.onRankButtonClick(1)}>
                        <View>
                            <View style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', backgroundColor: '#C2EEFC', borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                                <Text style={{ fontSize: 15, marginHorizontal: 10 }}>男生排行榜</Text>
                                <Text style={{ fontSize: 12, marginHorizontal: 10 }}>本週最讓人心動的男生排行！！</Text>
                            </View>
                            <View style={{ backgroundColor: 'rgba(52,52,52,0.2)', paddingBottom: 10 }}>
                                {/*<View style={{ backgroundColor: 'rgba(194,238,252,0.4)', paddingBottom: 10 }}>*/}
                                {
                                    this.props.rank_boy.length != 0 ?
                                        <ListView
                                            horizontal
                                            dataSource={rank_boy}
                                            renderRow={(rowData) =>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableHighlight
                                                        activeOpacity={0.8}
                                                        underlayColor='rgba(0,0,0,0)'
                                                        onPress={() => this.props.onRankButtonClick(1)}>
                                                        <View style={{ height: 90, width: 90, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                                            <Image source={{ uri: this.state.imageUpload + rowData.ImgName }} style={styles.relateImage} >
                                                            </Image>
                                                            <View style={{ backgroundColor: 'gray', borderRadius: 30, zIndex: 1, position: 'absolute', width: 30, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                                                <Text style={{ color: 'white' }}>{rowData.Rank}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableHighlight>

                                                </View>
                                            }
                                        />
                                        :
                                        <View style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 10 }}>
                                            <Text style={{ backgroundColor: 'rgba(0,0,0,0)', fontSize: 15 }}>本週暫無排名</Text>
                                        </View>
                                }
                            </View>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight
                        activeOpacity={0.8}
                        underlayColor='rgba(0,0,0,0)'
                        onPress={() => this.props.onRankButtonClick(0)}>
                        <View style={{ marginTop: 20 }}>
                            <View style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffd0da' }}>
                                <Text style={{ fontSize: 15, marginHorizontal: 10 }}>女生排行榜</Text>
                                <Text style={{ fontSize: 12, marginHorizontal: 10 }}>本週最讓人心動的女生排行！！</Text>
                            </View>
                            <View style={{ backgroundColor: 'rgba(52,52,52,0.2)', paddingBottom: 10 }}>
                                {
                                    this.props.rank_girl.length != 0 ?
                                        <ListView
                                            horizontal
                                            dataSource={rank_girl}
                                            renderRow={(rowData) =>
                                                <View style={{ flexDirection: 'row' }}>
                                                    <TouchableHighlight
                                                        activeOpacity={0.8}
                                                        underlayColor='rgba(0,0,0,0)'
                                                        onPress={() => this.props.onRankButtonClick(0)}>
                                                        <View style={{ height: 90, width: 90, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                                                            <Image source={{ uri: this.state.imageUpload + rowData.ImgName }} style={styles.relateImage} >

                                                            </Image>
                                                            <View style={{ backgroundColor: 'gray', borderRadius: 30, zIndex: 1, position: 'absolute', width: 30, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                                                <Text style={{ color: 'white' }}>{rowData.Rank}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableHighlight>
                                                </View>
                                            }
                                        />
                                        :
                                        <View style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 10 }}>
                                            <Text style={{ backgroundColor: 'rgba(0,0,0,0)', fontSize: 15 }}>本週暫無排名</Text>
                                        </View>
                                }
                            </View>
                        </View>
                    </TouchableHighlight>


                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        height: height - 54
    },
    scrollView: {
        // paddingHorizontal: width * 0.12,
        width: width,
        // height: 500,
    },
    relateImage: {
        // borderColor: 'rgba(52,52,52,0.5)',
        // borderWidth: 1,
        width: 80,
        height: 80,
        marginRight: 5,
        marginLeft: 5,
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 40
    },
});
export default Home;