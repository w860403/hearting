import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    NavigatorIOS,
    AsyncStorage,
    AlertIOS,
    ActivityIndicator,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native';
var { height, width } = Dimensions.get('window');

class LoveLetterInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: '',
            account: this.props.account,
            partner: this.props.partner
        };
    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.bigview}>
                    <Text style={styles.titleText}>
                        把想跟對方說的話都說出來吧!
                    </Text>
                    <View>
                        <TextInput
                            placeholder="UserName..."
                            placeholderTextColor="gray"
                            style={styles.textInput}
                            multiline={true}
                            numberOfLines={10}
                            onChangeText={(content) => this.setState({ content })}
                            value={this.state.content}
                            autoCapitalize="none"
                        />
                    </View>
                    <View style={styles.button}>
                        <Button onPress={() => this.props.commitLoveLetter(this.state.account, this.state.partner, this.state.content)}
                            title="送出"
                            color="white"
                        >
                        </Button>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}
const styles = StyleSheet.create({
    titleText: {
        fontSize: 18,
        margin: 20,
        fontFamily: 'Euphemia UCAS',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    bigview: {
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    textInput: {
        width: width * 0.8,
        height: height * 0.5,
        borderColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',
    },
    space: {
        height: 20,
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'black',
        marginTop: 10,
    },
    error_text: {
        color: '#FF3333'
    }
});
export default LoveLetterInput;
