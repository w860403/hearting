import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';

import { Actions } from 'react-native-router-flux';
import TopBar from './topbar'
var { height, width } = Dimensions.get('window');

class LiveActionQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            point: this.props.point ? this.props.point : 70,
            poorsignal: this.props.poorsignal ? this.props.poorsignal : 0,
            titletext: this.props.titletext ? this.props.titletext : '真人測驗',
            imageSource: ''
        };
    }
    render() {
        const pointstandard = 50
        const point = this.state.point
        const poorsignal = this.state.poorsignal
        return (
            <View style={styles.bigView}>
                <View style={styles.titleView}>
                    <Text style={styles.titleText}>
                        {this.state.titletext}
                    </Text>
                </View>
                <View style={styles.topView}>
                    <Text style={styles.topText}>
                        心動總指數：
                    </Text>
                </View>
                <View style={styles.secondView}>
                    <Text style={styles.pointText}>{this.props.avg} 分</Text>
                </View>
                <View style={styles.thirdView}>
                    <Image style={styles.heartpersent} source={require('../images/heartpersent.png')}>
                    </Image>
                    <TouchableHighlight
                        style={styles.button}
                        activeOpacity={0.8}
                        underlayColor='rgba(52,52,52,0.5)'
                        onPress={() => Actions.pop()}>
                        <Text style={styles.endquizText}>結束測驗</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    normalBackGroundView: {
        width: null,
        height: null,
        flex: 1,
    },
    blackBackGroundView: {
        backgroundColor: 'black',
        width: null,
        height: null,
        flex: 1,
    },
    titleView: {
        width: width,
        alignItems: 'center',
        marginTop: height * 0.1,
        backgroundColor: 'rgba(52, 52, 52, 0)',
    },
    bigView: {
        justifyContent: 'flex-start',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    bigViewBlack: {
        backgroundColor: '#000000',
        justifyContent: 'flex-start',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    textview: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topView: {
        width: width,
        height: 50,
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(52, 52, 52, 0)',
        marginLeft: 20,
        marginTop: height * 0.05
    },
    secondView: {
    },
    thirdView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * 0.4,
    },
    bottomView: {
        height: height * 0.08,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'rgba(52, 52, 52, 0)',
    },
    pointText: {
        fontSize: 90,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        color: '#E86868',
        fontFamily: 'Euphemia UCAS',
    },
    button: {
        backgroundColor: 'rgba(52,52,52,0.6)',
        width: 100,
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 35
    },
    topText: {
        fontSize: 25
    },
    messageView: {
        backgroundColor: 'rgba(52, 52, 52, 0.7)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageViewHide: {
        backgroundColor: 'rgba(52, 52, 52, 0)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageText: {
        fontSize: 30,
        color: 'white'
    },
    timeCounterText: {
        fontSize: 30
    },
    timeCounterBigText: {
        fontSize: 50
    },
    heartpersent: {
        width: 200,
        height: 185,
        marginBottom: 30
    },
    endquizText: {
        color: 'white',
        fontSize: 22
    },
});

export default LiveActionQuiz;
