import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ListView,
    Modal,
    processColor
} from 'react-native';
import reactAddonsUpdate from 'react-addons-update';
var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Entypo';
import SinglePicker from 'mkp-react-native-picker';
import { LineChart, BarChart } from 'react-native-charts-wrapper';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
var sex2 = [
    { label: '男', value: 0 },
    { label: '女', value: 1 }
];
const sex = [
    { key: 0, value: "男", },
    { key: 1, value: "女", }
];
const constellation = [
    { key: 0, value: "摩羯", },
    { key: 1, value: "水瓶", },
    { key: 2, value: "雙魚", },
    { key: 3, value: "牡羊", },
    { key: 4, value: "金牛", },
    { key: 5, value: "雙子", },
    { key: 6, value: "巨蟹", },
    { key: 7, value: "獅子", },
    { key: 8, value: "處女", },
    { key: 9, value: "天枰", },
    { key: 10, value: "天蠍", },
    { key: 11, value: "射手", },
];
const type = [
    { key: 0, value: "髮型", eng: 'Hairstyle' },
    { key: 1, value: "身材", eng: 'Stature' },
    { key: 2, value: "身高", eng: 'Height' },
    { key: 3, value: "第一眼看的部位", eng: 'FirstSee' },
    { key: 4, value: "外觀", eng: 'Style' },
    { key: 5, value: "年齡", eng: 'LikeAge' },
    { key: 6, value: "臉型", eng: 'FaceType' },
];
class SignAnalysis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            animating: true,
            loaded: true,
            isModal: false,
            modalVisible: true,
            constellation: '摩羯',
            sex: '男',
            type: 'Hairstyle',
            typeName: '髮型',
            other: '女',
            data: {},
            // legend: {
            //     enabled: true,
            //     textColor: processColor('blue'),
            //     textSize: 12,
            //     position: 'BELOW_CHART_RIGHT',
            //     form: 'SQUARE',
            //     formSize: 14,
            //     xEntrySpace: 20,
            //     yEntrySpace: 20,
            //     formToTextSpace: 10,
            //     wordWrapEnabled: true,
            //     maxSizePercent: 0.5,
            //     custom: {
            //         colors: [processColor('green')],
            //         labels: ['星座分析']
            //     }
            // },
            legend: {
                enabled: true,
                textSize: 14,
                form: 'SQUARE',
                formSize: 14,
                xEntrySpace: 10,
                yEntrySpace: 5,
                formToTextSpace: 5,
                wordWrapEnabled: true,
                maxSizePercent: 0.5
            },

            marker: {
                enabled: true,
                backgroundTint: processColor('teal'),
                markerColor: processColor('#F0C0FF8C'),
                textColor: processColor('white'),
            },
            ary: []
        }
    }
    setModalVisible = (visible, imageName) => {
        console.log(imageName);
        this.setState({ modalVisible: visible, imageName: imageName });
    }
    closeModalVisible = (visible, imageName) => {
        this.setState({
            modalVisible: visible,
            imageName: imageName,
            imgWidth: 0,
            imgHeight: 0
        });
    }
    onConfirm = (sex, constellation, type) => {
        this.setState({
            isModal: true,
            loaded: false,
        });
        this.props.getAnalysis(sex, constellation, type)
    }
    componentWillReceiveProps(nextProps) {
        const { analysis: previous_analysis } = this.props;
        const { analysis } = nextProps;
        var obj = {};
        var ary = [];
        var ary2 = [];        
        var self = this;
        if (previous_analysis != analysis) {
            analysis.map(function (data, index) {
                if (self.state.other == data.sex) {
                    obj = { y: data.cnt };
                    ary.push(obj);
                    ary2.push(data.Com_txt)
                }
            })
            this.setState(Object.assign({}, this.state, {
                loaded: true,
                animating: false,
                // data: {
                //     dataSets: [{
                //         values: ary,
                //         label: 'Company Dashed',
                //         config: {
                //             color: processColor('green'),
                //             drawFilled: true,
                //             fillColor: processColor('green'),
                //             fillAlpha: 50,
                //         }
                //     }],
                // },
                // xAxis: {
                //     limitLines: [
                //         { limit: 5 }
                //     ],
                //     valueFormatter: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5']
                // },
                data: {
                    dataSets: [{
                        values: ary,
                        label: 'Bar dataSet',
                        config: {
                            // color: processColor('teal'),
                            barSpacePercent: 40,
                            barShadowColor: processColor('lightgrey'),
                            highlightAlpha: 90,
                            highlightColor: processColor('red'),
                        }
                    }],
                },
                xAxis: {
                    valueFormatter: ary2,
                    granularityEnabled: true,
                    granularity: 1,
                }
            }))
        }
    }
    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }
    }
    render() {
        if (!this.state.loaded)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={[styles.centering, { height: 80 }]}
                    size="large"
                />
            );
        if (!this.state.isModal)
            return (
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.state.modalVisible}>
                    <View style={{ width: width, height: height, backgroundColor: 'rgba(52,52,52,0.5)', justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ width: width * 0.7, height: height * 0.5, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, marginBottom: 10 }}>不同星座的人喜歡</Text>
                            <View>
                                <Text style={styles.columnText}>請選擇星座：</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.constellation.show();
                                    }}>
                                    <TextInput
                                        placeholder="請選擇"
                                        placeholderTextColor="gray"
                                        style={styles.dropdown}
                                        editable={false}
                                        value={this.state.constellation}
                                    />
                                </TouchableOpacity>
                                <SinglePicker
                                    lang="en-US"
                                    ref={ref => this.constellation = ref}
                                    onConfirm={(option) => {
                                        this.setState({ constellation: option.value })
                                    }}
                                    options={constellation} />
                            </View>
                            <View>
                                <Text style={styles.columnText}>請選擇{this.state.constellation}男或{this.state.constellation}女：</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.sex.show();
                                    }}>
                                    <TextInput
                                        placeholder="請選擇"
                                        placeholderTextColor="gray"
                                        style={styles.dropdown}
                                        editable={false}
                                        value={this.state.sex}
                                    />
                                </TouchableOpacity>
                                <SinglePicker
                                    lang="en-US"
                                    ref={ref => this.sex = ref}
                                    onConfirm={(option) => {
                                        this.setState({ sex: option.value })
                                    }}
                                    options={sex} />
                            </View>
                            <View>
                                <Text style={styles.columnText}>請選擇類別：</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.type.show();
                                    }}>
                                    <TextInput
                                        placeholder="請選擇"
                                        placeholderTextColor="gray"
                                        style={styles.dropdown}
                                        editable={false}
                                        value={this.state.typeName}
                                    />
                                </TouchableOpacity>
                                <SinglePicker
                                    lang="en-US"
                                    ref={ref => this.type = ref}
                                    onConfirm={(option) => {
                                        this.setState({ type: option.eng, typeName: option.value })
                                    }}
                                    options={type} />
                            </View>
                            <TouchableOpacity
                                onPress={() => {
                                    this.onConfirm(this.state.sex, this.state.constellation, this.state.type);
                                }}>
                                <Text style={{ marginTop: 20 }}>確認</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            );

        return (
            < View style={styles.bigview} >
                <TouchableOpacity
                    onPress={() => {
                        this.setState({
                            isModal: false
                        })
                    }}>
                    <TextInput
                        placeholder="請選擇"
                        placeholderTextColor="gray"
                        style={styles.dropdown2}
                        editable={false}
                        value={this.state.constellation + this.state.sex + '喜歡的' + this.state.other + '生' + this.state.typeName + '分布'}
                    />
                </TouchableOpacity>
                <View style={{ height: 80, marginTop: 20 }}>
                    <RadioForm
                        radio_props={sex2}
                        initial={1}
                        formHorizontal={true}
                        onPress={(value) => {
                            var self = this;
                            var ary = [];
                            this.props.analysis.map(function (data, index) {
                                var obj = {};
                                if (value == 0) {
                                    self.setState({ other: '男' })
                                    if (data.sex == "男") {
                                        obj = { y: data.cnt };
                                        ary.push(obj);
                                    }
                                } else {
                                    self.setState({ other: '女' })
                                    if (data.sex == "女") {
                                        obj = { y: data.cnt };
                                        ary.push(obj);
                                    }
                                }

                            })
                            // this.state.data.dataSets[0].values = ary;
                            // self.setState(this.state);
                            self.setState({
                                data: {
                                    dataSets: [{
                                        ...this.state.dataSets,
                                        values: ary,
                                        label: 'Company Dashed',
                                    }],
                                }
                            })
                        }}
                    />
                </View>
                <View style={styles.container}>
                    {
                        //     <LineChart
                        //     style={styles.chart}
                        //     data={this.state.data}
                        //     description={{ text: '' }}
                        //     legend={this.state.legend}
                        //     marker={this.state.marker}
                        //     xAxis={this.state.xAxis}
                        //     drawGridBackground={false}
                        //     borderColor={processColor('teal')}
                        //     borderWidth={1}
                        //     drawBorders={true}
                        //     maxVisibleValueCount={50}
                        //     animation={{ durationY: 500 }}

                        //     touchEnabled={true}
                        //     dragEnabled={true}
                        //     scaleEnabled={true}
                        //     scaleXEnabled={true}
                        //     scaleYEnabled={true}
                        //     pinchZoom={true}
                        //     doubleTapToZoomEnabled={true}

                        //     dragDecelerationEnabled={true}
                        //     dragDecelerationFrictionCoef={0.99}

                        //     keepPositionOnRotation={false}
                        //     onSelect={this.handleSelect.bind(this)}
                        // />
                    }
                    <BarChart
                        style={styles.chart}
                        data={this.state.data}
                        xAxis={this.state.xAxis}
                        animation={{ durationY: 1000 }}
                        legend={this.state.legend}
                        gridBackgroundColor={processColor('#ffffff')}
                        drawBarShadow={false}
                        drawValueAboveBar={true}
                        drawHighlightArrow={true}
                        onSelect={this.handleSelect.bind(this)}
                    />
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        width: width,
        height: height * 0.7,
        backgroundColor: '#F5FCFF'
    },
    chart: {
        flex: 1
    },
    bigview: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    content: {
        flexDirection: 'row',
        paddingLeft: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        paddingBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dropdown: {
        width: width * 0.4,
        // alignItems: 'stretch',
        height: 30,
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',
    },
    dropdown2: {
        width: width * 0.6,
        // alignItems: 'stretch',
        height: 30,
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',
    },
    dropdownView: {
        marginVertical: 10
    },
    columnText: {
        marginTop: 10,
        marginBottom: 5
    }
});

export default SignAnalysis;

