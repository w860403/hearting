import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight,
    Modal,
    Image,
    TouchableOpacity,
    AlertIOS,
    Alert
} from 'react-native';
var imgUri = 'http://114.35.74.209/Hearting/Upload/';

import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';

const typing = (<Icon2 name="comments-o" size={30} color="gray" />)
const envelope = (<Icon2 name="envelope" size={30} color="gray" />)
const deleteicon = (<Icon3 name="delete" size={30} color="gray" />)
var { height, width } = Dimensions.get('window');
class FriendDetail extends Component {
    constructor(props) {
        super(props)
        console.log(this.props)
        this.state = {
            modalVisible: this.props.modalVisible ? this.props.modalVisible : false,
            imageName: this.props.imageName ? this.props.imageName : '3-randomTest-random-1.png',
            friendName: this.props.friendName ? this.props.friendName : '陳怡君',
            profile: this.props.profile ? this.props.profile : '我是小語唷我是小語唷我是小語唷我是小語唷我是小語唷',
            partner: this.props.partner ? this.props.partner : '',
            account: this.props.account ? this.props.account : '',
            rowID: this.props.rowID ? this.props.rowID : '',
            loveLetterStatusData: { Partner: null, Status: null },
            loveLetterText: '寄告白信'
        }
    }
    setModalVisible = (visible, imageName) => {
        this.setState({
            modalVisible: visible,
        });
    }
    componentWillReceiveProps(props) {
        this.setState({
            modalVisible: props.modalVisible,
            imageName: props.imageName,
            friendName: props.friendName,
            profile: props.profile,
            partner: props.partner,
            account: props.account,
            rowID: props.rowID,
            loveLetterStatusData: props.loveLetterStatusData
        }, function () {
            if (props.loveLetterStatusData) {
                if (props.loveLetterStatusData.Status == "0" || props.loveLetterStatusData.Partner != this.state.partner) {
                    this.setState({
                        loveLetterText: "寄告白信"
                    })
                    console.log('寄告白信');
                } else if (props.loveLetterStatusData.Status == "1" && props.loveLetterStatusData.Partner == this.state.partner) {
                    this.setState({
                        loveLetterText: "取消告白信"
                    })
                    console.log('取消告白信');

                } else if (props.loveLetterStatusData.Status == "2" && props.loveLetterStatusData.Partner == this.state.partner) {
                    this.setState({
                        loveLetterText: "查看告白信"
                    })
                    console.log('查看告白信');

                } else if (props.loveLetterStatusData.Status == "3" && props.loveLetterStatusData.Partner == this.state.partner) {
                    this.setState({
                        loveLetterText: "已回覆信件"
                    })
                    console.log('已回覆信件');
                }
            }
        });

    }
    render() {
        const imageUrl = 'http://114.35.74.209/Hearting/Upload/'
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={this.state.modalVisible}>
                <TouchableOpacity onPress={() => { this.setModalVisible(false), this.props.onClick(false); }}>
                    <View style={{ width: width, height: height, backgroundColor: 'rgba(52,52,52,0.5)', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableHighlight
                            activeOpacity={1}
                            underlayColor='rgba(52,52,52,0)'>
                            <View style={styles.friendDetailWindowView}>
                                <View style={{ flexDirection: 'row', }}>
                                    <View style={{ flex: 1, }}></View>
                                    <View style={{ flex: 4, height: 10, alignItems: 'center', }}>
                                        <Image source={{ uri: imageUrl + this.state.imageName }} style={styles.image}></Image>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end', }}>
                                        <TouchableHighlight
                                            style={{ paddingRight: 20, paddingTop: 10 }}
                                            activeOpacity={0.8}
                                            underlayColor='rgba(52,52,52,0)'
                                            onPress={() => { this.setModalVisible(false), this.props.onClick(false); }}>
                                            <Text style={{ fontSize: 30 }}>x</Text>
                                        </TouchableHighlight>
                                    </View>
                                </View>
                                <Text style={styles.friendNameText}>{this.state.friendName}</Text>
                                <View style={styles.profileView}>
                                    <Text style={styles.profileText}>{this.state.profile}</Text>
                                </View>
                                <View style={styles.buttonsView}>
                                    <TouchableHighlight
                                        style={styles.chatView}
                                        activeOpacity={0.8}
                                        underlayColor='rgba(52,52,52,0.3)'
                                        onPress={() => { this.props.getChat(this.state.partner,this.state.friendName) ,this.setModalVisible(false),this.props.onClick(false);}}>
                                        <View style={styles.chatView}>{typing}<Text style={styles.buttonText}>開始聊天</Text></View>
                                    </TouchableHighlight>
                                    {
                                        //當已送出交友邀請但是對象不是這個人的時候
                                        this.state.loveLetterStatusData.Status == "1" &&
                                            this.state.loveLetterStatusData.Partner != this.state.partner
                                            || this.state.loveLetterStatusData.Status == "2" &&
                                            this.state.loveLetterStatusData.Partner != this.state.partner
                                            || this.state.loveLetterStatusData.Status == "3" &&
                                            this.state.loveLetterStatusData.Partner != this.state.partner
                                            ?
                                            <TouchableHighlight
                                                style={styles.loveLetterView}
                                                activeOpacity={0.8}
                                                underlayColor='rgba(52,52,52,0.3)'
                                                onPress={() => AlertIOS.alert('已寄出告白信', '已寄出告白信，不可重複寄送！')}>
                                                <View style={styles.loveLetterView}>{envelope}
                                                    <Text style={styles.buttonText}>
                                                        {this.state.loveLetterText}
                                                    </Text>
                                                </View>
                                            </TouchableHighlight>
                                            : null}
                                    {
                                        //未送出告白信
                                        this.state.loveLetterStatusData.Status == "0" ?
                                            <TouchableHighlight
                                                style={styles.loveLetterView}
                                                activeOpacity={0.8}
                                                underlayColor='rgba(52,52,52,0.3)'
                                                onPress={() => {
                                                    this.setModalVisible(false)
                                                    this.props.onClick(false);
                                                    this.props.goToLoveLetterInput(this.state.account, this.state.partner)
                                                }}>
                                                <View style={styles.loveLetterView}>{envelope}
                                                    <Text style={styles.buttonText}>
                                                        {this.state.loveLetterText}
                                                    </Text>
                                                </View>
                                            </TouchableHighlight>
                                            : null}
                                    {
                                        //已送出告白信，對方尚未送出
                                        this.state.loveLetterStatusData.Status == "1" && this.state.loveLetterStatusData.Partner == this.state.partner ?
                                            <TouchableHighlight
                                                style={styles.loveLetterView}
                                                activeOpacity={0.8}
                                                underlayColor='rgba(52,52,52,0.3)'
                                                onPress={() => {
                                                    this.props.cancelLoveLetter(this.state.account)
                                                }}>
                                                <View style={styles.loveLetterView}>{envelope}
                                                    <Text style={styles.buttonText}>
                                                        {this.state.loveLetterText}
                                                    </Text>
                                                </View>
                                            </TouchableHighlight>
                                            : null}
                                    {
                                        //已送出告白信，對方也送出了，我未讀
                                        this.state.loveLetterStatusData.Status == "2" && props.loveLetterStatusData.Partner == this.state.partner ?
                                            <TouchableHighlight
                                                style={styles.loveLetterView}
                                                activeOpacity={0.8}
                                                underlayColor='rgba(52,52,52,0.3)'
                                                onPress={() => {
                                                    this.setModalVisible(false)
                                                    this.props.onClick(false);
                                                    this.props.goToLoveLetterDisplay(this.state.account)
                                                }}>
                                                <View style={styles.loveLetterView}>
                                                    <View style={styles.evelopeView}>
                                                        {envelope}
                                                        <View>
                                                            <Text style={styles.newLetterText}>new</Text>
                                                        </View>
                                                    </View>
                                                    <Text style={styles.buttonText}>
                                                        {this.state.loveLetterText}
                                                    </Text>
                                                </View>
                                            </TouchableHighlight>
                                            : null}
                                    {
                                        //已送出告白信，對方也送出了，我已讀
                                        this.state.loveLetterStatusData.Status == "3" && this.state.loveLetterStatusData.Partner == this.state.partner ?
                                            <TouchableHighlight
                                                style={styles.loveLetterView}
                                                activeOpacity={0.8}
                                                underlayColor='rgba(52,52,52,0.3)'
                                                onPress={() => {
                                                    this.setModalVisible(false)
                                                    this.props.onClick(false);
                                                    this.props.goToLoveLetterDisplay(this.state.account)
                                                }}>
                                                <View style={styles.loveLetterView}>
                                                    {envelope}
                                                    <Text style={styles.buttonText}>
                                                        {this.state.loveLetterText}
                                                    </Text>
                                                </View>
                                            </TouchableHighlight>
                                            : null}
                                    <TouchableHighlight
                                        style={styles.deleteView}
                                        activeOpacity={0.8}
                                        underlayColor='rgba(52,52,52,0.3)'
                                        onPress={() => AlertIOS.alert('確認刪除好友', '刪除好友後將無法反悔，確定要刪除嗎？', [{
                                            text: '確認', onPress: () => {
                                                this.props.pressRow(this.state.rowID)
                                                this.props.deleteFriend(this.state.account, this.state.partner)
                                                this.setModalVisible(false)
                                                this.props.onClick(false);
                                            }
                                        },
                                        { text: '取消', onPress: () => console.log('取消') }])}>
                                        <View style={styles.deleteView}>{deleteicon}<Text style={styles.buttonText}>刪除好友</Text></View>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>
                </TouchableOpacity>
            </Modal>

        );
    }
    static propTypes = {
        onClick: PropTypes.func,
    };
    static defaultProps = {
        onClick: () => {
        }
    };
}

const styles = StyleSheet.create({
    friendDetailWindowView: {
        width: width * 0.8,
        height: height * 0.4,
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,1)',
        alignItems: 'center',
    },
    image: {
        borderRadius: height * 0.075,
        width: height * 0.148,
        height: height * 0.148,
        marginTop: height * -0.075,
    },
    friendNameText: {
        fontSize: 22,
        marginTop: 10
    },
    profileText: {
        fontSize: 15,
        color: 'gray'
    },
    profileView: {
        margin: 20,
        height: 30
    },
    buttonsView: {
        flexDirection: 'row',
        marginTop: 30,
        borderTopWidth: 1,
        height: height * 0.13,
        backgroundColor: 'rgba(255,255,255,1)',
        width: width * 0.8,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderColor: 'rgba(52,52,52, 0.5)'
    },
    chatView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.13,
        borderBottomLeftRadius: 10,
    },
    loveLetterView: {
        flex: 1,
        height: height * 0.13,
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center',
    },
    newLetterView: {
        backgroundColor: 'red',
        position: 'absolute',
        marginLeft: 20
    },
    newLetterText: {
        color: 'white'
    },
    deleteView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.13,
        borderBottomRightRadius: 10,
    },
    buttonText: {
        color: 'gray'
    }
});

export default FriendDetail;
