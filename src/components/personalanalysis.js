import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
    TouchableHighlight,
    Dimensions,
    Image,
    TextInput,
    AlertIOS,
    AsyncStorage,
    ActivityIndicator,
    Button,
    TouchableOpacity,
    processColor
} from 'react-native';
import reactAddonsUpdate from 'react-addons-update';
import { RadarChart } from 'react-native-charts-wrapper';
import { Radar } from 'react-native-pathjs-charts'
var { height, width } = Dimensions.get('window');

class personal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            legend: {
                enabled: true,
                textSize: 14,
                form: 'CIRCLE',
                wordWrapEnabled: true
            },
            num: 10
        };
    }
    componentWillReceiveProps(nextProps) {
        const { analysis: previous_analysis } = this.props;
        const { analysis } = nextProps;
        if (previous_analysis != analysis) {
            this.setState({
                animating: false, loaded: true,
            })
            this.setState(
                reactAddonsUpdate(this.state, {
                    data: {
                        $set: {
                            dataSets: [{
                                values: [{ value: analysis.CharmList[0].Count }, { value: analysis.CharmList[1].Count }, { value: analysis.CharmList[2].Count }, { value: analysis.CharmList[3].Count }, { value: analysis.CharmList[4].Count }],
                                label: '魅力五角圖',
                                config: {
                                    color: processColor('#FF8C9D'),
                                    drawFilled: true,
                                    fillColor: processColor('#FF8C9D'),
                                    fillAlpha: 100,
                                    lineWidth: 2
                                }
                            }],
                        }
                    },
                    xAxis: {
                        $set: {
                            valueFormatter: [analysis.CharmList[0].Style, analysis.CharmList[1].Style, analysis.CharmList[2].Style, analysis.CharmList[3].Style, analysis.CharmList[4].Style],

                        },
                    },
                })
            );
        }
    }
    render() {
        return (
            <View style={{ flex: 1 }}>

                <View style={styles.container}>
                    <RadarChart
                        style={styles.chart}
                        data={this.state.data}
                        xAxis={this.state.xAxis}
                        yAxis={this.state.yAxis}
                        description={{ text: '342' }}
                        animation={{ durationY: 500 }}
                        legend={this.state.legend}
                        skipWebLineCount={1}
                        axisMinimum={0}
                    // onSelect={this.handleSelect.bind(this)}
                    />
                </View>
                <View style={{ flex: 1 }}>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

        // backgroundColor: '#F5FCFF'
    },
    chart: {
        flex: 1,
        marginTop: 20
    }
});
export default personal;