import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Modal,
    Image,
} from 'react-native';
var imgUri = 'http://114.35.74.209/Hearting/Upload/';

var { height, width } = Dimensions.get('window');
class ImagePreview extends Component {
    constructor(props) {
        super(props)
        console.log(this.props)
        this.state = {
            modalVisible: false,
            imageName: '',
        }
    }
    setModalVisible = (visible, imageName) => {
        this.setState({
            modalVisible: visible,
            imageName: imageName,
        });
    }
    render() {
        if (this.state.imageName) {
            return (
                <Modal
                    animationType={"fade"}
                    transparent={true}
                    visible={this.state.modalVisible}>
                    <TouchableOpacity onPress={() => { this.setModalVisible(false), this.props.onClick(false);}} style={{flex:1,backgroundColor:'rgba(52,52,52,0.5)',alignItems: 'center',justifyContent: 'center'}} >
                        <View style={styles.imageView}>
                            <Image source={{ uri: imgUri + this.state.imageName }} style={styles.image} />
                        </View>
                    </TouchableOpacity>

                </Modal>
            );
        }
        else {
            return null;
        }
    }
    static propTypes = {
        onClick: PropTypes.func,
    };
    static defaultProps = {
        onClick: () => {
        }
    };
}

const styles = StyleSheet.create({
    imageView: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width,
        height: height * 0.65,
        flex: 0,
        // borderColor: 'rgba(255,255,255,0.5)',
        // borderWidth: 5,
    },
    image: {
        resizeMode: Image.resizeMode.contain,
        // width: width * 0.87,
        // height: height * 0.65,
        width: width ,
        height: height * 0.65,
    },
});

export default ImagePreview;
