import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ListView,
    Modal
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import SinglePicker from 'mkp-react-native-picker';
import ImagePreview from './imagepreview';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
var imgUri = 'http://114.35.74.209/Hearting/Upload/';

class Photo extends Component {
    constructor() {
        super();
        // var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            selectedOption: '隨機',
            slideIndex: 0,
            dataSource: {},
            modalVisible: false,
            imageName: '',
        }
    }
    setModalVisible = (visible, imageName) => {
        console.log(imageName);
        this.setState({ modalVisible: visible, imageName: imageName });
    }
    closeModalVisible = (visible, imageName) => {
        this.setState({
            modalVisible: visible,
            imageName: imageName,
            imgWidth: 0,
            imgHeight: 0
        });
    }
    _renderRow = (rowData, sectionID, rowID) => {
        const down = (<Icon name="chevron-down" size={20} color="#383838" />)
        const heart = (<Icon name="heart" size={20} color="white" />)
        return (
            <View style={styles.listView}>
                <TouchableOpacity onPress={() => { this.setModalVisible(true, rowData.ImgName) }}>
                    <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.ImgName }} style={styles.square}>
                        <View style={styles.heart}>
                            <Text style={{ color: 'white', fontSize: 19 }}>{heart} {rowData.Timer.toString()}</Text>
                        </View>
                    </Image>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        var list = [
            { index: 0, title: "隨機", data: [{ img: '3-randomTest-random-1.png', people: '134' }, { img: '1-randomTest-random-1.png', people: '345' }, { img: '6-randomTest-random-1.png', people: '134' }, { img: '5-randomTest-random-1.png', people: '134' }, { img: '4-randomTest-random-1.png', people: '134' }] },
            { index: 1, title: "背影", data: [{ img: '4-randomTest-random-1.png', people: '345' }, { img: '6-randomTest-random-1.png', people: '134' }] },
            { index: 2, title: "零死角", data: [{ img: '5-randomTest-random-1.png', people: '2' }, { img: '7-randomTest-random-1.png', people: '134' }] }
        ];
        var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var self = this;
        var dataSource = ds.cloneWithRows(this.state.photoList)

        return (
            <View style={styles.bigview}>
                {
                    // <Modal
                    //     animationType={"fade"}
                    //     transparent={true}
                    //     visible={this.state.modalVisible}
                    // >
                    //     <View style={{ width:width,height:height,backgroundColor:'rgba(52,52,52,0.5)',justifyContent:'center' }}>
                    //         <View>
                    //             <TouchableOpacity
                    //                 onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
                    //                 <View style={styles.imageZoomView}>
                    //                     <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + this.state.imageZoom }} style={{ height: this.state.imgHeight, width: this.state.imgWidth }} />
                    //                 </View>
                    //             </TouchableOpacity>

                    //         </View>
                    //     </View>
                    // </Modal>
                }
                {
                    // <TouchableOpacity
                    //     onPress={() => { this.closeModalVisible(false) }}>
                    //     <ImagePreview modalVisible={this.state.modalVisible} imageName={this.state.imageName} />
                    // </TouchableOpacity>
                }
                <ImagePreview
                    modalVisible={this.state.modalVisible}
                    imageName={this.state.imageName}
                    onClick={(option) => {
                        this.setState({ modalVisible: option })
                    }}
                />
                {
                    //list
                }
                <Carousel
                    ref={(carousel) => { this._carousel = carousel; }}
                    sliderWidth={width}
                    itemWidth={width}
                    directionalLockEnabled={true}
                    centerContent={true}
                    // horizontal={true}
                    currentIndex={this.state.currentIndex}>
                    <View style={styles.slide1} >
                        <View style={styles.contentList}>
                            <ListView
                                dataSource={dataSource}
                                renderRow={self._renderRow}
                                contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap', width: width - width * 0.16 }}
                            />
                        </View>
                    </View>

                </Carousel>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    toparea: {
        marginTop: height * 0.04,
        marginBottom: height * 0.01,
        width: width * 0.85,
        height: height * 0.07
    },
    bigview: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',

    },
    square: {
        height: width * 0.25,
        width: width * 0.25,
        borderWidth: 3,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
    slide1: {
        // justifyContent: 'center',
        paddingLeft: width * 0.08,
        paddingRight: width * 0.08,
        width: width,
        marginTop:20,
    },
    contentList: {
    },
    listView: {
        paddingTop: 13,
        marginLeft: 5,
        marginRight: 5,
        paddingBottom: 13,
        width: width * 0.25,
        justifyContent: 'center',
        height: width * 0.25,
        marginBottom: 10
    },
    heart: {
        backgroundColor: 'rgba(52, 52, 52, 0.6)',
        width: width * 0.25,
        height: width * 0.25 * 0.35,
        marginTop: width * 0.25 - width * 0.25 * 0.35,
        alignItems: 'center',
        // justifyContent: 'center'
    },
});

export default Photo;

