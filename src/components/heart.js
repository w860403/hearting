import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Home from '../containers/home';
import Personal from '../containers/personalanalysis';
import Quiz from '../containers/quizlist';
import Friend from '../containers/friends';
import Member from '../containers/member';
import BottomBar from '../containers/bottombar';
import NoLogin from '../containers/nologin';

class Heart extends Component {
    constructor(props) {
        super(props);
    }

    _show = () => {
        console.log(this.props.account)
        switch (this.state.showComponent) {
            case 'home':
                return <Home />
            case 'personal':
                if (this.props.account !== null) {
                    return <Personal />
                }
                else {
                    return <NoLogin />
                }
            case 'quiz':
                if (this.props.account !== null) {
                    return <Quiz />
                }
                else {
                    return <NoLogin />
                }
            case 'friend':
                if (this.props.account !== null) {
                    return <Friend />
                }
                else {
                    return <NoLogin />
                }
            case 'member':
                if (this.props.account !== null) {
                    return <Member />
                }
                else {
                    return <NoLogin />
                }
            default:
                return <Home />
        }
    }
    render() {
        const showView = this._show();
        return (
            <View style={{ width: width, height: height }}>
                {showView}
                <BottomBar />
            </View>
        )

    }
}
const styles = StyleSheet.create({

});

export default Heart;

