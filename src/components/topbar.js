import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
var { height, width } = Dimensions.get('window');
import MindWaveMobile from 'react-native-mindwave-mobile';
const mwm = new MindWaveMobile()
class topbar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            backButtonVisible: this.props.backButtonVisible != null ? this.props.backButtonVisible : true,
            title: this.props.title ? this.props.title : '',
            titlecolor: this.props.titlecolor ? this.props.titlecolor : 0,
            lefttext: this.props.lefttext ? this.props.lefttext : 'Back',
            backtextcolor: this.props.backtextcolor ? this.props.backtextcolor : 0,
            cycle: this.props.cycle ? this.props.cycle : false,
            addfriend: this.props.addfriend ? this.props.addfriend : false,
            function: this.props.function ? this.props.function : '',
            popNumber: this.props.popNumber ? this.props.popNumber : 1,
            closeMindWaveListener: this.props.closeMindWaveListener ? this.props.closeMindWaveListener : false
        }
    }
    componentWillReceiveProps(nextProps) {
        const { title: previous_title } = this.props;
        const { title } = nextProps;
        if (previous_title != title) {
            this.setState({
                title: title ? title : ''
            });
        }
        const { backButtonVisible: previous_backButtonVisible } = this.props;
        const { backButtonVisible } = nextProps;
        if (previous_backButtonVisible != backButtonVisible) {
            this.setState({
                backButtonVisible: nextProps.backButtonVisible ? nextProps.backButtonVisible : true
            });
        }
        const { closeMindWaveListener: previous_closeMindWaveListener } = this.props;
        const { closeMindWaveListener } = nextProps;
        if (previous_closeMindWaveListener != closeMindWaveListener) {
            this.setState({
                closeMindWaveListener: nextProps.closeMindWaveListener ? nextProps.closeMindWaveListener : false
            });
        }
    }
    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color={this.props.backtextcolor == 1 ? 'white' : 'black'} />)
        const cycle = (<Icon name="cycle" size={25} color="black" />)
        const addfriend = (<Icon name="add-user" size={25} color="black" />)
        return (
            <View>
                <View style={styles.topBar}>
                    <View style={styles.topBarLeftView}>
                        {this.state.backButtonVisible ?
                            <TouchableHighlight
                                activeOpacity={0.8}
                                underlayColor='rgba(0,0,0,0)'
                                onPress={() => this.props.onBackButtonClick(this.state.closeMindWaveListener)}>
                                <Text style={this.props.backtextcolor == 1 ? styles.backTextWhite : styles.backText}>{left}{this.state.lefttext}</Text>
                            </TouchableHighlight>
                            :
                            null}
                    </View>
                    <View style={styles.topBarMiddleView}>
                        <Text style={this.props.titlecolor == 1 ? styles.titleTextWhite : styles.titleText}>{this.state.title}</Text>
                    </View>
                    <View style={styles.topBarRightView}>
                        {this.state.addfriend ? <TouchableHighlight
                            activeOpacity={0.8}
                            underlayColor='rgba(0,0,0,0)'
                            onPress={() => this.props.onAddFriendButtonClick()}>
                            {addfriend}
                        </TouchableHighlight> : null
                        }
                        {this.state.cycle ? <TouchableHighlight
                            activeOpacity={0.8}
                            underlayColor='rgba(0,0,0,0)'
                            onPress={() => this.render}>
                            {cycle}
                        </TouchableHighlight> : null
                        }
                    </View>
                    <View style={styles.titleView}>

                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    topBar: {
        width: width,
        height: height * 0.04,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: height * 0.05,
    },
    backButton: {
    },
    topBarLeftView: {
        marginLeft: width * 0.03,
        flex: 1,
        // backgroundColor:'red'
    },
    topBarMiddleView: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'blue'        
    },
    topBarRightView: {
        backgroundColor: 'rgba(0,0,0,0)',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginRight: 10,
        flex: 1,
    },
    backText: {
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 20,
    },
    backTextWhite: {
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 20,
        color: 'white'
    },
    titleText: {
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 20,
    },
    titleTextWhite: {
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 20,
        color: 'white'
    }
});

export default topbar;
