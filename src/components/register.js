import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  PixelRatio,
  AlertIOS,
  Keyboard
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import SinglePicker from 'mkp-react-native-picker';
import ImagePicker from './imagepicker';
import ImagePicker2 from 'react-native-image-picker';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
const address = [
  { key: 0, value: "台北市" },
  { key: 1, value: "新北市" },
  { key: 2, value: "桃園市" },
  { key: 3, value: "新竹市" },
  { key: 4, value: "新竹縣" },
  { key: 5, value: "苗栗縣" },
  { key: 6, value: "台中市" },
  { key: 7, value: "彰化縣" },
  { key: 8, value: "南投縣" },
  { key: 9, value: "雲林縣" },
  { key: 10, value: "嘉義市" },
  { key: 11, value: "嘉義縣" },
  { key: 12, value: "嘉義縣" },
  { key: 13, value: "台南市" },
  { key: 14, value: "高雄市" },
  { key: 15, value: "屏東縣" },
  { key: 16, value: "宜蘭縣" },
  { key: 17, value: "基隆市" },
  { key: 18, value: "花蓮縣" },
  { key: 19, value: "台東縣" },
  { key: 20, value: "澎湖縣" },
  { key: 21, value: "金門縣" },
  { key: 22, value: "連江縣" }
];
const blood = [
  { key: 0, value: "A" },
  { key: 1, value: "B" },
  { key: 2, value: "O" },
  { key: 3, value: "AB" }
];
const M_Hairstyle = [
  { key: 0, value: "==請選擇==", image: '', folder: '' },
  { key: 1, value: "微捲暖男造型", image: '1.jpg', folder: '男生髮型/' },
  { key: 2, value: "二分區式造型", image: '2.jpg', folder: '男生髮型/' },
  { key: 3, value: "後吹露額造型", image: '3.png', folder: '男生髮型/' },
  { key: 4, value: "分線油頭造型", image: '4.jpg', folder: '男生髮型/' },
  { key: 5, value: "平瀏海造型", image: '5.jpg', folder: '男生髮型/' }
];
const W_Hairstyle = [
  { key: 0, value: "==請選擇==", image: '', folder: '' },
  { key: 1, value: "氣質長直髮", image: '1.jpg', folder: '女生髮型/' },
  { key: 2, value: "知性中長髮", image: '2.jpg', folder: '女生髮型/' },
  { key: 3, value: "讓漫長捲髮", image: '3.jpg', folder: '女生髮型/' },
  { key: 4, value: "甜美及肩髮", image: '4.jpg', folder: '女生髮型/' },
  { key: 5, value: "輕熟女短髮", image: '5.jpg', folder: '女生髮型/' }
];
const isLove = [
  { key: 0, value: "單身" },
  { key: 1, value: "一言難盡" },
  { key: 2, value: "穩定交往" },
  { key: 3, value: "已婚" }
];
const sex = [
  { key: 0, value: "男", },
  { key: 1, value: "女", }
];
const aptitude = [
  { key: 0, value: "男" },
  { key: 1, value: "女" },
];
const M_myStyle = [
  { key: 0, value: "陽光帥氣型" },
  { key: 1, value: "文藝青年型" },
  { key: 2, value: "沈穩憨厚型" },
  { key: 3, value: "成熟大叔型" },
  { key: 4, value: "書生氣息型" },
  { key: 5, value: "韓系型男型" },
  { key: 6, value: "質感宅男型" },
  { key: 7, value: "肌肉型男型" }
];
const W_myStyle = [
  { key: 0, value: "甜美可愛型" },
  { key: 1, value: "火辣性感型" },
  { key: 2, value: "優雅氣質型" },
  { key: 3, value: "健美陽光型" },
  { key: 4, value: "個性男孩型" },
  { key: 5, value: "棉花糖風型" },
  { key: 6, value: "溫暖淳樸型" },
  { key: 7, value: "文藝少女型" }
];
const W_Headhair = [
  { key: 0, value: "==請選擇==", image: '', folder: '' },
  { key: 1, value: "咩咩頭", image: '1.jpg', folder: '女生瀏海/' },
  { key: 2, value: "斜劉海", image: '2.jpg', folder: '女生瀏海/' },
  { key: 3, value: "中分", image: '3.jpg', folder: '女生瀏海/' },
  { key: 4, value: "旁分", image: '4.jpg', folder: '女生瀏海/' },
  { key: 5, value: "空氣瀏海", image: '5.jpg', folder: '女生瀏海/' },
  { key: 6, value: "allback", image: '6.png', folder: '女生瀏海/' },
  { key: 7, value: "無", image: '', folder: '' }
];
const M_Stature = [
  { value: '圓胖西洋梨型', key: 0 },
  { value: '精瘦小黃瓜型', key: 1 },
  { value: '肌肉磚塊型', key: 2 },
  { value: '扁寬倒三角形', key: 3 },
  { value: '乾扁豌豆型', key: 4 },
];
const W_Stature = [
  { value: '曲線沙漏型', key: 0 },
  { value: '纖瘦骨感型', key: 1 },
  { value: '下身肉肉型', key: 2 },
  { value: '運動線條型', key: 3 },
  { value: '腰寬蘋果型', key: 4 },
];
const W_Height = [
  { value: '150以下', key: 0 },
  { value: '150~155', key: 1 },
  { value: '156~160', key: 2 },
  { value: '161~165', key: 3 },
  { value: '166~170', key: 4 },
  { value: '170以上', key: 5 },
];
const M_Height = [
  { value: '165以下', key: 0 },
  { value: '165~170', key: 1 },
  { value: '171~175', key: 2 },
  { value: '176~180', key: 3 },
  { value: '181~185', key: 4 },
  { value: '185以上', key: 5 },
];
const FirstSee = [
  { value: '臉', key: 0 },
  { value: '胸', key: 1 },
  { value: '腿', key: 2 },
  { value: '其他', key: 3 },
];
const LikeAge = [
  { value: '小五歲以上', key: 0 },
  { value: '小2~5歲', key: 1 },
  { value: '差兩歲左右', key: 2 },
  { value: '大2~5歲', key: 3 },
  { value: '大5歲以上', key: 4 },
];
var FaceType = [
  { key: 0, value: "==請選擇==", image: '', folder: '' },
  { value: '國字臉', key: 1, image: '1.png', folder: '臉型/' },
  { value: '圓臉', key: 2, image: '2.png', folder: '臉型/' },
  { value: '瓜子臉', key: 3, image: '3.png', folder: '臉型/' },
  { value: '鵝蛋臉', key: 4, image: '4.png', folder: '臉型/' },
  { value: '長臉', key: 5, image: '5.png', folder: '臉型/' },
];
var checkNum = 0;
class Register extends Component {

  constructor(props) {
    super(props);
    let now = new Date();
    this.state = {
      toDay: now,
      selectedOption: '帳號密碼',
      slideIndex: 0,
      Account: '',
      Password: '',
      checkPassword: '',
      Name: '',
      Age: 0,
      Birthday: now,
      Sex: '',
      Aptitude: '',
      Email: '',
      Constellation: '',
      Blood: '',
      Height: -1,
      Weight: -1,
      Address: '',
      Image: '',
      isLove: '',
      Interest: '',
      avatarSource: null,
      basicVisible: false,
      favoriteVisible: false,
      accountVisible: true,
      d1: '0',
      d2: '0',
      d3: '0',
      d4: '0',
      d5: '0',
      d6: '0',
      d7: '0',
      d8: '0',
      d9: '0',
      d10: '0',
      d0: '0',
      isD1: false,
      isD2: false,
      isD3: false,
      isD4: false,
      isD5: false,
      isD6: false,
      isD7: false,
      isD8: false,
      isD9: false,
      isD10: false,
      isD0: false,

    }
  }
  componentWillMount() {
    let now = new Date();
    this._Constellation(now)
  }
  // componentDidUpdate(prevProps, prevState) {
  //   if (this.state.selectedOption == '基本資料' && this.state.slideIndex !== 1) {
  //     this._carousel.snapToItem(1)
  //     console.log('選擇 基本資料')
  //   } else if (this.state.selectedOption == '個人喜好' && this.state.slideIndex !== 2) {
  //     this._carousel.snapToItem(2)
  //     console.log('選擇 個人喜好')
  //   } else if (this.state.selectedOption == '帳號密碼' && this.state.slideIndex !== 0) {
  //     this._carousel.snapToItem(0)
  //     console.log('選擇 帳號密碼')
  //   }
  // }
  //判斷星座
  _Constellation = (date) => {
    var birthday = new Date(date);
    var year = birthday.getFullYear();
    var mouth = birthday.getMonth();
    var day = birthday.getDay();
    this.setState({ Age: 2017 - year });
    if (mouth === 0) {
      if (day < 21) {
        this.setState({ Constellation: "摩羯座" });
      }
      else {
        this.setState({ Constellation: "水瓶座" });
      }
    } else if (mouth === 1) {
      if (day < 21) {
        this.setState({ Constellation: "水瓶座" });
      }
      else {
        this.setState({ Constellation: "雙魚座" });
      }

    } else if (mouth === 2) {
      if (day < 21) {
        this.setState({ Constellation: "雙魚座" });
      }
      else {
        this.setState({ Constellation: "牡羊座" });
      }

    } else if (mouth === 3) {
      if (day < 21) {
        this.setState({ Constellation: "牡羊座" });
      }
      else {
        this.setState({ Constellation: "金牛座" });
      }

    } else if (mouth === 4) {
      if (day < 21) {
        this.setState({ Constellation: "金牛座" });
      }
      else {
        this.setState({ Constellation: "雙子座" });
      }

    } else if (mouth === 5) {
      if (day < 21) {
        this.setState({ Constellation: "雙子座" });
      }
      else {
        this.setState({ Constellation: "巨蟹座" });
      }
    } else if (mouth === 6) {
      if (day < 21) {
        this.setState({ Constellation: "巨蟹座" });
      }
      else {
        this.setState({ Constellation: "獅子座" });
      }
    } else if (mouth === 7) {
      if (day < 21) {
        this.setState({ Constellation: "獅子座" });
      }
      else {
        this.setState({ Constellation: "處女座" });
      }
    } else if (mouth === 8) {
      if (day < 21) {
        this.setState({ Constellation: "處女座" });
      }
      else {
        this.setState({ Constellation: "天枰座" });
      }
    } else if (mouth === 9) {
      if (day < 21) {
        this.setState({ Constellation: "天枰座" });
      }
      else {
        this.setState({ Constellation: "天蠍座" });
      }
    } else if (mouth === 10) {
      if (day < 21) {
        this.setState({ Constellation: "天蠍座" });
      }
      else {
        this.setState({ Constellation: "射手座" });
      }
    } else if (mouth === 11) {
      if (day < 21) {
        this.setState({ Constellation: "射手座" });
      }
      else {
        this.setState({ Constellation: "摩羯座" });
      }
    }
  }
  _interest = () => {
    return (

      <View style={{ flexDirection: 'row', flexWrap: 'wrap', height: 50 }}>
        <CheckBox
          label="運動"
          size={30}
          checked={this.state.isD0}
          onPress={() => this._checkButtonInterest(0, this.state.isD0)}
        />
        <CheckBox
          label="音樂"
          size={30}
          checked={this.state.isD1}
          onPress={() => this._checkButtonInterest(1, this.state.isD1)}
        />
        <CheckBox
          label="藝術"
          size={30}
          checked={this.state.isD2}
          onPress={() => this._checkButtonInterest(2, this.state.isD2)}
        /><CheckBox
          label="旅遊"
          size={30}
          checked={this.state.isD3}
          onPress={() => this._checkButtonInterest(3, this.state.isD3)}
        /><CheckBox
          label="逛街購物"
          size={30}
          checked={this.state.isD4}
          onPress={() => this._checkButtonInterest(4, this.state.isD4)}
        /><CheckBox
          label="閱讀"
          size={30}
          checked={this.state.isD5}
          onPress={() => this._checkButtonInterest(5, this.state.isD5)}
        /><CheckBox
          label="影視"
          size={30}
          checked={this.state.isD6}
          onPress={() => this._checkButtonInterest(6, this.state.isD6)}
        /><CheckBox
          label="聊天"
          size={30}
          checked={this.state.isD7}
          onPress={() => this._checkButtonInterest(7, this.state.isD7)}
        /><CheckBox
          label="線上遊戲"
          size={30}
          checked={this.state.isD8}
          onPress={() => this._checkButtonInterest(8, this.state.isD8)}
        /><CheckBox
          label="上網"
          size={30}
          checked={this.state.isD9}
          onPress={() => this._checkButtonInterest(9, this.state.isD9)}
        /><CheckBox
          label="美食"
          size={30}
          checked={this.state.isD10}
          onPress={() => this._checkButtonInterest(10, this.state.isD10)}
        />
      </View>
    );
  }
  _checkButtonInterest = (checkbox, bool) => {
    if (bool) {
      checkNum--;
      if (checkbox === 0) {
        this.setState({ isD0: false, d0: '0' });
      } else if (checkbox === 1) {
        this.setState({ isD1: false, d1: '0' });
      } else if (checkbox === 2) {
        this.setState({ isD2: false, d2: '0' });
      } else if (checkbox === 3) {
        this.setState({ isD3: false, d3: '0' });
      } else if (checkbox === 4) {
        this.setState({ isD4: false, d4: '0' });
      } else if (checkbox === 5) {
        this.setState({ isD5: false, d5: '0' });
      } else if (checkbox === 6) {
        this.setState({ isD6: false, d6: '0' });
      } else if (checkbox === 7) {
        this.setState({ isD7: false, d7: '0' });
      } else if (checkbox === 8) {
        this.setState({ isD8: false, d8: '0' });
      } else if (checkbox === 9) {
        this.setState({ isD9: false, d9: '0' });
      } else if (checkbox === 10) {
        this.setState({ isD10: false, d10: '0' });
      }
    }
    else {
      checkNum++;
      if (checkNum > 5) {
        checkNum = 5;
        alert('最多只能選五個興趣！！');
      } else {
        if (checkbox === 0) {
          this.setState({ isD0: true, d0: '1' });
        } else if (checkbox === 1) {
          this.setState({ isD1: true, d1: '1' });
        } else if (checkbox === 2) {
          this.setState({ isD2: true, d2: '1' });
        } else if (checkbox === 3) {
          this.setState({ isD3: true, d3: '1' });
        } else if (checkbox === 4) {
          this.setState({ isD4: true, d4: '1' });
        } else if (checkbox === 5) {
          this.setState({ isD5: true, d5: '1' });
        } else if (checkbox === 6) {
          this.setState({ isD6: true, d6: '1' });
        } else if (checkbox === 7) {
          this.setState({ isD7: true, d7: '1' });
        } else if (checkbox === 8) {
          this.setState({ isD8: true, d8: '1' });
        } else if (checkbox === 9) {
          this.setState({ isD9: true, d9: '1' });
        } else if (checkbox === 10) {
          this.setState({ isD10: true, d10: '1' });
        }
      }
    }

  }
  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker2.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        this.setState({
          avatarSource: source,
          imageData: response.data,
          image: response
        });
      }
      this.setState({ Image: this.state.Account + '-Member.png' })
      this.props.oploadImage(this.state.Account, this.state.imageData)
      this.props.oploadTestImage(this.state.Account, 'randomTest', 'random', this.state.imageData)
    });
  }
  register = () => {
    var self = this;
    var str = this.state.d0 + this.state.d1 + this.state.d2 + this.state.d3 + this.state.d4 + this.state.d5 + this.state.d6 + this.state.d7 + this.state.d8 + this.state.d9 + this.state.d10;
    var data = {
      "master": {
        Account: self.state.Account,
        Password: self.state.Password,
        Name: self.state.Name,
        Email: self.state.Email,
        Age: this.state.Age,
        Birthday: self.state.Birthday,
        Sex: self.state.Sex,
        Aptitude: self.state.Aptitude,
        Constellation: self.state.Constellation,
        Blood: self.state.Blood,
        Height: self.state.Height,
        Weight: self.state.Weight,
        Address: self.state.Address,
        isLove: self.state.isLove,
        Image: self.state.Image,
        Interest: str
      },
      detail: {
        Account: self.state.Account,
        M_myStyle: this.state.M_myStyle_key,
        W_myStyle: this.state.W_myStyle_key,
        M_Hairstyle: this.state.M_Hairstyle_key == -1 ? null : this.state.M_Hairstyle_key,
        W_Hairstyle: this.state.W_Hairstyle_key == -1 ? null : this.state.W_Hairstyle_key,
        W_Headhair: this.state.W_Headhair_key,
        W_Stature: this.state.W_Stature_key,
        M_Stature: this.state.M_Stature_key,
        W_Height: this.state.W_Height_key,
        M_Height: this.state.M_Height_key,
        FirstSee: this.state.FirstSee_key,
        M_Style: this.state.M_Style_key,
        W_Style: this.state.W_Style_key,
        LikeAge: this.state.LikeAge_key,
        FaceType: this.state.FaceType_key == -1 ? null : this.state.FaceType_key,
        Profile: this.state.Profile
      }
    }
    this.props.registerMember(data);

  }
  //檢查密碼
  _checkPassword(password) {
    if (password !== this.state.Password) {
      return (
        AlertIOS.alert(
          "警告",
          "密碼不一致",
          [
            { text: 'OK', onPress: () => { this.setState({ Password: '', checkPassword: '' }) } },
          ]
        )
      );
    }
  }
  _accountShow = () => {
    if (this.state.accountVisible) {
      return (
        <View style={{ padding: width * 0.12, width: width, height: 500, }}>
          <View style={styles.content}>
            <Text style={styles.columnText}>帳號</Text>
            <TextInput
              placeholder="帳號..."
              placeholderTextColor="gray"
              style={styles.textInput}
              onChangeText={(Account) => this.setState({ Account })}
              autoCapitalize="none"
              onEndEditing={() => this.props.checkAccount(this.state.Account)}
              value={this.state.Account}
            />
          </View>
          {
            this.state.check_account_status ?
              [
                AlertIOS.alert(
                  "警告",
                  "此帳號已被使用",
                  [
                    { text: 'OK', onPress: () => { this.setState({ Account: '', check_account_status: false }), this.props.checkAccount(this.state.Account) } },
                  ]
                )
              ]
              :
              []
          }
          <View style={styles.space}></View>
          <View style={styles.content}>
            <Text style={styles.columnText}>密碼</Text>
            <TextInput
              placeholder="密碼..."
              placeholderTextColor="gray"
              style={styles.textInput}
              onChangeText={(Password) => this.setState({ Password })}
              autoCapitalize="none"
              secureTextEntry={true}
              value={this.state.Password}
            />
          </View>
          <View style={styles.space}></View>
          <View style={styles.content}>
            <Text style={styles.columnText}>確認</Text>
            <TextInput
              placeholder="確認密碼..."
              placeholderTextColor="gray"
              style={styles.textInput}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={(Password) => this.setState({ checkPassword: Password })}
              onEndEditing={() => this._checkPassword(this.state.checkPassword)}
              value={this.state.checkPassword}
            />
          </View>
          <View style={styles.space}></View>
          <View style={styles.btnContent}>

            <View style={styles.button}>
              <Button onPress={() => {
                if (this.state.Account == '' || this.state.Password == '' || this.state.checkPassword == '') {
                  alert('請正確填寫欄位！！');
                } else if (this.state.checkPassword !== this.state.Password) {
                  Keyboard.dismiss();
                }
                else {
                  this.setSelectedOption('基本資料');
                }
              }}
                title="Next >"
                color="white"
              >
              </Button>
            </View>
          </View>
        </View>
      );
    }
  }
  _basicShow = () => {
    const camera = (<Icon name="camera" size={30} color="gray" />)

    if (this.state.basicVisible) {
      return (
        <View>
          <ScrollView
            automaticallyAdjustContentInsets={false}
            contentInset={{ top: 0 }}
            style={styles.scrollView}>
            <View style={styles.content}>
              <View style={styles.imageView}>
                <TouchableOpacity onPress={() => this.selectPhotoTapped()}>
                  <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <View style={[styles.avatar, styles.avatarContainer]}>
                      {this.state.avatarSource === null ? <Text>Select a Photo</Text> :
                        <Image style={styles.avatar} source={this.state.avatarSource} />
                      }
                    </View>
                    <Text style={{ fontSize: 18, backgroundColor: 'rgba(0,0,0,0)', marginLeft: 20 }}>{camera} 上傳大頭貼</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.space}></View>

            <View style={styles.content}>
              <Text style={styles.columnText}>姓名</Text>
              <TextInput
                placeholder="姓名..."
                placeholderTextColor="gray"
                style={styles.textInput}
                autoCapitalize="none"
                value={this.state.Name}
                onChangeText={(Name) => this.setState({ Name })} />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>生日</Text>
              <DatePicker
                style={{ width: 200 }}
                date={this.state.Birthday}
                mode="date"
                placeholder="placeholder"
                format="YYYY/MM/DD"
                minDate="1900/01/01"
                maxDate={this.state.toDay}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={(Birthday) => {
                  this.setState({ Birthday: Birthday });
                  this._Constellation(Birthday)
                }}
              />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>信箱</Text>
              <TextInput
                placeholder="信箱..."
                placeholderTextColor="gray"
                style={styles.textInput}
                value={this.state.Email}
                onChangeText={(Email) => this.setState({ Email })}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.space}></View>

            <View style={styles.content}>
              <Text style={styles.columnText}>性別</Text>
              <TouchableOpacity
                onPress={() => {
                  this.sex.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Sex}
                />
              </TouchableOpacity>
              <SinglePicker
                lang="en-US"
                ref={ref => this.sex = ref}
                onConfirm={(option) => {
                  this.setState({ Sex: option.value })
                }}
                options={sex} />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>身高</Text>
              <TextInput
                placeholder="身高..."
                placeholderTextColor="gray"
                keyboardType='number-pad'
                value={parseInt(this.state.Height)}
                style={styles.textInput}
                onChangeText={(Height) => this.setState({ Height })}
              />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>體重</Text>
              <TextInput
                placeholder="體重..."
                placeholderTextColor="gray"
                keyboardType='number-pad'
                style={styles.textInput}
                value={parseInt(this.state.Weight)}
                onChangeText={(Weight) => this.setState({ Weight })}
              />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>居住</Text>
              <TouchableOpacity
                onPress={() => {
                  this.address.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Address}
                />
              </TouchableOpacity>
              <SinglePicker
                lang="en-US"
                ref={ref => this.address = ref}
                onConfirm={(option) => {
                  this.setState({ Address: option.value })
                }}
                options={address} />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>性向</Text>
              <TouchableOpacity
                onPress={() => {
                  this.aptitude.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Aptitude}
                />
              </TouchableOpacity>
              <SinglePicker
                lang="en-US"
                ref={ref => this.aptitude = ref}
                onConfirm={(option) => {
                  this.setState({ Aptitude: option.value })
                }}
                options={aptitude} />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>血型</Text>
              <TouchableOpacity
                onPress={() => {
                  this.bloodPicker.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Blood}
                />
              </TouchableOpacity>
              <SinglePicker
                lang="en-US"
                ref={ref => this.bloodPicker = ref}
                onConfirm={(option) => {
                  this.setState({ Blood: option.value })
                }}
                options={blood} />

            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>戀愛狀況</Text>
              <TouchableOpacity
                onPress={() => {
                  this.isLovePicker.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.isLove}
                />
              </TouchableOpacity>
              <SinglePicker
                lang="en-US"
                ref={ref => this.isLovePicker = ref}
                onConfirm={(option) => {
                  this.setState({ isLove: option.value })
                }}
                options={isLove} />

            </View>
            <View style={styles.space}></View>
            <View style={styles.content}>
              <Text style={styles.columnText}>我的類型</Text>
              <TouchableOpacity
                onPress={() => {
                  this.myStyle.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={
                    this.state.Sex === '男' ?
                      this.state.M_myStyle :
                      this.state.W_myStyle
                  }
                />
              </TouchableOpacity>
              {
                this.state.Sex === '男' ?
                  <SinglePicker
                    lang="en-US"
                    ref={ref => this.myStyle = ref}
                    onConfirm={(option) => {
                      this.setState({ M_myStyle: option.value, M_myStyle_key: option.key })
                    }}
                    options={M_myStyle} />
                  :
                  <SinglePicker
                    lang="en-US"
                    ref={ref => this.myStyle = ref}
                    onConfirm={(option) => {
                      this.setState({ W_myStyle: option.value, W_myStyle_key: option.key })
                    }}
                    options={W_myStyle} />
              }
            </View>
            <View style={styles.space}></View>
            <View style={{ height: 200 }}>
              <Text style={styles.columnText}>興趣</Text>
              {this._interest()}
            </View>
            <View style={styles.btnContent}>

              <View style={styles.button}>
                <Button onPress={() => {
                  var str = this.state.d0 + this.state.d1 + this.state.d2 + this.state.d3 + this.state.d4 + this.state.d5 + this.state.d6 + this.state.d7 + this.state.d8 + this.state.d9 + this.state.d10;
                  var birthday = new Date(this.state.Birthday)
                  var age = 2017 - birthday.getFullYear();
                  let now = new Date();
                  this.setState({
                    Interest: str,
                    Age: age
                  }, function () {
                    if (this.state.Name == '' || this.state.Age == 0 || this.state.Sex == '' || this.state.Aptitude == '' || this.state.Email == '' || this.state.Constellation == '' || this.state.Blood == '' || this.state.Height == -1 || this.state.Weight == -1 || this.state.Address == '' || this.state.isLove == '' || this.state.Image == '' || this.state.Interest == '') {
                      var str = '';
                      if (this.state.Name == '')
                        str += "姓名 ";
                      else if (this.state.Age == 0)
                        str += "生日 ";
                      else if (this.state.Sex == '')
                        str += "性別 ";
                      else if (this.state.Aptitude == '')
                        str += "性向 ";
                      else if (this.state.Email == '')
                        str += "信箱 ";
                      else if (this.state.Constellation == '')
                        str += "星座 ";
                      else if (this.state.Blood == '')
                        str += "血型 ";
                      else if (this.state.Height == -1)
                        str += "身高 ";
                      else if (this.state.Weight == -1)
                        str += "體重 ";
                      else if (this.state.Address == '')
                        str += "地址 ";
                      else if (this.state.isLove == '')
                        str += "戀愛狀況 ";
                      else if (this.state.Image == '')
                        str += "圖片 ";
                      else if (this.state.Interest == '')
                        str += "興趣 ";
                      // console.log({ image: this.state.image, interest: this.state.Interest })
                      alert("請正確填寫" + str);
                    } else {
                      this.setSelectedOption('個人喜好');
                    }
                  })
                }}
                  title="Next >"
                  color="white"
                >
                </Button>
              </View>
            </View>
          </ScrollView>
        </View>
      );
    }
  }
  _favoriteShow = () => {
    if (this.state.favoriteVisible) {
      return (
        <View>
          <ScrollView
            automaticallyAdjustContentInsets={true}
            contentInset={{ top: 0 }}
            style={styles.scrollView}>
            <View style={styles.space}></View>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
              <Text style={styles.columnText}>喜歡{this.state.Sex === '男' ? '女' : '男'}生的...</Text>
            </View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>髮型</Text>
              <TouchableOpacity
                onPress={() => {
                  this.hairStyle.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Sex === '男' ? this.state.W_Hairstyle : this.state.M_Hairstyle}
                />
              </TouchableOpacity>
              {
                this.state.Sex === '男' ?
                  <ImagePicker
                    ref={ref => this.hairStyle = ref}
                    onConfirm={(option) => {
                      this.setState({ W_Hairstyle: option.value, W_Hairstyle_key: option.key - 1 })
                    }}
                    options={W_Hairstyle} />
                  :
                  <ImagePicker
                    lang="en-US"
                    ref={ref => this.hairStyle = ref}
                    onConfirm={(option) => {
                      this.setState({ M_Hairstyle: option.value, M_Hairstyle_key: option.key - 1 })
                    }}
                    options={M_Hairstyle} />
              }
            </View>
            {
              // this.state.Sex === '男' ?
              //   <View>
              //     <View style={styles.space}></View>
              //     <View style={styles.content2}>
              //       <Text style={styles.columnText}>瀏海</Text>
              //       <TouchableOpacity
              //         onPress={() => {
              //           this.headHair.show();
              //         }}>
              //         <TextInput
              //           placeholder="請選擇"
              //           placeholderTextColor="gray"
              //           style={styles.dropdown}
              //           editable={false}
              //           value={this.state.W_Headhair}
              //         />
              //       </TouchableOpacity>
              //       <ImagePicker
              //         ref={ref => this.headHair = ref}
              //         onConfirm={(option) => {
              //           this.setState({ W_Headhair: option.value })
              //         }}
              //         options={W_Headhair} />
              //     </View>
              //     <View style={styles.space}></View>
              //   </View>
              //   :
              //   <View style={styles.space}></View>
            }
            <View style={styles.space}></View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>身材</Text>
              <TouchableOpacity
                onPress={() => {
                  this.Stature.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Sex === '男' ? this.state.W_Stature : this.state.M_Stature}
                />
              </TouchableOpacity>
              {
                this.state.Sex === '男' ?
                  <SinglePicker
                    ref={ref => this.Stature = ref}
                    onConfirm={(option) => {
                      this.setState({ W_Stature: option.value, W_Stature_key: option.key })
                    }}
                    options={W_Stature} />
                  :
                  <SinglePicker
                    lang="en-US"
                    ref={ref => this.Stature = ref}
                    onConfirm={(option) => {
                      this.setState({ M_Stature: option.value, M_Stature_key: option.key })
                    }}
                    options={M_Stature} />
              }
            </View>
            <View style={styles.space}></View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>身高</Text>
              <TouchableOpacity
                onPress={() => {
                  this.Height.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Sex === '男' ? this.state.W_Height : this.state.M_Height}
                />
              </TouchableOpacity>
              {
                this.state.Sex === '男' ?
                  <SinglePicker
                    ref={ref => this.Height = ref}
                    onConfirm={(option) => {
                      this.setState({ W_Height: option.value, W_Height_key: option.key })
                    }}
                    options={W_Height} />
                  :
                  <SinglePicker
                    lang="en-US"
                    ref={ref => this.Height = ref}
                    onConfirm={(option) => {
                      this.setState({ M_Height: option.value, M_Height_key: option.key })
                    }}
                    options={M_Height} />
              }
            </View>
            <View style={styles.space}></View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>外觀</Text>
              <TouchableOpacity
                onPress={() => {
                  this.Style.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.Sex === '男' ? this.state.W_Style : this.state.M_Style}
                />
              </TouchableOpacity>
              {
                this.state.Sex === '男' ?
                  <SinglePicker
                    ref={ref => this.Style = ref}
                    onConfirm={(option) => {
                      this.setState({ W_Style: option.value, W_Style_key: option.key })
                    }}
                    options={W_myStyle} />
                  :
                  <SinglePicker
                    lang="en-US"
                    ref={ref => this.Style = ref}
                    onConfirm={(option) => {
                      this.setState({ M_Style: option.value, M_Style_key: option.key })
                    }}
                    options={M_myStyle} />
              }
            </View>
            <View style={styles.space}></View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>年齡</Text>
              <TouchableOpacity
                onPress={() => {
                  this.LikeAge.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.LikeAge}
                />
              </TouchableOpacity>
              <SinglePicker
                ref={ref => this.LikeAge = ref}
                onConfirm={(option) => {
                  this.setState({ LikeAge: option.value, LikeAge_key: option.key })
                }}
                options={LikeAge} />
            </View>
            <View style={styles.space}></View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>臉型</Text>
              <TouchableOpacity
                onPress={() => {
                  this.FaceType.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.FaceType}
                />
              </TouchableOpacity>
              <ImagePicker
                ref={ref => this.FaceType = ref}
                onConfirm={(option) => {
                  this.setState({ FaceType: option.value, FaceType_key: option.key - 1 })
                }}
                options={FaceType} />
            </View>
            <View style={styles.space}></View>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
              <Text style={styles.columnText}>第一眼看{this.state.Sex === '男' ? '女' : '男'}生的...</Text>
            </View>
            <View style={styles.content2}>
              <Text style={styles.columnText}>部位</Text>
              <TouchableOpacity
                onPress={() => {
                  this.FirstSee.show();
                }}>
                <TextInput
                  placeholder="請選擇"
                  placeholderTextColor="gray"
                  style={styles.dropdown}
                  editable={false}
                  value={this.state.FirstSee}
                />
              </TouchableOpacity>
              <SinglePicker
                ref={ref => this.FirstSee = ref}
                onConfirm={(option) => {
                  this.setState({ FirstSee: option.value, FirstSee_key: option.key })
                }}
                options={FirstSee} />
            </View>
            <View style={styles.space}></View>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 20 }}>
              <Text style={styles.columnText}>心情小語</Text>
            </View>
            <View style={styles.content2}>
              <TextInput
                placeholder="心情小語..."
                placeholderTextColor="gray"
                style={styles.textInput}
                onChangeText={(Profile) => this.setState({ Profile })}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.content2}>

              <View style={styles.button}>
                <Button onPress={() => { this.register() }}
                  title="註冊"
                  color="white"
                >
                </Button>
              </View>
            </View>

          </ScrollView>

        </View>
      );
    }
  }
  setSelectedOption = (selectedOption) => {
    if (selectedOption == "基本資料") {
      this.setState({
        basicVisible: true,
        favoriteVisible: false,
        accountVisible: false,
      });
    } else if (selectedOption == "個人喜好") {
      this.setState({
        basicVisible: false,
        favoriteVisible: true,
        accountVisible: false,
      });
    } else if (selectedOption == "帳號密碼") {
      this.setState({
        basicVisible: false,
        favoriteVisible: false,
        accountVisible: true,
      });
    }
    this.setState({
      selectedOption: selectedOption
    });
  }
  render() {
    const options = [
      '帳號密碼',
      '基本資料',
      '個人喜好'
    ];
    const image = [{ image: '3-randomTest-random-1.png' }, { image: '1-randomTest-random-1.png' }]

    function renderOption(option, selected, onSelect, index) {
      const style = selected ? { fontWeight: 'bold' } : {};
      return (
        <TouchableWithoutFeedback onPress={onSelect} key={index}>
          <Text style={style}>{option}</Text>
        </TouchableWithoutFeedback>
      );
    }

    function renderContainer(optionNodes) {

      return <View>{optionNodes}</View>;
    }
    return (
      <View style={styles.bigview}>
        {
          // <View style={styles.toparea}>
          //   <SegmentedControls
          //     tint={'rgba(52, 52, 52, 0.8)'}
          //     selectedTint={'#FFFFFF'}
          //     selectedBackgroundColor={'rgba(52, 52, 52, 0.8)'}
          //     backTint={'rgba(52, 52, 52, 0)'}
          //     options={options}
          //     allowFontScaling={false} // default: true
          //     onSelection={this.setSelectedOption.bind(this)}
          //     selectedOption={this.state.selectedOption}
          //     optionStyles={{ fontFamily: 'AvenirNext-Medium' }}
          //     optionContainerStyle={{ flex: 1 }}
          //   />
          // </View>
        }
        <View style={{ height: height * 0.9 }}>
          {this._accountShow()}
          {this._basicShow()}
          {this._favoriteShow()}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toparea: {
    marginTop: height * 0.04,
    marginBottom: height * 0.01,
    width: width * 0.8,
    height: height * 0.06
  },
  imageView: {
    // width: width - width * 0.1,
    // zIndex: 1,
    // position: 'absolute',
    // alignItems: 'center',
  },
  cricle: {
    height: 80,
    width: 80,
    borderWidth: 3,
    borderColor: 'rgba(52, 52, 52, 0.8)',
    borderRadius: 40,
    marginBottom: 10,
    marginLeft: 20,
    // justifyContent:'center',
  },
  loginText: {
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 40,
    width: 80,
    height: 80
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  backgroundImage: {
    flex: 1,
    //resizeMode: 'cover', // or 'stretch'
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection:'row'
  },
  bigview: {
    justifyContent: 'flex-start',
    // flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    height: height

  },
  content: {
    // justifyContent: 'center',s
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  btnContent: {
    justifyContent: 'center',
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content2: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  radioContent: {
    alignItems: 'center',
    // flex: 1,
    flexDirection: 'row',
  },
  columnText: {
    fontSize: 20,
    marginRight: 10,
    marginTop: 5,
    backgroundColor:'rgba(52,52,52,0)'
  },
  columnRadio: {
    fontSize: 20,
    marginRight: 10,
    // marginTop: 12,
    marginLeft: 5,
  },
  textInput: {
    width: width * 0.6,
    // alignItems: 'stretch',
    height: 30,
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 5,
    fontFamily: 'Euphemia UCAS',

  },
  space: {
    height: 20,
  },
  radioSpace: {
    height: 10,
  },
  button: {
    width: width * 0.25,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'black',
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slide1: {
    justifyContent: 'center',
    padding: width * 0.1,
    width: width,
    height: 600,
    alignItems: 'center',
  },
  scrollView: {
    paddingHorizontal: width * 0.12,
    width: width,
    height: 500,
  },
  radioView: {
    // flex:1,
    flexDirection: 'row',
  },
  passwordBtn: {
    paddingLeft: width * 0.04,
    paddingRight: width * 0.04,
    paddingTop: width * 0.02,
    paddingBottom: width * 0.02,

    backgroundColor: 'black'
  },
  dropdown: {
    width: width * 0.4,
    // alignItems: 'stretch',
    height: 30,
    borderColor: 'black',
    borderWidth: 1,
    paddingLeft: 5,
    fontFamily: 'Euphemia UCAS',
  }
});

export default Register;

