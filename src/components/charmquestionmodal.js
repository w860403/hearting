import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Modal,
    Image,
    ListView,
    TextInput,
    TouchableHighlight
} from 'react-native';
import SinglePicker from 'mkp-react-native-picker';
var imgUri = 'http://114.35.74.209/Hearting/Upload/';
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
var { height, width } = Dimensions.get('window');
class CharmQuestionModal extends Component {
    constructor(props) {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        super(props)
        console.log(this.props)
        this.state = {
            dataSource: ds.cloneWithRows(this.props.charmList),
            ds: ds,
            modalVisible: this.props.modalVisible ? this.props.modalVisible : true,
            charmList: this.props.charmList ? this.props.charmList : [],
            sex: this.props.sex ? this.props.sex : null,
            charmPickerList: [this.p1, this.p2, this.p3, this.p4, this.p5, this.p6, this.p7, this.p8, this.p9, this.p10],
            optionValueList: ['', '', '', '', '', '', '', '', '', ''],
            checkWriteOrNot: true,
            notWriteMessageVisiable: false
        }
    }
    componentWillReceiveProps(props) {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        if (props.charmList) {
            this.setState({
                //dataSource: ds.cloneWithRows(props.charmList),
                dataSource: ds.cloneWithRows(props.charmList),
                modalVisible: props.modalVisible,
                charmList: props.charmList,
            }, function () {
                console.log(this.state.charmList);
            });
        }
    }
    chooseCharm(rowID, charmOption) {
        console.log(rowID)
        var newDs = [];
        var optionValueList = [];
        newDs = this.state.charmList.slice();
        optionValueList = this.state.optionValueList.slice();
        newDs[rowID].Style = charmOption.key;
        newDs[rowID].StyleText = charmOption.value;
        optionValueList[rowID] = charmOption.value;
        console.log(newDs)
        this.setState({
            dataSource: this.state.ds.cloneWithRows(newDs),
            charmList: newDs,
            optionValueList: optionValueList
        })
    }
    checkWrite(charmList, sex) {
        var self = this
        for (i = 0; i < charmList.length; i++) {
            if (charmList[i].Style == null) {
                self.setState({
                    checkWriteOrNot: false
                })
                console.log(charmList[i].Account + 'didnt choose charm');
                break;
            } else {

            }
        }
        setTimeout(() => {
            if (self.state.checkWriteOrNot) {
                console.log('all stuff is pick')
                console.log(charmList);
                console.log(sex);
                self.setState({
                    notWriteMessageVisiable: false
                })
                self.props.saveAsCharm(charmList, sex)
                self.setState({
                    charmList: [],
                    sex: null,
                })
                self.props.setModalVisible(false)
            } else {
                self.setState({
                    notWriteMessageVisiable: true
                }, function () {
                    self.setState({
                        checkWriteOrNot: true
                    })
                })
            }
        }, 500);
    }
    render() {
        const M_Style = [{
            key: 0,
            value: "帥氣"
        }, {
            key: 1,
            value: "沉穩"
        }, {
            key: 2,
            value: "可愛"
        }, {
            key: 3,
            value: "斯文"
        }, {
            key: 4,
            value: "壞壞"
        }];
        const W_Style = [{
            key: 0,
            value: "可愛"
        }, {
            key: 1,
            value: "性感"
        }, {
            key: 2,
            value: "柔弱"
        }, {
            key: 3,
            value: "俏皮"
        }, {
            key: 4,
            value: "氣質"
        }];
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={this.state.modalVisible}>
                <View style={{ width: width, height: height, backgroundColor: 'rgba(52,52,52,0.5)', justifyContent: 'center', alignItems: 'center', }}>
                    <View style={styles.windows}>
                        <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                            <View style={styles.bigTitleView}>
                                <Text style={styles.bigTitleText}>
                                    魅力調查問卷
                            </Text>
                            </View>
                            <View style={styles.titleView}>
                                <View style={styles.titleLeftView}>
                                </View>
                                <View style={styles.titleMiddleView}>
                                    <Text style={styles.titleText}>心動指數</Text>
                                </View>
                                <View style={styles.titleRightView}>
                                    <Text style={styles.titleText}>吸引你的點</Text>
                                </View>
                            </View>
                            <View style={styles.listView}>
                                {this.props.charmList.length != 0 ?
                                    <ListView
                                        dataSource={this.state.dataSource}
                                        renderRow={(rowData, sectionID, rowID) =>
                                            <View style={{ borderColor: 'rgba(52,52,52,0.5)', borderBottomWidth: 1, }}>
                                                <View style={styles.content}>
                                                    <View style={styles.listViewContent}>
                                                        <View style={styles.listViewContentLeftView}>
                                                            <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.Image }} style={styles.image} />
                                                        </View>
                                                        <View style={styles.listViewContentMiddleView}>
                                                            <View style={styles.listViewContentMiddleView}>
                                                                <Text style={styles.pointText}>
                                                                    {rowData.Score}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.listViewContentRightView}>
                                                            <TouchableOpacity
                                                                onPress={() => {
                                                                    this.state.charmPickerList[rowID].show();
                                                                }}>
                                                                <TextInput
                                                                    placeholder="請選擇"
                                                                    placeholderTextColor="gray"
                                                                    style={styles.dropdown}
                                                                    value={this.state.optionValueList[rowID]}
                                                                    editable={false}
                                                                />
                                                            </TouchableOpacity>
                                                            {
                                                                this.state.sex == '男' ?
                                                                    <SinglePicker
                                                                        lang="en-US"
                                                                        ref={ref => this.state.charmPickerList[rowID] = ref}
                                                                        onConfirm={(option) => {
                                                                            this.chooseCharm(rowID, option)
                                                                        }}
                                                                        options={M_Style} />
                                                                    :
                                                                    <SinglePicker
                                                                        lang="en-US"
                                                                        ref={ref => this.state.charmPickerList[rowID] = ref}
                                                                        onConfirm={(option) => {
                                                                            this.chooseCharm(rowID, option)
                                                                        }}
                                                                        options={W_Style} />
                                                            }
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        }
                                    />
                                    :
                                    <View style={{alignItems:'center'}}>
                                        <Text style={{backgroundColor:'rgba(52,52,52,0)'}}>本次測驗無心動的會員</Text>
                                    </View>
                                }

                            </View>
                            <View style={styles.bottomView}>
                                <View>
                                    <Text style={styles.notWriteMessageText}>
                                        {this.state.notWriteMessageVisiable ? '選項選完才能送出唷～' : null}
                                    </Text>
                                </View>
                                <TouchableHighlight
                                    style={styles.button}
                                    activeOpacity={0.8}
                                    underlayColor='rgba(52,52,52,0.5)'
                                    onPress={() => this.checkWrite(this.state.charmList, this.state.sex)}>
                                    <Text style={styles.commitText}>確定</Text>
                                </TouchableHighlight>
                            </View>
                        </Image>
                    </View>
                </View>
            </Modal>
        );
    }
    static propTypes = {
        onClick: PropTypes.func,
    };
    static defaultProps = {
        onClick: () => {
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent:'center',
        alignItems: 'flex-start',
        flexDirection: 'row'
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    dropdown: {
        width: width * 0.2,
        // alignItems: 'stretch',
        height: 30,
        borderColor: 'black',
        borderWidth: 1,
        paddingLeft: 5,
        fontFamily: 'Euphemia UCAS',
        alignItems: 'center',
        justifyContent: 'center',
    },
    windows: {
        width: width * 0.8,
        height: height * 0.8,
        backgroundColor: 'rgba(255,255,255,1)',
        alignItems: 'center',
        borderRadius: 20
    },
    bigTitleView: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    notWriteMessageText: {
        backgroundColor: 'rgba(255,255,255,0)',
        color: 'red',
        fontSize: 17
    },
    bottomView: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    bigTitleText: {
        fontSize: 25,
        backgroundColor: 'rgba(52,52,52,0)'
    },
    titleView: {
        marginTop: 30,
        flexDirection: 'row',
        width: width * 0.8,
        height: 30,
        borderColor: 'rgba(52,52,52,0.5)',
        borderBottomWidth: 1,
    },
    titleLeftView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    titleMiddleView: {
        alignItems: 'flex-end',
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    titleRightView: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'flex-end',
    },
    titleText: {
        fontSize: 18,
        backgroundColor: 'rgba(0,0,0,0)'
    },
    content: {
        width: width * 0.8,
        borderColor: 'rgba(52,52,52,0.5)',
    },
    square: {
        height: 60,
        width: 60,
        borderWidth: 3,
        borderColor: 'rgba(52, 52, 52, 0.8)',
        marginLeft: 10
    },
    listView: {
        height: height * 0.5,
        width: width * 0.8,
    },
    listViewContent: {
        flexDirection: 'row',
        width: width * 0.8,
        height: 70,
        alignItems: 'center',
    },
    listViewContentLeftView: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    listViewContentMiddleView: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    listViewContentRightView: {
        alignItems: 'center',
        flex: 1
    },
    image: {
        borderColor: 'rgba(52,52,52,0.5)',
        borderWidth: 1,
        width: 50,
        height: 50
    },
    pointText: {
        fontSize: 25,
        color: '#E86868',
        backgroundColor: 'rgba(52,52,52,0)',
    },
    button: {
        backgroundColor: 'rgba(52,52,52,0.6)',
        width: 60,
        height: 30,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    commitText: {
        fontSize: 20,
        color: 'white'
    }
});

export default CharmQuestionModal;
