import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  TouchableWithoutFeedback,
  ScrollView,
  TextInput,
  TouchableOpacity,
  ListView,
  AlertIOS
} from 'react-native';
import FriendDetail from './frienddetailmodal'
import TopBar from '../containers/topbar'
var { height, width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/Foundation';

const typing = (<Icon2 name="comments-o" size={30} color="black" />)
const newMessage = (<Icon3 name="burst-new" size={35} color="black" />)

var ScrollableTabView = require('react-native-scrollable-tab-view');
class Friends extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(this.props.friendsData),
      friendsData: this.props.friendsData,
      ds: ds,
      imageName: '',
      friendName: '',
      profile: '',
      partner: '',
      rowID: '',
      account: '',
      friendDetailModalVisible: false,
    }
  }
  _setFriendDetailModalVisible = (visible, imageName, friendName, profile, account, partner, rowID) => {
    this.setState({ friendDetailModalVisible: visible, imageName: imageName, friendName: friendName, profile: profile, account: account, partner: partner, rowID: rowID });
  }

  pressRow(rowID) {
    var newDs = [];
    newDs = this.state.friendsData.slice();
    newDs.splice(rowID, 1)
    this.setState({
      dataSource: this.state.ds.cloneWithRows(newDs),
      friendsData: newDs,
    })
  }
  render() {
    const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
    var self = this;
    if (!this.state.loaded)
      return (
        <ActivityIndicator
          animating={this.state.animating}
          style={[styles.centering, { height: 80 }]}
          size="large"
        />
      );
    return (
      <View style={styles.bigview}>
        <TopBar title='好友' backButtonVisible={false} lefttext='Back' addfriend={true} />

        <FriendDetail
          modalVisible={this.state.friendDetailModalVisible}
          imageName={this.state.imageName}
          friendName={this.state.friendName}
          profile={this.state.profile}
          partner={this.state.partner}
          account={this.state.account}
          rowID={this.state.rowID}
          loveLetterStatusData={this.state.loveLetterStatusData}
          onClick={(option) => {
            this.setState({ friendDetailModalVisible: option })
          }}
          pressRow={(rowID) => {
            this.pressRow(rowID)
          }}
          deleteFriend={(account, partner) => {
            this.props.deleteFriend(account, partner)
          }}
          goToLoveLetterInput={(account, partner) => {
            this.props.goToLoveLetterInput(account, partner)
          }}
          getChat={(account, partner) => {
            this.props.getChat(account, partner)
          }}
          cancelLoveLetter={(account) => {
            this.props.cancelLoveLetter(account)
          }}
          goToLoveLetterDisplay={(account) => {
            this.props.goToLoveLetterDisplay(account)
          }}
        />
        <ScrollableTabView
          tabBarActiveTextColor="#ED6D86"
          tabBarUnderlineStyle={{ backgroundColor: '#ED6D86', height: 2 }}
          // tabBarActiveTextColor="#1fb5ec"
          tabBarTextStyle={{ fontSize: 17, backgroundColor: 'rgba(0,0,0,0)' }}
        >
          <View tabLabel='好友' style={styles.slide1} >
            <View style={styles.content}>
              <View style={styles.titleListImg}>
              </View>
              <View style={styles.titleListName}>
                <Text style={styles.listTitleText}>
                  名字
              </Text>
              </View>
              <View style={styles.titleListNum}>
                <Text style={styles.listTitleText}>
                  每日好感度
              </Text>
              </View>
              <View style={styles.titleListChat}>
                <Text style={styles.listTitleText}>
                  聊天
              </Text>
              </View>
            </View>

            <View style={styles.contentList}>
              {this.props.friendsData.length != 0 ?
                <ListView
                  dataSource={this.state.dataSource}
                  renderRow={(rowData, sectionID, rowID) => {
                    const down = (<Icon name="chevron-down" size={20} color="#383838" />)
                    return (
                      <View style={{ borderBottomWidth: 1, borderBottomColor: 'black', }}>
                        <TouchableHighlight
                          style={styles.button}
                          activeOpacity={0.8}
                          underlayColor='rgba(255,255,255,0)'
                          onPress={() => this._setFriendDetailModalVisible(true, rowData.Image, rowData.Name, rowData.Profile, this.props.account, rowData.Partner, rowID)}>
                          <View style={styles.listView}>
                            <View style={styles.imageView}>
                              <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.Image }} style={styles.square} />
                            </View>
                            <View style={styles.listName}>
                              <Text style={styles.nameText}>
                                {rowData.Name}
                              </Text>
                            </View>
                            <View style={styles.listNum}>
                              <Text style={styles.pairPersentText}>
                                {rowData.finalval ? rowData.finalval : '無數據'}
                              </Text>
                              {rowData.number == 0 ?
                                <Text style={styles.pairPersentTextBlack}>
                                  {'+' + rowData.number}
                                </Text>
                                : null}
                              {rowData.number > 0 ?
                                <Text style={styles.pairPersentTextGreen}>
                                  {'+' + rowData.number}
                                </Text>
                                : null}
                              {rowData.number < 0 ?
                                <Text style={styles.pairPersentTextRed}>
                                  {rowData.number}
                                </Text>
                                : null}
                            </View>
                            <View style={styles.listChat}>
                              <TouchableHighlight
                                activeOpacity={0.8}
                                underlayColor='rgba(52,52,52,0)'
                                onPress={() => { self.props.getChat(rowData.Partner, rowData.Name) }}>
                                {typing}
                              </TouchableHighlight>
                            </View>
                          </View>
                        </TouchableHighlight>
                      </View>
                    )
                  }}
                />
                :
                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                  <Text style={{ fontSize: 20, backgroundColor: 'rgba(0,0,0,0)' }}>無好友資料</Text>
                </View>}
            </View>
          </View>
          <View tabLabel='聊天' style={styles.slide1}>
            {this.props.chatList.length != 0 ?
              <ListView
                dataSource={this.state.chatListSource}
                renderRow={(rowData, sectionID, rowID) => {
                  const down = (<Icon name="chevron-down" size={20} color="#383838" />)
                  return (
                    <View style={{ borderBottomWidth: 1, borderBottomColor: 'black', }}>
                      <TouchableHighlight
                        style={styles.button}
                        activeOpacity={0.8}
                        underlayColor='rgba(255,255,255,0)'
                        onPress={() => { self.props.getChat(rowData.Partner, rowData.Name) }}
                      >
                        <View style={styles.chatListView}>
                          <View style={styles.imageView}>
                            <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.Image }} style={styles.square} />
                          </View>
                          <View style={styles.chatList}>
                            <Text style={styles.nameText}>
                              {rowData.Name}
                            </Text>
                            <Text style={{ fontSize: 15, color: '#6D6D6D', marginTop: 8 }}>
                              {rowData.Content ? rowData.Content : '無聊天記錄'}
                            </Text>
                          </View>
                          <View style={styles.listChatNew}>
                            {rowData.isLook == "1" ? null :
                              newMessage
                            }
                          </View>
                        </View>

                      </TouchableHighlight>
                    </View>
                  )
                }}
              />
              :
              <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }}>
                <Text style={{ fontSize: 20, backgroundColor: 'rgba(0,0,0,0)' }}>無聊天記錄</Text>
              </View>}
          </View>
        </ScrollableTabView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  bigview: {
    // justifyContent: 'center',
    flex: 1,
    flexDirection: 'column',
    // alignItems: 'center',
    // paddingTop: height * 0.05
  },
  contentList: {
    height: height * 0.71
  },
  square: {
    height: 60,
    width: 60,
    borderWidth: 3,
    borderColor: 'rgba(52, 52, 52, 0.8)',
    marginLeft: 10
  },
  content: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    paddingBottom: 13,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slide1: {
    // justifyContent: 'center',
    paddingLeft: width * 0.05,
    paddingRight: width * 0.05,
    width: width,
    height: 600,
    marginTop: height * 0.05
    // alignItems: 'center',
  },
  titleListImg: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleListName: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleListNum: {
    flex: 1.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleListChat: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 13,
    paddingBottom: 13,
    justifyContent: 'center',
  },
  chatListView: {
    flexDirection: 'row',
    paddingTop: 13,
    paddingBottom: 13,
  },
  listName: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageView: {
    flex: 1,
  },
  listNum: {
    flex: 1.3,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  listChat: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52,52,52,0)'
  },
  listChatNew: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52,52,52,0)'
  },
  chatList: {
    flex: 2.8,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'rgba(52,52,52,0)'
  },
  pairPersentText: {
    fontSize: 20,
    color: '#383838',
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'Euphemia UCAS',
  },
  pairPersentTextBlack: {
    fontSize: 20,
    color: '#383838',
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'Euphemia UCAS',
  },
  pairPersentTextGreen: {
    fontSize: 20,
    color: 'green',
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'Euphemia UCAS',
  },
  pairPersentTextRed: {
    fontSize: 20,
    color: 'red',
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'Euphemia UCAS',
  },
  nameText: {
    fontSize: 20,
    color: '#383838',
    backgroundColor: 'rgba(0,0,0,0)',
    fontFamily: 'Euphemia UCAS',
  },
  listTitleText: {
    fontSize: 18,
    color: '#383838',
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(0,0,0,0)'
  },
  rowBack: {
    width: 75,
    height: 60,
    fontSize: 20,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Euphemia UCAS',
    backgroundColor: 'rgba(52,52,52,0.5)'
  }
});

export default Friends;
