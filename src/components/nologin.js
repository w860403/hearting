import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
    TouchableHighlight,
    Dimensions,
    Image,
    TextInput,
    AlertIOS,
    AsyncStorage,
    ActivityIndicator,
    Button,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/Entypo';

var { height, width } = Dimensions.get('window');

class noLogin extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.bigview}>
                <TouchableOpacity onPress={() => this.props.onLoginButtonClick()} >
                    <View style={styles.btnView}>
                        <Text style={styles.btnText}>登入</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.onRegisterButtonClick()} style={{ marginTop: 30 }}>
                    <View style={styles.btnView}>
                        <Text style={styles.btnText}>註冊</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.onRegisterButtonClick()} style={{ marginTop: 30 }}>
                    <View style={styles.btnView}>
                        <Text style={styles.btnText}>線上報名</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    bigview: {
        justifyContent:'center',
        flex: 1,
        flexDirection: 'column',
        // alignItems:'center',
    },
    btnView: {
        marginHorizontal: 30,
        // paddingHorizontal: 25,
        paddingVertical: 20,
        borderWidth: 1,
        borderRadius: 20,
        alignItems: 'center'
    },
    btnText: {
        backgroundColor: 'rgba(0,0,0,0)',
        fontSize: 20,

    }
});
export default noLogin;

