import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    NavigatorIOS,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    TextInput,
    AlertIOS,
    AsyncStorage,
    ActivityIndicator,
    ListView,
    ScrollView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
// import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon from 'react-native-vector-icons/Entypo';
var { height, width } = Dimensions.get('window');

class Home extends Component {
    constructor() {
        super();
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            userData: {},
            isLogin: false,
            imageUrl: 'http://114.35.74.209/Hearting/Upload/',
            relateDataSource: ds.cloneWithRows([
                { ImageName: '1-randomTest-random-1', kind: '頭髮' }, { ImageName: '2-randomTest-random-1', kind: '身材' },
                { ImageName: '3-randomTest-random-1', kind: '臉型' }, { ImageName: '4-randomTest-random-1', kind: '頭髮' },
                { ImageName: '5-randomTest-random-1', kind: '頭髮' }, { ImageName: '1-randomTest-random-1', kind: '頭髮' }, { ImageName: '2-randomTest-random-1', kind: '頭髮' },
                { ImageName: '3-randomTest-random-1', kind: '頭髮' }, { ImageName: '4-randomTest-random-1', kind: '頭髮' },
                { ImageName: '5-randomTest-random-1', kind: '頭髮' },
            ]),
            rankData: ds.cloneWithRows([
                { ImageName: '1-randomTest-random-1', rank: 1 }, { ImageName: '2-randomTest-random-1', rank: 2 },
                { ImageName: '3-randomTest-random-1', rank: 3 }
            ]),
            animating: true,
            loaded: false,
        };
    }
    // _renderPage(data, pageID) {
    //     return (
    //         <Image
    //             source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + data }}
    //             style={styles.page} />
    //     );
    // }
    render() {
        // var IMGS = [
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' },
        //     { image: '3-randomTest-random-1.png' }
        // ];
        // var ds = new ViewPager.DataSource({
        //     pageHasChanged: (p1, p2) => p1 !== p2,
        // });
        // var self = this;
        // var image = IMGS.map(function (data, index) {
        //     var dataSource = ds.cloneWithPages(data.image)
        //     return (
        //         <ViewPager
        //             style={{ width: width }}
        //             key={index}
        //             dataSource={dataSource}
        //             renderPage={self._renderPage}
        //             isLoop={true}
        //             autoPlay={true} />
        //     )
        // })
        if (!this.state.loaded)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={[styles.centering, { height: 80 }]}
                    size="large"
                />
            );
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var rank_girl = ds.cloneWithRows(this.props.rank_girl);
        var rank_boy = ds.cloneWithRows(this.props.rank_boy);
        return (

            <View style={styles.bigview} hidden={true}>
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    contentInset={{ top: 0 }}
                    style={styles.scrollView}>
                    <Image source={require('../images/handsome04.jpg')} style={{ width: width, height: height * 0.35 }} />

                    <View style={styles.relateListView}>
                        <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, marginHorizontal: 10 }}>外觀分析</Text>
                            <Text style={{ fontSize: 12, marginHorizontal: 10 }}>看看現在大眾喜愛的外觀吧！</Text>
                        </View>
                        <ListView
                            horizontal
                            dataSource={this.state.relateDataSource}
                            renderRow={(rowData) =>
                                <View style={styles.relateListViewContent}>
                                    <TouchableHighlight
                                        activeOpacity={0.8}
                                        underlayColor='rgba(0,0,0,0)'
                                        onPress={() => this.render}>
                                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={{ uri: this.state.imageUrl + rowData.ImageName + '.png' }} style={styles.relateImage} />
                                            <Text style={{ marginTop: 10 }}>{rowData.kind}</Text>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            }
                        />
                    </View>
                    <View style={styles.relateListView}>
                        <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, marginHorizontal: 10 }}>行為分析</Text>
                            <Text style={{ fontSize: 12, marginHorizontal: 10 }}>看看現在大眾喜愛的外觀吧！</Text>
                        </View>
                        <ListView
                            horizontal
                            dataSource={this.state.relateDataSource}
                            renderRow={(rowData) =>
                                <View style={styles.relateListViewContent}>
                                    <TouchableHighlight
                                        activeOpacity={0.8}
                                        underlayColor='rgba(0,0,0,0)'
                                        onPress={() => this.render}>
                                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                            <Image source={{ uri: this.state.imageUrl + rowData.ImageName + '.png' }} style={styles.relateImage} />
                                            <Text style={{ marginTop: 10 }}>{rowData.kind}</Text>
                                        </View>
                                    </TouchableHighlight>
                                </View>
                            }
                        />
                    </View>
                    <View style={{ height: 400 }}>
                        <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, marginHorizontal: 10 }}>排行榜</Text>
                            <Text style={{ fontSize: 12, marginHorizontal: 10 }}>本週排行！！</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                {this.props.rank_girl.length != 0 ?
                                    <ListView
                                        vertical
                                        dataSource={rank_girl}
                                        renderRow={(rowData) =>
                                            <View style={{ flexDirection: 'row' }}>
                                                <TouchableHighlight
                                                    activeOpacity={0.8}
                                                    underlayColor='rgba(0,0,0,0)'
                                                    onPress={() => this.render}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                                        <Image source={{ uri: this.state.imageUrl + rowData.ImgName }} style={styles.relateImage} />
                                                        <Text style={{ marginTop: 10 }}>{rowData.profile}</Text>
                                                    </View>
                                                </TouchableHighlight>
                                            </View>
                                        }
                                    />
                                    :
                                    <View style={{alignItems:'center'}}>
                                        <Text style={{backgroundColor:'rgba(0,0,0,0)'}}>本週暫無排名</Text>
                                    </View>
                                }
                            </View>
                            <View style={{ flex: 1 }}>
                                {this.props.rank_boy.length != 0 ?
                                    <ListView
                                        vertical
                                        dataSource={rank_boy}
                                        renderRow={(rowData) =>
                                            <View style={{ flexDirection: 'row' }}>
                                                <TouchableHighlight
                                                    activeOpacity={0.8}
                                                    underlayColor='rgba(0,0,0,0)'
                                                    onPress={() => this.render}>
                                                    <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                                        <Image source={{ uri: this.state.imageUrl + rowData.ImgName }} style={styles.relateImage} />
                                                        <Text style={{ marginTop: 10 }}>{rowData.profile}</Text>
                                                    </View>
                                                </TouchableHighlight>
                                            </View>
                                        }
                                    />
                                    :
                                    <View style={{alignItems:'center'}}>
                                        <Text style={{backgroundColor:'rgba(0,0,0,0)'}}>本週暫無排名</Text>
                                    </View>
                                }
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    cricle: {
        height: 120,
        width: 120,
        borderRadius: 60,
        marginBottom: 20
    },
    bigview: {
        // justifyContent: 'center',
        flex: 1,
        // flexDirection: 'row',
        alignItems: 'center',
    },
    slide1: {
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        margin: width * 0.035,
        width: width * 0.7,
        height: 400,
        alignItems: 'center',
    },
    textView: {
        marginBottom: 25,
        fontSize: 35,
        fontFamily: 'Euphemia UCAS',
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#A03066',
    },
    relateListView: {
        height: 160,
        width: width * 1,
        // backgroundColor: 'rgba(52,52,52,0.5)',
        // marginTop: 20,
        paddingLeft: 5,
        paddingRight: 5,
        borderBottomWidth: 1,
        borderColor: 'rgba(52,52,52,0.5)',
        paddingBottom: 10
    },
    relateListViewContent: {
    },
    relateImageCover: {
        width: 80,
        height: 80,
        backgroundColor: 'rgba(52,52,52,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    relateImageCoverText: {
        color: 'white',
        fontSize: 50
    },
    relateImage: {
        borderColor: 'rgba(52,52,52,0.5)',
        width: 80,
        height: 80,
        marginRight: 5,
        marginLeft: 5,
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollView: {
        // paddingHorizontal: width * 0.12,
        width: width,
        // height: 500,
    },
});
export default Home;