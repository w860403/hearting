import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ListView
} from 'react-native';

var { height, width } = Dimensions.get('window');
import Carousel from 'react-native-snap-carousel';
import { RadioButtons, SegmentedControls } from 'react-native-radio-buttons'
import Icon from 'react-native-vector-icons/Entypo';
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-icon-checkbox';
import SinglePicker from 'mkp-react-native-picker';
import ImagePreview from './imagepreview';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

class HistoryRank extends Component {
    constructor(props) {
        super(props);
        // var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            selectedOption: '隨機',
            slideIndex: 0,
            dataSource: {},
            modalVisible: false,
            imageName: '',
        }
    }
    setModalVisible = (visible, imageName) => {
        console.log(imageName);
        this.setState({ modalVisible: visible, imageName: imageName });
    }
    closeModalVisible = (visible, imageName) => {
        this.setState({
            modalVisible: visible,
            imageName: imageName,
            imgWidth: 0,
            imgHeight: 0
        });
    }
    _renderRow = (rowData, sectionID, rowID) => {
        console.log("start renderRow");
        console.log(rowData);
        const down = (<Icon name="chevron-down" size={20} color="#383838" />)
        return (
            <TouchableOpacity onPress={() => { this.setModalVisible(true, rowData.ImgName) }}>
                <View style={styles.listView}>
                    <View style={styles.imageView}>
                        <Image source={{ uri: 'http://114.35.74.209/Hearting/Upload/' + rowData.ImgName }} style={styles.square} />
                    </View>
                    <View style={styles.listNum}>
                        <Text style={styles.numText}>
                            02/23
                        </Text>
                        <Text style={styles.numText}>
                            {down}
                        </Text>
                        <Text style={styles.numText}>
                            02/23
                        </Text>
                    </View>

                    <View style={styles.listNum}>
                        <Text style={styles.numText}>
                            第 {rowData.Rank} 名
                        </Text>
                        <Text style={styles.numText}>
                            {rowData.HeartNumbers}人心動
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.selectedOption == '隨機' && this.state.slideIndex !== 0) {
            this._carousel.snapToItem(0)
            console.log('選擇 隨機')
        } else if (this.state.selectedOption == '零死角' && this.state.slideIndex !== 1) {
            this._carousel.snapToItem(1)
            console.log('選擇 零死角')
        } else if (this.state.selectedOption == '背影' && this.state.slideIndex !== 2) {
            this._carousel.snapToItem(2)
            console.log('選擇 背影')
        }

    }

    render() {
        const left = (<Icon name="chevron-thin-left" size={20} color="gray" />)
        const options = [
            '隨機',
            '零死角',
            '背影',
        ];
        function setSelectedOption(selectedOption) {
            this.setState({
                selectedOption: selectedOption
            });
        }
        function renderOption(option, selected, onSelect, index) {
            const style = selected ? { fontWeight: 'bold' } : {};
            return (
                <TouchableWithoutFeedback onPress={onSelect} key={index}>
                    <Text style={style}>{option}</Text>
                </TouchableWithoutFeedback>
            );
        }

        function renderContainer(optionNodes) {
            return <View>{optionNodes}</View>;
        }
        var list = [
            { index: 0, title: "隨機", data: [{ img: '3-randomTest-random-1.png', date: '02/12', rank: '4', people: '134' }, { img: '1-randomTest-random-1.png', date: '02/26', rank: '2', people: '345' }] },
            { index: 1, title: "零死角", data: [{ img: '2-randomTest-random-1.png', date: '03/13', rank: '5', people: '45' }] },
            { index: 2, title: "背影", data: [{ img: '4-randomTest-random-1.png', date: '04/15', rank: '2', people: '345' }, { img: '6-randomTest-random-1.png', date: '02/12', rank: '53', people: '134' }] }
        ];
        var list2 = [
            {
                "Account": "jy9999",
                "Function": "randomTest",
                "ImgType": "random ",
                "ImgId": "1",
                "StartDate": "2017-04-10T00:00:00",
                "HeartNumbers": 87,
                "Rank": 1,
                "ImgName": "jy9999-randomTest-random-1"
            }
        ]
        var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        var self = this;
        var content = this.props.historyRank.map(function (data, index) {
            console.log(data.data);
            var dataSource = ds.cloneWithRows(data.data)
            return (
                <View key={index} style={styles.slide1} >
                    <View style={styles.content}>
                        <View style={styles.titleListDate}>
                            <Text style={styles.listTitleText}>
                                圖片
                            </Text>
                        </View>
                        <View style={styles.titleListNum}>
                            <Text style={styles.listTitleText}>
                                日期
                                </Text>
                        </View>
                        <View style={styles.titleListNum}>
                            <Text style={styles.listTitleText}>
                                名次/人數
                                </Text>
                        </View>
                    </View>
                    {
                        data.data.length !== 0 ?
                            <View style={{height:height*0.7}}>
                                <ListView
                                    dataSource={dataSource}
                                    renderRow={self._renderRow}
                                    style={{ flexDirection: 'column', }}
                                />
                            </View> :
                            <View style={{alignItems:'center',justifyContent:'center',marginTop:20}}>
                            <Text style={{fontSize:20,backgroundColor:'rgba(0,0,0,0)'}}>暫無紀錄</Text>
                            </View>

                    }
                </View>);
        })
        if (!this.state.loaded)
            return (
                <ActivityIndicator
                    animating={this.state.animating}
                    style={[styles.centering, { height: 80 }]}
                    size="large"
                />
            );
        return (
            <View style={styles.bigview}>
                <ImagePreview
                    modalVisible={this.state.modalVisible}
                    imageName={this.state.imageName}
                    onClick={(option) => {
                        this.setState({ modalVisible: option })
                    }}
                />
                <View style={styles.toparea}>
                    <SegmentedControls
                        tint={'rgba(52, 52, 52, 0.8)'}
                        selectedTint={'#FFFFFF'}
                        selectedBackgroundColor={'rgba(52, 52, 52, 0.9)'}
                        backTint={'rgba(52, 52, 52, 0)'}
                        options={options}
                        allowFontScaling={false} // default: true
                        onSelection={setSelectedOption.bind(this)}
                        selectedOption={this.state.selectedOption}
                        optionStyles={{ fontFamily: 'AvenirNext-Medium' }}
                        optionContainerStyle={{ flex: 1 }}
                    />
                </View>
                {
                    //list
                }

                <Carousel
                    ref={(carousel) => { this._carousel = carousel; }}
                    sliderWidth={width}
                    itemWidth={width}
                    directionalLockEnabled={true}
                    centerContent={true}
                    // horizontal={true}
                    currentIndex={this.state.currentIndex}
                    onSnapToItem={(item) => {
                        if (item === 0) {
                            console.log('0')
                            this.setState({
                                selectedOption: '隨機',
                                slideIndex: 0
                            });
                        } else if (item === 1) {
                            console.log('1')
                            this.setState({
                                selectedOption: '零死角',
                                slideIndex: 1
                            });
                        } else if (item === 2) {
                            console.log('2')
                            this.setState({
                                selectedOption: '背影',
                                slideIndex: 2
                            });
                        }
                    }}>
                    {content}
                </Carousel>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    toparea: {
        marginTop: height * 0.04,
        marginBottom: height * 0.01,
        width: width * 0.85,
        height: height * 0.07
    },
    bigview: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',

    },
    square: {
        height: 90,
        width: 90,
        borderWidth: 3,
        borderColor: 'rgba(52, 52, 52, 0.8)',
    },
    content: {
        flexDirection: 'row',
        paddingLeft: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        paddingBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',

    },
    slide1: {
        // justifyContent: 'center',
        paddingLeft: width * 0.1,
        paddingRight: width * 0.1,

        width: width,
        height: 600,
        // alignItems: 'center',
    },
    titleListDate: {
        width: width * 0.25,
    },
    titleListNum: {
        width: width * 0.25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    listView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 13,
        paddingBottom: 13,
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        justifyContent: 'center',
    },
    listDate: {
        width: width * 0.25,
        color: '#383838',
        fontSize: 18,
        backgroundColor: 'rgba(0,0,0,0)',
    },
    imageView: {
        width: width * 0.25,
    },
    listNum: {
        width: width * 0.25,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    listRight: {
        justifyContent: 'flex-end',
        color: '#383838',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    numText: {
        fontSize: 18,
        color: '#383838',
        backgroundColor: 'rgba(0,0,0,0)',
        fontFamily: 'Euphemia UCAS',
    },
    listTitleText: {
        fontSize: 18,
        color: '#383838',
        fontFamily: 'Euphemia UCAS',
        backgroundColor: 'rgba(0,0,0,0)'
    }
});

export default HistoryRank;

