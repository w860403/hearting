import React, { Component, PropTypes } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Button,
    Dimensions,
    NavigatorIOS,
    AsyncStorage,
    AlertIOS,
    ActivityIndicator,
    Keyboard,
    TouchableWithoutFeedback
} from 'react-native';
var { height, width } = Dimensions.get('window');

class LoveLetterDisplay extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     loveLetterContent: ''
        // }
    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.bigview}>
                    <Text style={styles.titleText}>
                        你收到的信件
                    </Text>
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            {this.props.loveLetterContent}
                        </Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}
const styles = StyleSheet.create({
    titleText: {
        fontSize: 18,
        margin: 20,
        fontFamily: 'Euphemia UCAS',
        backgroundColor: 'rgba(0,0,0,0)',
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    bigview: {
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    textView: {
        width: width * 0.8,
        height: height * 0.5,
        borderColor: 'black',
        justifyContent: 'center',
        backgroundColor: (52, 52, 52, 0),
        alignItems: 'center',
        borderWidth: 1,
        paddingLeft: 5,
    },
    text: {
        fontFamily: 'Euphemia UCAS',
        fontSize: 20
    },
    space: {
        height: 20,
    },
    button: {
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'black',
        marginTop: 10,
    },
    error_text: {
        color: '#FF3333'
    }
});
export default LoveLetterDisplay;
