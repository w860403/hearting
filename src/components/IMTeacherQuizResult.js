import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  Button,
  Image,
  ActivityIndicator,
  AlertIOS,
  ListView
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
var { height, width } = Dimensions.get('window');
import CharmQuestionModal from './charmquestionmodal'
class IMTeacherQuizResult extends Component {
  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    // console.log('on quizresult component')
    // console.log(this.props.quizResultData)
    // console.log(this.props.quizResultData.PersonList)
    this.state = {
      ds: ds,
      dataSource: ds.cloneWithRows(this.props.TeacherQuizResultData),
      personList: this.props.TeacherQuizResultData,
      sex: this.props.sex,
      charmList: [],
      imageUrl: 'http://114.35.74.209/Hearting/img/',
      modalVisible: true
    };
  }
  // componentWillMount() {
  //   var self = this
  //   this.props.quizResultData.PersonList.map(function (data) {
  //     if (data.Score > 70) {
  //       var object = { Image: data.Image, Account: data.Account, Score: data.Score, Style: null }
  //       self.state.charmList.push(object)
  //     }
  //     console.log('charmList:');
  //     console.log(self.state.charmList);
  //   })
  // }
  setModalVisible(bool) {
    this.setState({
      modalVisible: bool
    })
  }
  render() {
    return (
      <View style={styles.bigView} >
        {
          // <CharmQuestionModal
          //   modalVisible={this.state.modalVisible}
          //   charmList={this.state.charmList}
          //   sex={this.state.sex}
          //   setModalVisible={(bool) => {
          //     this.setModalVisible(bool)
          //   }}
          //   saveAsCharm={(charmList, sex) => {
          //     this.props.saveAsCharm(charmList, sex)
          //   }}
          // />
        }

        <View style={styles.titleView}>
          <View style={styles.titleLeftView}>
          </View>
          <View style={styles.titleMiddleView}>
            <Text style={styles.titleText}>心動指數</Text></View>
        </View>
        <View style={styles.listView}>
          <ListView
            dataSource={this.state.dataSource}
            renderRow={(rowData, sectionID, rowID) =>
              <View style={styles.listViewContent}>
                <View style={styles.listViewContentLeftView}>
                  <Image source={{ uri: this.state.imageUrl + rowData.Image }} style={styles.image} />
                </View>
                <View style={styles.listViewContentMiddleView}>
                  <Image source={require('../images/Heart.png')} style={styles.HeartImage}></Image>
                  <Text style={styles.pointText}>{rowData.Point}</Text>
                </View>
              </View>
            }
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  HeartImage: {
    width: 30,
    height: 30,
    marginRight:5
  },
  bigView: {
    alignItems: 'center',
    flex: 1,
  },
  titleView: {
    marginTop: 30,
    flexDirection: 'row',
    width: width * 0.95,
    height: 30,
  },
  titleLeftView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  titleMiddleView: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
  },
  titleMiddleView1: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleMiddleView2: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleRightView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleText: {
    fontSize: 18,
    backgroundColor: 'rgba(0,0,0,0)'
  },
  listView: {
    flex: 1,
    marginTop: 5,
    width: width * 0.95,
  },
  listViewContent: {
    flexDirection: 'row',
    width: width * 0.95,
    height: 120,
    borderColor: 'rgba(52,52,52,0.5)',
    borderTopWidth: 1,
    alignItems: 'center',
  },
  listViewContentLeftView: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 10,
    flex: 1
  },
  listViewContentMiddleView: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  listViewContentMiddleView1: {
    alignItems: 'center',
    flex: 1
  },
  listViewContentMiddleView2: {
    alignItems: 'center',
    flex: 1
  },
  listViewContentRightView: {
    alignItems: 'center',
    flex: 1
  },
  image: {
    borderColor: 'rgba(52,52,52,0.5)',
    backgroundColor:'white',
    borderWidth: 1,
    width: 100,
    height: 100,
    resizeMode: Image.resizeMode.contain,
  },
  pointText: {
    fontSize: 30,
    color: '#E86868',
    marginLeft:5,
    backgroundColor: 'rgba(52,52,52,0)',
  },
  button: {
    backgroundColor: 'rgba(52,52,52,0.6)',
    width: 60,
    height: 30,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addFriendText: {
    color: 'white',
    fontSize: 15
  },
  E_MsgText: {
    backgroundColor: 'rgba(52,52,52,0)',
    fontSize: 15
  },
});

export default IMTeacherQuizResult;

