import React, { Component, PropTypes } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableHighlight,
    Dimensions,
    Button,
    Image,
    ActivityIndicator,
    TouchableWithoutFeedback
} from 'react-native';
import TopBar from './topbar'
var { height, width } = Dimensions.get('window');

class LiveActionQuiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 2,
            point: this.props.point ? this.props.point : 0,
            poorSignal: this.props.poorSignal ? this.props.poorSignal : 0,
            timeCounterMinus: this.props.timeCounterMinus ? this.props.timeCounterMinus : 15,
            titleText: this.props.titleText ? this.props.titleText : '真人測驗'
        };
    }
    render() {
        const pointstandard = 50
        const point = this.state.point
        const poorSignal = this.state.poorSignal
        return (
            <View style={styles.bigView}>
                <View style={styles.titleView}>
                    <Text style={styles.titleText}>
                        {this.state.titleText}
                    </Text>
                </View>
                <View style={styles.topView}>
                    <Text style={styles.topText}>
                        心動指數：
                    </Text>
                </View>
                <View style={styles.secondView}>
                    <Text style={styles.pointText}>{poorSignal != 0 ? '??' : point} 分</Text>
                </View>
                <View style={styles.thirdView}>
                    <View style={styles.messageView}>
                        {poorSignal != 0 ? <Text style={styles.messageText}>
                            訊號不正常
                            </Text> :
                            <Text style={styles.messageText}>
                                {point < pointstandard ? '猴!' : '哇！'}
                            </Text>
                        }
                        {poorSignal != 0 ?
                            <Text style={styles.messageText}>
                                請調整腦波耳機
                            </Text> :
                            <Text style={styles.messageText}>
                                {point < pointstandard ? '你有一點心動了唷' : '心動指數要爆表了'}
                            </Text>
                        }
                    </View>
                </View>
                <View style={styles.bottomView}>
                    <Text style={styles.timeCounterText}>剩餘</Text>
                    <Text style={styles.timeCounterBigText}>{this.state.timeCounterMinus}</Text>
                    <Text style={styles.timeCounterText}>秒</Text>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
    normalBackGroundView: {
        width: null,
        height: null,
        flex: 1,
    },
    blackBackGroundView: {
        backgroundColor: 'black',
        width: null,
        height: null,
        flex: 1,
    },
    titleView: {
        width: width,
        alignItems: 'center',
        marginTop: height * 0.1,
        backgroundColor: 'rgba(52, 52, 52, 0)',
    },
    bigView: {
        justifyContent: 'flex-start',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    bigViewBlack: {
        backgroundColor: '#000000',
        justifyContent: 'flex-start',
        flex: 1,
        alignItems: 'center',
        width: width,
    },
    textview: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topView: {
        width: width,
        height: 50,
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(52, 52, 52, 0)',
        marginLeft: 20,
        marginTop: height * 0.05
    },
    secondView: {
    },
    thirdView: {
        justifyContent: 'center',
        alignItems: 'center',
        height: height * 0.3,
    },
    bottomView: {
        height: height * 0.08,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'rgba(52, 52, 52, 0)',
    },
    pointText: {
        fontSize: 90,
        backgroundColor: 'rgba(52, 52, 52, 0)',
        color: '#E86868',
        fontFamily: 'Euphemia UCAS',
    },
    button: {
        backgroundColor: 'rgba(52,52,52,0.6)',
        width: 100,
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 35
    },
    topText: {
        fontSize: 25
    },
    messageView: {
        backgroundColor: 'rgba(52, 52, 52, 0.7)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageViewHide: {
        backgroundColor: 'rgba(52, 52, 52, 0)',
        width: width * 0.7,
        height: height * 0.2,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    messageText: {
        fontSize: 30,
        color: 'white'
    },
    timeCounterText: {
        fontSize: 30
    },
    timeCounterBigText: {
        fontSize: 50
    }
});

export default LiveActionQuiz;
