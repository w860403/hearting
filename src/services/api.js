const get_base_url = () => {
    return 'http://114.35.74.209/Hearting';
};

//--註冊相關--
//確認帳號是否重複（POST）
export function fetch_check_account(account) {
    var self = this;
    const api_url = `${get_base_url()}/api/Memberapi/QuOther`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            apiname: 'CheckAccount',
            postdata: { Account: account }
        })
    }).then(response => {
        return response.json()
    });
}

//上傳圖片
export function fetch_upload_image(account, fileStr) {
    const api_url = `${get_base_url()}/api/Memberapi/PostImage`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Function: 'Member',
            ImgType: 'Member',
            FileStr: fileStr
        })
    }).then(response => {
        return response.json()
    });
}
//註冊會員
export function fetch_register_member(memberData) {
    const api_url = `${get_base_url()}/api/Memberapi/Create`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(memberData)
    }).then(response => {
        return response.json()
    });
}

//--會員相關--
//登入（POST）
export function fetch_login(account, password) {
    const api_url = `${get_base_url()}/api/Memberapi/Read`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "Account": account,
            "Password": password
        })
    }).then(res => {
        return res.json()
    });
}
//取得會員基本資料
export function fetch_get_member_data(account) {
    const api_url = `${get_base_url()}/api/Memberapi/GetUser?Account=` + account;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        return response.json()
    });
}
//取得會員喜好
export function fetch_get_member_info(account) {
    const api_url = `${get_base_url()}/api/Memberapi/GetUserInfo?Account=` + account;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        return response.json()
    });
}
//修改會員資料
export function fetch_edit_member_data(data) {
    const api_url = `${get_base_url()}/api/Memberapi/Update`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            master: data

        })
    }).then(response => {
        return response.json()
    });
}
//修改會員喜好
export function fetch_edit_member_info(data) {
    const api_url = `${get_base_url()}/api/Memberapi/Update`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            detail: data

        })
    }).then(response => {
        return response.json()
    });
}
//取得會員歷史上榜記錄
export function fetch_get_history_rank(account) {
    const api_url = `${get_base_url()}/api/HistoryApi/GetRankHistory?account=` + account;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
//上傳測驗照片
export function fetch_upload_test_image(account, fun, imageType, fileStr) {
    const api_url = `${get_base_url()}/api/TestApi/PostImage`;
    return fetch(api_url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Function: fun,
            ImgType: imageType,
            Filestr: fileStr

        })
    }).then(response => {
        return response.json()
    });
}
//取得目前已上傳的測驗照片
export function fetch_get_now_image(account, fun) {
    const api_url = `${get_base_url()}/api/TestApi/GetNowImg?account=` + account + '&function=' + fun;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
//取得隨機、背影、三角度的相簿照片
export function fetch_get_member_photo(account, type) {
    const api_url = `${get_base_url()}/api/TestApi/GetAlbum?account=` + account + '&function=' + type;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
export function fetch_get_history_quiz(account, type) {
    const api_url = `${get_base_url()}/api/HistoryApi/GetTestMHistory?account=` + account + '&function=' + type;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
export function fetch_get_history_quiz_detail(account, fun, date) {
    const api_url = `${get_base_url()}/api/HistoryApi/GetTestDHistory?account=` + account + '&function=' + fun + '&date=' + date;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
//首頁
export function fetch_get_rank(sex) {
    const api_url = `${get_base_url()}/api/RankApi/GetLastWeekRank?sex=` + sex;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}


//--測驗相關api--
//取得測驗圖片（GET）
export function fetch_get_image(account, functionName) {
    const api_url = `${get_base_url()}/api/TestApi/Read?account=` + account + '&function=' + functionName;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//取得校園心動人氣王照片(GET)
export function fetch_get_student_image(sex) {
    const api_url = `${get_base_url()}/api/PlayApi/GetPlayPhoto?sex=`+sex;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//fetch_get_teacher_image
//取得資管七王子照片(GET)
export function fetch_get_teacher_image() {
    const api_url = `${get_base_url()}/api/PlayApi/GetTeacherPhoto?`;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//取得心動分數男（POST）
export function fetch_get_quiz_point(mindwaveData) {
    const api_url = `${get_base_url()}/api/TestApi/IOSGetTestPoint`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(mindwaveData)
    }).then(res => {
        return res.json()
    });
}
//取得心動分數女（POST）
export function fetch_get_quiz_pointw(mindwaveData) {
    const api_url = `${get_base_url()}/api/TestApi/IOSGetTestPointW`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(mindwaveData)
    }).then(res => {
        return res.json()
    });
}
//存入歷史資料
export function fetch_save_quiz_history(imageArray, quizPointArray, account) {
    const api_url = `${get_base_url()}/api/TestApi/SaveAsHistory`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            MemberList: imageArray,
            PointList: quizPointArray,
            Account: account
        })
    }).then(res => {
        return res.json()
    });
}
//校園心動人氣王存入歷史資料
//fetch_save_student_quiz_history
export function fetch_save_student_quiz_history(imageArray, quizPointArray) {
    const api_url = `${get_base_url()}/api/PlayApi/SavePlay`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            PlayList: imageArray,
            PointList: quizPointArray
        })
    }).then(res => {
        return res.json()
    });
}
//資管七王子(GET)
//fetch_save_teacher_quiz_history
export function fetch_save_teacher_quiz_history(imageArray, quizPointArray) {
    const api_url = `${get_base_url()}/api/PlayApi/SaveTeacherPlay`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            PlayList: imageArray,
            PointList: quizPointArray
        })
    }).then(res => {
        return res.json()
    });
}
//送出吸引項目
//fetch_save_as_charm
export function fetch_save_as_charm(charmList,sex) {
    const api_url = `${get_base_url()}/api/TestApi/SaveAsCharm`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            CharmList: charmList,
            Sex: sex, 
        })
    }).then(res => {
        return res.json()
    });
}
//送出交友邀請（POST）
//114.35.74.209/Hearting/api/FrendApi/SendInvitation
export function fetch_send_invitation(account, partner) {
    const api_url = `${get_base_url()}/api/FriendApi/SendInvitation`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Partner: partner
        })
    }).then(res => {
        return res.json()
    });
}

//--好友相關api--
//取得好友列表（GET）
// 114.35.74.209/Hearting/api/FriendApi/GetFriendList?account=
export function fetch_get_friend_list(account) {
    const api_url = `${get_base_url()}/api/FriendApi/GetFriendList?account=` + account;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//取得好友邀請列表（GET）
export function fetch_get_friend_request(account) {
    const api_url = `${get_base_url()}/api/FriendApi/GetInvation?account=` + account;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//回應好友邀請
//fetch_reply_invation(account,partner,isaccept)
export function fetch_reply_invation(account, partner, isaccept) {
    const api_url = `${get_base_url()}/api/FriendApi/ReplyInvation`;
    console.log({ account: account, partner: partner, isaccept: isaccept });
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Partner: partner,
            isAccept: isaccept
        })
    }).then(res => {
        return res.json()
    });
}
//刪除好友（GET）
export function fetch_delete_friend(account, partner) {
    const api_url = `${get_base_url()}/api/FriendApi/RemoveFriend?account=` + account + '&partner=' + partner;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//寄出告白信
//114.35.74.209/Hearting/api/FriendApi/SendLoveLetter
export function fetch_sent_love_letter(account, partner, content) {
    const api_url = `${get_base_url()}/api/FriendApi/SendLoveLetter`;
    console.log({ account: account, partner: partner, content: content });
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Partner: partner,
            Content: content
        })
    }).then(res => {
        return res.json()
    });
}
//取得告白信狀態
//fetch_get_love_letter_status
export function fetch_get_love_letter_status(account) {
    const api_url = `${get_base_url()}/api/FriendApi/LoveLetterStatus?account=` + account;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//取消告白信
//fetch_cancel_love_letter
export function fetch_cancel_love_letter(account) {
    const api_url = `${get_base_url()}/api/FriendApi/CancelLetter?account=` + account;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
//取得對方告白信內容
//fetch_get_love_letter_content
export function fetch_get_love_letter_content(account) {
    const api_url = `${get_base_url()}/api/FriendApi/GetLoveLetter?account=` + account;
    // TODO deal with json decode error situation
    return fetch(api_url).then(res => {
        return res.json()
    });
}
export function fetch_get_chat(account, partner) {
    const api_url = `${get_base_url()}/api/FriendApi/GetPersonChat`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Partner: partner,
        })
    }).then(res => {
        return res.json()
    });
}
export function fetch_post_message(account, partner, content) {
    const api_url = `${get_base_url()}/api/FriendApi/PutMessage`;
    // TODO deal with json decode error situation
    return fetch(api_url, {
        method: 'POST',
        headers: {
            //'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            Account: account,
            Partner: partner,
            Content: content
        })
    }).then(res => {
        return res.json()
    });
}
export function fetch_get_chat_list(account) {
    const api_url = `${get_base_url()}/api/FriendApi/GetChatList?account=` + account ;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
export function fetch_get_constellation(sex,constellation,type) {
    const api_url = `${get_base_url()}/api/AnalysisApi/DataAnalysis?sex=`+sex+'&constellation='+constellation+'&type='+type+'&mystyle=' ;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}
export function fetch_get_personal(account) {
    const api_url = `${get_base_url()}/api/TestApi/GetCharm?account=` + account ;
    return fetch(api_url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    }).then(response => {
        return response.json()
    });
}