//SAVE_CHARM_SUCCESS
//SAVE_CHARM_FAIL
export default (state = false, action) => {
    switch (action.type) {
        case 'SAVE_CHARM_SUCCESS':
            return action.status;
        case 'SAVE_CHARM_FAIL':
            return action.status
        default:
            return state;
    }
};
