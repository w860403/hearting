export default (state = null, action) => {
    switch (action.type) {
        case 'GET_IMAGE_SUCCESS':
            console.log('GET_IMAGE_SUCCESS')
            return action.data;
        default:
            return state;
    }
};
