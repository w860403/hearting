export default (state = null, action) => {
    switch (action.type) {
        case 'MEMBER_INFO':
            return  action.member_info;
        default:
            return state;
    }
};