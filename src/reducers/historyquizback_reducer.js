export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'HISTORY_QUIZ_BACK':
            return  action.history_quiz_back;
        default:
            return state;
    }
};