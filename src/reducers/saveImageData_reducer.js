export default (state = {imageType:null}, action) => {
    switch (action.type) {
        case 'SAVE_IMAGE_TYPE':
            return {
                imageType: action.image,
            };
        default:
            return state;
    }
};