//SAVE_STUDENT_QUIZ_HISTORY_SUCCESS
export default (state = false, action) => {
    switch (action.type) {
        case 'SAVE_STUDENT_QUIZ_HISTORY_SUCCESS':
            return action.data;
        default:
            return state;
    }
};
