export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'MEMBER_PHOTO_BACK':
            return action.member_photo_back
        default:
            return state;
    }
};