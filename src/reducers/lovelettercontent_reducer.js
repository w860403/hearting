//GET_LOVE_LETTER_CONTENT_SUCCESS
export default (state = null,action) => {
    switch (action.type) {
        case 'GET_LOVE_LETTER_CONTENT_SUCCESS':
            return action.content;
        default:
            return state;
    }
};
