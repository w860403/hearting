//GET_FRIEND_REQUEST_SUCCESS
export default (state = [{ Partner: null, Name: null, Image: null, Com_txt: null }], action) => {
    switch (action.type) {
        case 'GET_FRIEND_REQUEST_SUCCESS':
            return action.friendsRequestData;
        default:
            return state;
    }
};
