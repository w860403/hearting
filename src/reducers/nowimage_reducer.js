export default (state = [], action) => {
    switch (action.type) {
        case 'MEMBER_NOW_IMAGE':
            return  action.member_now_image;
        default:
            return state;
    }
};