
export default (state =  null , action) => {
    switch (action.type) {
        case 'GET_CHAT_LIST':
            return action.chatlist;
        default:
            return state;
    }
};
