export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'MEMBER_PHOTO_ANGLE':
            return  action.member_photo_angle;
        default:
            return state;
    }
};