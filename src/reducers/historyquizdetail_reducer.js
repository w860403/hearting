export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'HISTORY_QUIZ_DETAIL':
            return  action.history_quiz_detail;
        default:
            return state;
    }
};