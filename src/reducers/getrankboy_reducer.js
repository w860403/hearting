export default (state =  null , action) => {
    switch (action.type) {
        case 'RANK_BOY':
            return action.rank_boy;
        default:
            return state;
    }
};
