//LovaLetterStatusData
export default (state = { Partner: null, Status: null }, action) => {
    switch (action.type) {
        case 'GET_LOVE_LETTER_STATUS_SUCCESS':
            return action.LovaLetterStatusData;
        default:
            return state;
    }
};
