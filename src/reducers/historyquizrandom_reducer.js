export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'HISTORY_QUIZ_RANDOM':
            return  action.history_quiz_random;
        default:
            return state;
    }
};