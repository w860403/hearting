import { combineReducers } from 'redux';
import login_reducer from './login_reducer';
import login_data_reducer from './login_data_reducer';
import loginsex_reducer from './loginsex_reducer'
import quiz_reducer from './quiz_reducer'
import eegpowerdelta_reducer from './eegpowerdelta_reducer'
import eegpowerlowbeta_reducer from './eegpowerlowbeta_reducer'
import esense_reducer from './esense_reducer'
import register_reducer from './register_reducer'
import uploadimage_reducer from './uploadimage_reducer'
import heart_reducer from './heart_reducer'
import saveImageData_reducer from './saveImageData_reducer'
import memberdata_reducer from './memberdata_reducer'
import memberinfo_reducer from './memberinfo_reducer'
import historyrank_reducer from './historyrank_reducer'
import nowimage_reducer from './nowimage_reducer'
import quizpointarray_reducer from './quizpointarray_reducer'
import quizpoint_reducer from './quizpoint_reducer'
import friendsdata_reducer from './friendsdata_reducer'
import friendsrequestdata_reducer from './friendsrequestdata_reducer'
import quizresultdata_reducer from './quizresultdata_reducer'
import memberphotorandom_reducer from './memberphotorandom_reducer'
import loveletterstatusdata_reducer from './loveletterstatusdata_reducer'
import memberphotoback_reducer from './memberphotoback_reducer'
import memberphotoangle_reducer from './memberphotoangle_reducer'
import historyquizrandom_reducer from './historyquizrandom_reducer'
import historyquizback_reducer from './historyquizback_reducer'
import historyquizangle_reducer from './historyquizangle_reducer'
import historyquizdetail_reducer from './historyquizdetail_reducer'
import getrankgirl_reducer from './getrankgirl_reducer'
import getrankboy_reducer from './getrankboy_reducer'
import cancellovelettermessage_reducer from './cancellovelettermessage_reducer'
import lovelettercontent_reducer from './lovelettercontent_reducer'
import chat_reducer from './chat_reducer'
import chatlist_reducer from './chatlist_reducer'
import savecharmstatus_reducer from './savecharmstatus_reducer'
import getanalysis_reducer from './getanalysis_reducer'
import StudentImageData_Reducer from './StudentImageData_Reducer'
import StudentQuizResultData_Reducer from './StudentQuizResultData_Reducer'
import TeacherImageData_Reducer from './TeacherImageData_Reducer'
import TeacherQuizResultData_Reducer from './TeacherQuizResultData_Reducer'
const root_reducers = combineReducers({
  //登入後所有頁面狀態
  heart: heart_reducer,
  save_image_data: saveImageData_reducer,
  rank_girl: getrankgirl_reducer,
  rank_boy: getrankboy_reducer,
  analysis: getanalysis_reducer,
  //register
  register: register_reducer,
  upload_image: uploadimage_reducer,
  //login
  login_status: login_reducer,
  login_data: login_data_reducer,
  login_sex: loginsex_reducer,
  //member
  member_data: memberdata_reducer,
  member_info: memberinfo_reducer,
  history_rank: historyrank_reducer,
  now_image: nowimage_reducer,
  member_photo_random: memberphotorandom_reducer,
  member_photo_back: memberphotoback_reducer,
  member_photo_angle: memberphotoangle_reducer,
  history_quiz_random: historyquizrandom_reducer,
  history_quiz_back: historyquizback_reducer,
  history_quiz_angle: historyquizangle_reducer,
  history_quiz_detail: historyquizdetail_reducer,
  image_data_array: quiz_reducer,


  eeg_power_delta: eegpowerdelta_reducer,
  eeg_power_low_beta: eegpowerlowbeta_reducer,
  esense: esense_reducer,
  quiz_point_array: quizpointarray_reducer,
  quiz_point: quizpoint_reducer,
  quiz_result_data: quizresultdata_reducer,
  friends_data: friendsdata_reducer,
  friends_request_data: friendsrequestdata_reducer,
  love_letter_status_data: loveletterstatusdata_reducer,
  cancel_love_letter_message: cancellovelettermessage_reducer,
  love_letter_content: lovelettercontent_reducer,
  chat: chat_reducer,
  chat_list: chatlist_reducer,
  save_charm_status: savecharmstatus_reducer,
  student_image_data_array: StudentImageData_Reducer,
  student_quiz_resultdata: StudentQuizResultData_Reducer,
  teacher_image_data_array: TeacherImageData_Reducer,
  teacher_quiz_resultdata: TeacherQuizResultData_Reducer
});

export default root_reducers;
