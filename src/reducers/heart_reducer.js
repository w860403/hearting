export default (state = { component: 'home' }, action) => {
    switch (action.type) {
        case 'HOME':
            return {
                component: 'home',
            };
        case 'PERSONAL':
            return {
                component: 'personal',
            };
        case 'QUIZ':
            return {
                component: 'quiz',
            };
        case 'FRIEND':
            return {
                component: 'friend',
            };
        case 'MEMBER':
            return {
                component: 'member',
            };
        default:
            return state;
    }
};
