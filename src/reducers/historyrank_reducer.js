export default (state = [], action) => {
    switch (action.type) {
        case 'HISTORY_RANK':
            return action.history_rank
        default:
            return state;
    }
};
