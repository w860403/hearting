
export default (state = null, action) => {

    switch (action.type) {
        case 'CANCEL_LOVE_LETTER_FAIL':
            console.log(action.Message)
            action.Callback(action.Message)
            return action.Message;
        default:
            return state;
    }
};
