//GET_FRIEND_LIST_SUCCESS
export default (state = [{ Account: null, Date: null, MatchPercent: null, Partner: null }], action) => {
    switch (action.type) {
        case 'GET_FRIEND_LIST_SUCCESS':
            return action.friendsData;
        default:
            return state;
    }
};
