//SAVE_QUIZ_HISTORY_SUCCESS
export default (state = {
    PersonList: [{ Account: null, Status: null, E_Msg: null, MatchPercent: null, Score: null }],
    RecommendList: [{ Account: null, ImgId: null, Function: null, ImgName: null, ImgType: null }]
}, action) => {
    switch (action.type) {
        case 'SAVE_QUIZ_HISTORY_SUCCESS':
            console.log('reducer quizResultData =');
            console.log(action.resultData);
            return action.resultData;
        default:
            return state;
    }
};
