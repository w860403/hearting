//GET_STUDENT_IMAGE_SUCCESS
export default (state = null, action) => {
    switch (action.type) {
        case 'GET_STUDENT_IMAGE_SUCCESS':
            console.log('GET_STUDENT_IMAGE_SUCCESS')
            console.log(action.data);
            return action.data;
        default:
            return state;
    }
};
