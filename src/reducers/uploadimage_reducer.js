export default (state = {status:false}, action) => {
    switch (action.type) {
        case 'UPLOAD_IMAGE':
            return {
                status: action.image,
            };
        default:
            return state;
    }
};
