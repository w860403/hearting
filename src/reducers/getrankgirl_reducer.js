export default (state =  null , action) => {
    switch (action.type) {
        case 'RANK_GIRL':
            return action.rank_girl;
        default:
            return state;
    }
};
