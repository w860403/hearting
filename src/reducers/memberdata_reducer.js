export default (state = null, action) => {
    switch (action.type) {
        case 'MEMBER_DATA':
            return  action.member_data;
        default:
            return state;
    }
};