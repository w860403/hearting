export default (state =  null , action) => {
    switch (action.type) {
        case 'GET_CHAT':
            return action.chat;
        default:
            return state;
    }
};
