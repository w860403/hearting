export default (
    state = null,
    action) => {
    switch (action.type) {
        case 'HISTORY_QUIZ_ANGLE':
            return  action.history_quiz_angle;
        default:
            return state;
    }
};