export default (state = {status:false}, action) => {
    switch (action.type) {
        case 'ACCOUNT_REPEAT':
            return {
                status: action.status,
            };
        default:
            return state;
    }
};
