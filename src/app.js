import React from 'react';
import { Platform } from 'react-native';
import { Router } from 'react-native-router-flux';
import {Provider} from 'react-redux'
import scenes from './scenes';
import store from './store'

// Copy from react-native-router-flux example
const getSceneStyle = (/* NavigationSceneRendererProps */ props, computedProps) => {
  const style = {
    flex: 1,
    backgroundColor: '#fff',
    shadowColor: null,
    shadowOffset: null,
    shadowOpacity: null,
    shadowRadius: null,
  };
  computedProps.hideNavBar=true;
  if (computedProps.isActive) {
    style.marginTop = computedProps.hideNavBar ? 0 : Platform.select({ios: 64, android: 54});
  }
  return style;
};

const App = () => {
  return (
    <Provider store={store}>
        <Router scenes={scenes} getSceneStyle={getSceneStyle} showNavigationBar={false} />
    </Provider>
  );
};

export default App;