import IMTeacherQuizResult from '../containers/IMTeacherQuizResult';
import TopBar from '../containers/topbar'
import React, { Component, PropTypes } from 'react';
//import TopBar from '../components/topbar'
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
const IMTeacherQuizResultScene = (data) => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='測驗結果' lefttext='Back' />
        <IMTeacherQuizResult TeacherQuizResultData={data.TeacherQuizResultData} sex={data.sex} />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default IMTeacherQuizResultScene;
