import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native';
import IMTeacherQuiz from '../containers/IMTeacherQuiz';


const IMTeacherQuizScene = () => {
    return (
        <View style={styles.container}>
            <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                <IMTeacherQuiz />
            </Image>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent:'center',
        alignItems: 'flex-start',
        flexDirection: 'row'
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
})
export default IMTeacherQuizScene;
