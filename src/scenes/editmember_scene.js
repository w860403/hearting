import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import EditMember from '../containers/editmember';
import TopBar from '../containers/topbar'

const EditMemberScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='修改會員資料' lefttext='Back' />
        <EditMember />
      </Image>
    </View>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default EditMemberScene;
