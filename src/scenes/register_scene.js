import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import TopBar from '../containers/topbar'
import Register from '../containers/register';

const RegisterScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='註冊' lefttext='Back' />
        <Register />
      </Image>
    </View>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default RegisterScene;
