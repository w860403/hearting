import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native';
import LoveLetterInput from '../containers/loveletterinput';
import TopBar from '../containers/topbar'

const LoveLetterInputScene = (Data) => {
    return (
        <View style={styles.container}>
            <Image source={require('../images/home.png')} style={styles.backgroundImage}>
                <TopBar title='告白信' lefttext='Back' />
                <LoveLetterInput account={Data.account} partner={Data.partner} />
            </Image>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
    },
})
export default LoveLetterInputScene;
