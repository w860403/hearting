import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Chat from '../containers/chat';
import TopBar from '../containers/topbar'
var { height, width } = Dimensions.get('window');

const ChatScene = (data) => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <Image
          source={require('../images/chat.png')}
          style={{backgroundColor:'white', zIndex: 100, height: height * 0.11, width: width ,paddingBottom:15}}
        >
          <TopBar title={data.name} lefttext='Back' />
        </Image>
        <Chat partner={data.partner} />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default ChatScene;
