import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Heart from '../containers/heart';

const HeartScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home2.png')} style={styles.backgroundImage}>
        <Heart />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default HeartScene;
