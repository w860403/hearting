
import UploadPhoto from '../containers/uploadphoto';
import TopBar from '../containers/topbar'

import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
const UploadPhotoScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='' lefttext='Back' />
        <UploadPhoto />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default UploadPhotoScene;
