import React from 'react';
import { Actions, Scene } from 'react-native-router-flux';
import Home from './home_scene';
import Login from './login_scene';
import Register from './register_scene';
import EditMember from './editmember_scene';
import Member from './member_scene';
import QuizList from './quizlist_scene';
import QuizResult from './quizresult_scene';
import UploadPhoto from './uploadphoto_scene';
import QuizResultDetail from './quizresultdetail_scene';
import LiveActionQuiz from './liveactionquiz_scene';
import WifeControl from './wifecontrol_scene';
import Friends from './friends_scene';
import FriendsRequest from './friendsrequest_scene'
import HistoryQuiz from './historyquiz_scene';
import HistoryQuizDetail from './historyquizdetail_scene';
import HistoryRank from './historyrank_scene';
import Photo from './photo_scene';
import Rank from './rank_scene';
import RankDetail from './rankdetail_scene';
import Doomed from './doomed_scene';
import DoomedResult from './doomedresult_scene';
import Chat from './chat_scene';
import LiveActionQuizResult from './liveactionquizresult_scene'
import Heart from './heart_scene'
import LoveLetterInput from './loveletterinput_scene'
import LoveLetterDispley from './loveletterdisplay_scene'
import ImageQuiz from './imagequiz_scene'
import EditPassword from './editpassword_scene'
import signAnalysis from './signAnalysis_scene'
import NUTCStudentQuiz from './NUTCStudentQuiz_Scene'
import NUTCStudentQuizResult from './NUTCStudentQuizResult_Scene'
import IMTeacherQuiz from './IMTeacherQuiz_Scene'
import IMTeacherQuizResult from './IMTeacherQuizResult_Scene'


const scenes = Actions.create(
    <Scene key="root">
        <Scene key="heart" component={Heart} hideNavBar={true} />
        <Scene key="imteacherquiz" component={IMTeacherQuiz} hideNavBar={true} />
        <Scene key="imteacherquizresult" component={IMTeacherQuizResult} hideNavBar={true} />
        <Scene key="nutcstudentquiz" component={NUTCStudentQuiz} hideNavBar={true} />
        <Scene key="nutcstudentquizresult" component={NUTCStudentQuizResult} hideNavBar={true} />
        <Scene key="imagequiz" component={ImageQuiz} hideNavBar={true} />
        <Scene key="loveletterinput" component={LoveLetterInput} hideNavBar={true} />
        <Scene key="loveletterdisplay" component={LoveLetterDispley} hideNavBar={true} />
        <Scene key="home" component={Home} hideNavBar={true} />
        <Scene key="login" component={Login} hideNavBar={true} />
        <Scene key="register" component={Register} hideNavBar={true}></Scene>
        <Scene key="rank" component={Rank} hideNavBar={true}></Scene>
        <Scene key="photo" component={Photo} hideNavBar={true} />
        <Scene key="editmember" component={EditMember} hideNavBar={true}></Scene>
        <Scene key="rankdetail" component={RankDetail} hideNavBar={true}></Scene>
        <Scene key="uploadphoto" component={UploadPhoto} hideNavBar={true}></Scene>
        <Scene key="friends" component={Friends} hideNavBar={true}></Scene>
        <Scene key="quizlist" component={QuizList} hideNavBar={true}></Scene>
        <Scene key="quizresultdetail" component={QuizResultDetail} hideNavBar={true}></Scene>
        <Scene key="quizresult" component={QuizResult} hideNavBar={true}></Scene>
        <Scene key="doomed" component={Doomed} hideNavBar={true}></Scene>
        <Scene key="chat" component={Chat} hideNavBar={true}></Scene>
        <Scene key="doomedresult" component={DoomedResult} hideNavBar={true}></Scene>
        <Scene key="friendsrequest" component={FriendsRequest} hideNavBar={true}></Scene>
        <Scene key="liveactionquizresult" component={LiveActionQuizResult} hideNavBar={true}></Scene>
        <Scene key="liveactionquiz" component={LiveActionQuiz} hideNavBar={true}></Scene>
        <Scene key="wifecontrol" component={WifeControl} hideNavBar={true}></Scene>
        <Scene key="historyquizdetail" component={HistoryQuizDetail} hideNavBar={true}></Scene>
        <Scene key="historyquiz" component={HistoryQuiz} hideNavBar={true}></Scene>
        <Scene key="historyrank" component={HistoryRank} hideNavBar={true}></Scene>
        <Scene key="member" component={Member} hideNavBar={true} />
        <Scene key="editpassword" component={EditPassword} hideNavBar={true} />   
        <Scene key="signanalysis" component={signAnalysis} hideNavBar={true} />                     
    </Scene>
);

export default scenes;