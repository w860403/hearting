
import HistoryRank from '../containers/historyrank.js';
import React, { Component, PropTypes } from 'react';
import TopBar from '../containers/topbar'

import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
const HistoryRankScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='上榜紀錄' lefttext='Back' />
        <HistoryRank />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default HistoryRankScene;
