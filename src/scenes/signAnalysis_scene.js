import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import TopBar from '../containers/topbar'
import SignAnalysis from '../containers/signAnalysis';

const SignAnalysisScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <TopBar title='星座分析' lefttext='Back' />
        <SignAnalysis />
      </Image>
    </View>

  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent:'center',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default SignAnalysisScene;
