import React, { Component, PropTypes } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  Image
} from 'react-native';
import Login from '../containers/login';


const LoginScene = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../images/home.png')} style={styles.backgroundImage}>
        <Login />
      </Image>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    width: null,
    height: null,
  },
})
export default LoginScene;
