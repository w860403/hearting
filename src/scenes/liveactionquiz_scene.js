import LiveActionQuiz from '../containers/liveactionquiz';
import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Dimensions,
    Image
} from 'react-native';

const LiveActionQuizScene = () => {
    return (
        <LiveActionQuiz />
    );
};
const styles = StyleSheet.create({

})

export default LiveActionQuizScene;
